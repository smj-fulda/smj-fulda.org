---
title: SMS-Adventskalender
permalink: adventskalender/
layout: page
cover_image: adventskalender/kerzen.jpg
---

<figure>
  <img src="https://farm9.staticflickr.com/8483/8169818137_c89c33e245_b.jpg" alt="SMS-Türchen" />
</figure>

<p class="lead">Die SMS-Adventskalender-Aktion ist seit einigen Jahren ein Angebot der SMJ-Fulda um den Advent mit geistigen Impulsen zu bereichern. Täglich wird dabei per SMS ein Adventskalender-Türchen verschickt.</p>

Liebe Bekannte, Freunde und Mitglieder der Schönstatt-Mannesjugend Fulda,<br/>
die Zeit vor der Ankunft unseres Herrn ist eine Zeit des Wartens und der Vorbereitung. Wir möchten Euch daher auch in diesem Jahr diese Wartezeit etwas versüßen. Jedoch nicht mit Schokoladenblättchen oder ähnlichem, sondern mit geistiger Kost. So laden wir Dich ein, bei unserem SMS-Adventskalender mitzumachen und jeden Tag ein kleines Türchen kostenlos auf das Handy zu erhalten.

Wer mitmachen möchte schreibe einfach eine eMail mit Name und Hanynummer an [adventskalender@smj-fulda.org](mailto:adventskalender@smj-fulda.org) und erhält vom 1. bis 24. Dezember täglich eine kostenlose SMS.

Allen, die mitmachen, wünschen wir viel Freude mit den einzelnen Türchen.

Einen schönen Advent wünscht die SMJ-Fulda!

## Anmerkung zum Datenschutz:

Die Kontaktdaten werden ausschließlich im Zeitraum vom 1. bis 24. Dezember im Rahmen der Aktion zum Versenden der SMS verwendet und anschließend gelöscht. Die Daten werden vertraulich behandelt und nicht zu Werbezwecken genutzt oder an Dritte weitergegeben.

Ein Anspruch auf Teilnahme besteht nicht. Die Aktion steht unter dem Vorbehalt der technischen Durchführbarkeit.

Um sich vom SMS-Adventskalender wieder abzumelden, genügt eine E-Mail an: [adventskalender@smj-fulda.org](mailto:adventskalender@smj-fulda.org)

## Wichtige Hinweise

* Die Anmeldung ist nur bis zum *30. November* möglich.
* Zur Teilnahme ist eine *deutsche Mobilfunknummer* notwendig. Ein Versand der Nachrichten ins Ausland ist leider ausgeschlossen.
* Eine Anmeldung von Freunden/Verwandten etc. ist leider nicht möglich. Die Anmeldung kann *nur persönlich* für die eigene Rufnummer erfolgen.

## Presseschau:

* [Adventskalender per SMS – die SMJ macht‘s möglich](http://schoenstatt.de/de/news/1729/112/Adventskalender-per-SMS-die-SMJ-macht-s-moeglich.htm) (23. November 2012, *schoenstatt.de*)
* [SMS als Adventskalender](https://www.dropbox.com/s/t4pay3tc1vpl7gp/SMJ-SMS-Adventskalender.jpg) (24. November 2012, *Die Tagespost*)
* [Der etwas andere Adventskalender ](http://www.firstlife.de/der_etwas_andere_adventskalender/) (26. November 2013, *f1rstlife.de*)
