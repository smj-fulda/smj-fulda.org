---
categories:
- Glaubenskurs
- Sonstiges
migrated:
  node_id: 862
  migrated_at: 2017-02-21 14:40
  alias: artikel/862-glaubenskurs-der-gemeinschaften-smj,-mjf,-kjf-omi-jugend
  user_id: 5
image:
  cover: https://farm6.staticflickr.com/5302/5561483626_98cf1c806b_b.jpg
author: Tobias Buedel
title: Glaubenskurs der Gemeinschaften SMJ, MJF, KJF & OMI-Jugend
created_at: 2011-03-21 15:53
excerpt: Am Wochenende vom 18. – 20. März 2011 fand im Thomas-Morus-Haus in Hilders ein Glaubenskurs für Jugendliche ab 16 Jahren statt. Die Jugendgemeinschaften SMJ (Schönstatt-Mannesjugend), MJF (Mädchen und junge Frauen Schönstatt), KJF (Katholische Jugend Fulda) sowie die OMI-Jugend (Oblaten der Maria Immaculata) stellten sich der Herausforderung jungen Christen in Glaubensfragen weiterhelfen zu wollen. So gestalteten sie gemeinsam ein vielversprechendes Programm mit Referenten, die den Jugendlichen viele Dinge mit auf ihren Weg gaben und ihnen Rede und Antwort standen.
---
<p>Am Wochenende vom 18. &ndash; 20. März 2011 fand im Thomas-Morus-Haus in Hilders ein Glaubenskurs für Jugendliche ab 16 Jahren statt. Die Jugendgemeinschaften SMJ (Schönstatt-Mannesjugend), MJF (Mädchen und junge Frauen Schönstatt), KJF (Katholische Jugend Fulda) sowie die OMI-Jugend (Oblaten der Maria Immaculata) stellten sich der Herausforderung jungen Christen in Glaubensfragen weiterhelfen zu wollen. So gestalteten sie gemeinsam ein vielversprechendes Programm mit Referenten, die den Jugendlichen viele Dinge mit auf ihren Weg gaben und ihnen Rede und Antwort standen.</p>
<p>
	Insgesamt hatten sich 21 Jugendliche zu diesem Wochenende eingefunden. Nach dem Beziehen der Zimmer und dem Abendessen ging es auch schon bald ans Eingemachte. Nach einer kurzen Vorstellungsrunde der Jugendlichen begrüßten sie in ihrer Runde den Bischofssekretär Dirk Gärtner. Dieser verschaffte den Jugendlichen einen Einblick in eines seiner Spezialthemen, der Moraltheologie. Besprochen wurden moralische Fragen über Abtreibung, Sterbehilfe, etc. Nach einem langen und doch kurz vorkommenden Abend voller Gespräch und Austausch gingen die Jugendlichen nach einem Abendgebet direkt oder über einen gemeinsamen Spieleabend in ihre Betten.</p>
<p>
	Der kommende Samstagmorgen hatte auch schon sein zweites Themengebiet im Gepäck. Eingeladen war P. Felix Rehbock, Jugendseelsorger in Hünfeld für die Oblaten. Er behandelte mit den Jugendlichen das Thema &bdquo;Kirche-Glaube-ich&ldquo;. &bdquo;Was muss ich alles glauben um Christ zu sein?&ldquo;, &bdquo;Glaube ja-Kirche nein, wie argumentiert man?&ldquo; oder &bdquo;Wie stehe ich mir meinem Glauben in der Gesellschaft?&ldquo; waren nur einige der vielen Fragen mit denen sich die Jugendlichen und P. Felix Rehbock beschäftigten.</p>
<p>
	Anschließend an diesen Vormittag und das darauf folgende Mittagessen gingen die Jugendlichen nach draußen um sich bei ein paar Spielen und vielen Sonnenstrahlen ein wenig zu erholen und wortwörtlich Luft zu holen. Denn darauffolgend nach einer kurzen Kaffeepause folgte auch schon die dritte und letzte große Themeneinheit dieses Wochenendes. Mit dem Thema &bdquo;Liturgie&ldquo; beschäftigen sich die Jugendlichen zusammen mit Kaplan Florian Böth, Präsens der KJF und Kaplan in Petersberg. Kaplan Böth, der diesen Glaubenskurs mit organisierte und durchführte, erläuterte den Jugendlichen den Sinn von Zeichen in der Liturgie und erklärte warum manche Dinge eben so sind wie sie sind. Auch die Grundlegung und Gestaltung der Liturgie waren beliebte Themen.</p>
<p>
	Nach dem Abendessen wurden alle Fragen der &bdquo;Fragewand&ldquo; besprochen und beantwortet. Die Fragewand war eine Wand, auf der die Jugendlichen das ganze Wochenende über Fragen über &bdquo;Gott und die Welt&ldquo; formulieren konnten. Zum Abschluss dieses ereignisreichen Tages gestalteten die Jugendlichen eine &bdquo;Abend der Versöhnung&ldquo;. Einen Abend voller Lobpreis und Anbetung. Pfarrer Carsten Noll war zu diesem Abend eingeladen um eine Katechese zu halten.</p>
<p>
	Am Sonntagmorgen wurden noch die übrig gebliebenen Fragen der Fragewand beantwortet. Es gab auch eine Reflexionsrunde über das erlebte Wochenende. Den Abschluss des Wochenendes feierten die Jugendlichen mit einer Heiligen Messe, anschließend gab es Mittagessen und alle Jugendlichen brachen neu gestärkt in ihrem Glauben wieder zurück in ihren Alltag auf.</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157626231958337">
  <a href="https://www.flickr.com/photos/45962678@N06/5561483626/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5302/5561483626_98cf1c806b_q.jpg" alt="A-K & Jojoman" data-src-large="https://farm6.staticflickr.com/5302/5561483626_98cf1c806b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5560910857/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5137/5560910857_398b0ea574_q.jpg" alt="Florian" data-src-large="https://farm6.staticflickr.com/5137/5560910857_398b0ea574_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5560913321/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5176/5560913321_8564b4b442_q.jpg" alt="Marta & Nicole" data-src-large="https://farm6.staticflickr.com/5176/5560913321_8564b4b442_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5560915357/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5291/5560915357_d91ceb7e09_q.jpg" alt="Paul" data-src-large="https://farm6.staticflickr.com/5291/5560915357_d91ceb7e09_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5560917607/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5179/5560917607_5146327c81_q.jpg" alt="Johannes" data-src-large="https://farm6.staticflickr.com/5179/5560917607_5146327c81_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5560919863/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5093/5560919863_2ccd9a37fb_q.jpg" alt="Julian" data-src-large="https://farm6.staticflickr.com/5093/5560919863_2ccd9a37fb_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5561496702/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5017/5561496702_3d4e3920cc_q.jpg" alt="Johannes" data-src-large="https://farm6.staticflickr.com/5017/5561496702_3d4e3920cc_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5561624871/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5150/5561624871_3f98bed093_q.jpg" alt="Tobias & Christin" data-src-large="https://farm6.staticflickr.com/5150/5561624871_3f98bed093_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5561628817/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5264/5561628817_74687fb50b_q.jpg" alt="Kaplan Dirk Gärtner" data-src-large="https://farm6.staticflickr.com/5264/5561628817_74687fb50b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5561498376/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5187/5561498376_4ee6b2a512_q.jpg" alt="Vortrag Moraltheologie" data-src-large="https://farm6.staticflickr.com/5187/5561498376_4ee6b2a512_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5561500644/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5222/5561500644_17919ea703_q.jpg" alt="Kaplan Dirk Gärtner" data-src-large="https://farm6.staticflickr.com/5222/5561500644_17919ea703_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5560927527/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5019/5560927527_7507480d6d_q.jpg" alt="Glaube - Kirche - Ich" data-src-large="https://farm6.staticflickr.com/5019/5560927527_7507480d6d_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5560929195/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5268/5560929195_d9140b312c_q.jpg" alt="Pater Felix Rehbock" data-src-large="https://farm6.staticflickr.com/5268/5560929195_d9140b312c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5561506178/in/album-72157626231958337"><img src="https://farm6.staticflickr.com/5300/5561506178_5fa6f0e57e_q.jpg" alt="Pater Felix Rehbock" data-src-large="https://farm6.staticflickr.com/5300/5561506178_5fa6f0e57e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157626231958337" class="flickr-link" title="Bildergalerie Glaubenskurs März 2011 auf Flickr">Bildergalerie</a>
</figure>
