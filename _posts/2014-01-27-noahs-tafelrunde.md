---
tags: [smjfulda, gewo]
migrated:
  node_id: 975
  migrated_at: 2017-02-20 22:28
  alias: artikel/975-noahs-tafelrunde
  user_id: 17
tagline: Die Verantwortlichen der SMJ-Fulda haben sich in Dietershausen zur Jahresplanung getroffen.
image:
  cover: https://farm6.staticflickr.com/5500/12178749336_8d45733458_b.jpg
author: Christian
title: Noahs Tafelrunde
created_at: 2014-01-27 22:01
excerpt: Am 17. Und 18. Januar fand die sogenannte „Noahs Tafelrunde“ in Dietershausen statt. Jedes Jahr treffen sich die Verantwortlichen der SMJ Fulda, um zu Beginn des Jahres auf die Herausforderungen der kommenden zwölf Monate zu blicken. Hier werden Aufgaben verteilt, Planungen gemacht, Themen erarbeitet und Details des anstehenden Jahres erörtert. Das Jahr 2014 steht ganz im Zeichen des <a href="http://2014.schoenstatt.de/">100jährigen Jubiläums der Schönstattbewegung</a>. Im Oktober feiert die weltweite Schönstattfamilie das hundertjährige Bestehen von Schönstatt. Dazu findet ein großes internationales Fest am Ursprungsort Vallendar statt.
---
<p>Am 17. Und 18. Januar fand die sogenannte &bdquo;Noahs Tafelrunde&ldquo; in Dietershausen statt. Jedes Jahr treffen sich die Verantwortlichen der SMJ Fulda, um zu Beginn des Jahres auf die Herausforderungen der kommenden zwölf Monate zu blicken. Hier werden Aufgaben verteilt, Planungen gemacht, Themen erarbeitet und Details des anstehenden Jahres erörtert.</p>
<p>Das Jahr 2014 steht ganz im Zeichen des <a href="http://2014.schoenstatt.de/">100jährigen Jubiläums der Schönstattbewegung</a>. Im Oktober feiert die weltweite Schönstattfamilie das hundertjährige Bestehen von Schönstatt. Dazu findet ein großes internationales Fest am Ursprungsort Vallendar statt.</p>
<p>Doch die SMJ blickt nicht nur auf das Fest. <a href="/zeltlager">Das Zeltlager</a> bildet den Höhepunkt unserer Jahresarbeit. Wie jedes Jahr besteht das Programm aus verschiedenen Angeboten. Vom 29. Juli bis zum 9. August übernachtet das Team auf einem Zeltplatz in Schimborn bein Aschaffenburg. Außerdem stehen wieder vier Gemeinschaftswochenenden für Jungs von 9 bis 13 auf dem Programm. Für die Älteren Kreistagungen, Glaubenskurse und das Fest des Glaubens. Auf Regioebene werden zusammen mit den Bistümern Mainz/Limburg und Speyer eine Fahrt nach Innsbruck und ein Zeltlager angeboten. <a href="/termine/2014">Der Terminkalender 2014</a> ist also schon gut gefüllt.</p>
