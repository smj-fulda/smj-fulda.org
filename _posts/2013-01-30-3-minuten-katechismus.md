---
tags: [katechismus, aktion]
migrated:
  node_id: 928
  migrated_at: 2017-02-20 22:27
  alias: artikel/928-3-minuten-katechismus
  user_id: 1
tagline: Katechismus – leicht gemacht!
title: 3 Minuten Katechismus
created_at: 2013-01-30 21:34
excerpt: 'Am 11. Oktober 2012 hat Papst Benedikt XVI. das Jahr des Glaubens ausgerufen. Wesentlicher Bestandteil davon ist die Auseinandersetzung mit den Inhalten und Werten des Glaubens. Das ist manchmal gar nicht so einfach, haben sich ein paar Filmemacher aus Österreich gedacht: Doch in ihrem <strong>3-Minuten-Katechismus</strong> (Arbeitstitel <em>Oh mein Gott</em>) werden die Themen des Glaubensbekenntnisses in 72 Folgen mit einer Länge von je 3-4 Minuten verständlich erklärt: Katechismus – leicht gemacht!'
---
<p>Am 11. Oktober 2012 hat Papst Benedikt XVI. das Jahr des Glaubens ausgerufen. Wesentlicher Bestandteil davon ist die Auseinandersetzung mit den Inhalten und Werten des Glaubens. Das ist manchmal gar nicht so einfach, haben sich ein paar Filmemacher aus Österreich gedacht: Doch in ihrem <strong>3-Minuten-Katechismus</strong> (Arbeitstitel <em>Oh mein Gott</em>) werden die Themen des Glaubensbekenntnisses in 72 Folgen mit einer Länge von je 3-4 Minuten verständlich erklärt: Katechismus &ndash; leicht gemacht!</p>
<p><iframe allowfullscreen="" frameborder="0" height="370" src="https://www.youtube.com/embed/xirLaXbEPhY" width="658"></iframe></p>
<p>Wir laden Dich ein mit uns auf die Spuren unseres katholischen Glaubens zu kommen. Aber nicht nur Du und Ich wollen mehr über unseren Glauben erfahren. Gerne kannst Du diese DVD&#39;s auch an Familie, Freunde und Bekannte verschenken &ndash; im Alter von 12 bis 120 Jahren.</p>
<p>Mit einer gemeinsamen Sammelbestellung können wir dabei sogar Geld sparen: Wir haben schon über 100 Vorbestellungen und können eine DVD-Box daher zum Selbstkostenpreis von <strong>2,90 &euro;</strong> anbieten. Das ist ein klasse Preis für so ein geniales Produkt!</p>
<p><strong>Bestellungen bitte bis 10. Februar an <a href="mailto:katechismus@smj-fulda.org">katechismus@smj-fulda.org</a></strong></p>

<p>Auf der Website gibt es schon ein paar <a href="http://3mc.me/de/toc.html">Videos als Vorschau</a>.</p>
<p><iframe allowfullscreen="" frameborder="0" height="370" src="https://www.youtube.com/embed/aiDTInDp9gA" width="658"></iframe></p>
