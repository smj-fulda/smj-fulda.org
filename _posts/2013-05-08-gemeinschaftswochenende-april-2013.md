---
tags: [smjfulda, gewo]
migrated:
  node_id: 948
  migrated_at: 2017-02-20 22:27
  alias: artikel/948-gemeinschaftswochenende-april-2013
  user_id: 1
tagline: Revolverhelden, bereit zum Duell. Die SMJ-Fulda traf sich im Schönstattzentrum Dietershausen zum Gemeinschaftswochenende.
title: Gemeinschaftswochenende April 2013
created_at: 2013-05-08 13:13
excerpt: Zum wiederholten Male trafen sich am Wochenende vom 19. -21. April über 20 abenteuerlustige Jungs in Dietershausen zum Gemeinschaftswochenende der SMJ-Fulda. Mit dabei, 5 junge und motivierte Betreuer. Vor ihnen lag ein spannendes Wochenende mit dem Motto „Revolverheld, bereit zum Duell“. Und Duelle sollte es dieses Wochenende zur Genüge geben.
---
<p>Zum wiederholten Male trafen sich am Wochenende vom 19. -21. April über 20 abenteuerlustige Jungs in Dietershausen zum Gemeinschaftswochenende der SMJ-Fulda. Mit dabei, 5 junge und motivierte Betreuer. Vor ihnen lag ein spannendes Wochenende mit dem Motto &bdquo;Revolverheld, bereit zum Duell&ldquo;. Und Duelle sollte es dieses Wochenende zur Genüge geben.</p>
<p>Der Freitagabend begann, wie jedes Mal, mit dem gemeinsamen Abendessen. Gut gestärkt wurde der Freitagabend wie gewohnt mit Kennenlernspielen begonnen. Nachdem nun jeder zumindest die Namen der anderen kannte, Jungs mit gutem Gedächtnis natürlich auch Hobbys, Wohnort und so weiter, wurde der Abend mit spannenden Spielen wie Schrubberhockey zu Ende gebracht. Nach diesem anstrengenden Abend gingen alle fröhlich in die frisch selbstbezogenen (naja, jedenfalls bei den meisten selbst bezogen) Betten.</p>
<p>Am Samstagmorgen konnten sich die Kinder über ein wenig Frühsport freuen, die Betreuer natürlich auch. Am Vormittag erwartete die Kinder ein Vortrag über das Thema &bdquo;Mann sein&ldquo;, welches anhand des Themas &bdquo;Cowboy&ldquo; anschaulich erklärt werden konnte.</p>
<p>Am Nachmittag folgte eine Cowboy-Olympiade. Bei dieser durften die Jungs in 5 Riegen gegeneinander antreten. Die Aufgaben waren: Lasso werfen, Hufeisen werfen, Zielwerfen, Gold waschen und Stiefel werfen. Der Höhepunkt für die meisten war das Goldwaschen, welches auch nach der Olympiade noch für Begeisterung sorgte.</p>
<p>Am Abend des Samstages kam es zum großen Duell zwischen Kindern und unserem Diözesanleiter Steffen. Beim großen &bdquo;SMJ Fulda - Schlag den Büdel&ldquo; hatten die Kinder in 15 Spielen die Möglichkeit unserem &bdquo;Chef&ldquo; mal zu zeigen wo der Hammer hängt. In &bdquo;Schlag den Raab&ldquo;-Manier wurde das Schönstattzentrum zum Studio umfunktioniert und um jeweils steigende Punktezahlen gekämpft. Bei Quiz, Ratespielen, Schätzfragen und Sport wurde es immer spannender, bis zum Schluss das letzte Spiel die Entscheidung bringen musste. Bei diesem mussten die Kinder gemeinsam Steffen besiegen, was ihnen natürlich auch gelang. Als stolze Gewinner von über 60 Schokoriegeln machten sie sich auf in die Betten.</p>
<p>Etwas später als der Samstag begann der Sonntag. Ohne Frühsport, das fanden nicht nur die Jungen, sonder auch die immer noch erschöpften Betreuer gut. Als nächstes stand die gemeinsame Messe in dem Kapellchen an. Darauf folgte das gemeinsame Zimmeraufräumen und das das Wochenende ging, langsam aber sicher, dem Ende zu. Nach einigen Spielen folgte das abschließende Mittagessen.</p>
<p>Alles in allem war es ein sehr schönes Wochenende. Auch für die Betreuer. Die freuen sich jetzt schon wieder auf das nächste Gemeinschaftswochende, und DU hoffentlich auch&hellip;</p>
