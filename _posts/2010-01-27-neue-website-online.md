---
categories:
- SMJ-Fulda
- website
migrated:
  node_id: 816
  migrated_at: 2017-02-20 22:25
  alias: artikel/816-neue-website-online
  user_id: 1
tagline: Die neue Website der SMJ-Fulda ist nun online!
title: Neue Website online
created_at: 2010-01-27 11:56
excerpt: Freunde der Sonne, es ist vollbracht! Seit heute morgen 11:17 Uhr Ortszeit im Königreich Flieden ist die neue Website der SMJ-Fulda auf unsere Domain <a href="http://smj-fulda.org/" rel="nofollow">smj-fulda.org</a> geschaltet und somit offiziell "online". Das Forum ist davon unangetastet und weiterhin unter <a href="http://smj-fulda.org/forum" rel="nofollow">smj-fulda.org/forum</a> erreichbar.
---
<div class="section"><p>Freunde der Sonne, es ist vollbracht!</p>
<p>Seit heute morgen 11:17 Uhr  Ortszeit im Königreich Flieden ist die neue Website der SMJ-Fulda auf  unsere Domain <a target="_blank" href="http://smj-fulda.org/">smj-fulda.org</a>  geschaltet und somit offiziell &quot;online&quot;. Das Forum ist davon unangetastet und weiterhin unter <a href="http://smj-fulda.org/forum">smj-fulda.org/forum</a>  erreichbar.</p>
</div>
<p>Wie bereits bei Noahs Tafelrunde erläutert, basiert  die Website auf einem ziemlich ausgefuchsten Content-Management-System  (CMS) namens <a href="http://drupal.org/">drupal</a>.  Dadurch haben wir gegenüber der alten Homepage, die ohne ein solches  System auskam, einige Vorteile: Zum einen ist die Pflege wesentlich  vereinfacht, da dies recht komfortabel direkt auf der Website möglich  ist und somit auch Laien ganz einfach Texte einstellen und bearbeiten  können. Das führt uns auch zum nächsten Punkt, nämlich, dass  grundsätzlich jeder SMJ-ler einfach an der Homepage mitarbeiten kann,  z.B. indem er einen Bericht schreibt, der dann nicht erst an den Admin  geschickt und eingepflegt werden muss, sondern dass kann jeder fanz  einfach selbst erledigen. Doch näheres dazu gibt es später...</p>
<div class="section"><p>Doch  abseits von der bloßen Struktur gibt es auch noch einige weiter  Neuigkeiten:</p>
<ul><li>Wir  haben nun einen kompletten <strong>Terminkalender,</strong> der das bisherige Forum <a href="http://smj-fulda.org/forum/index.php?board=12.0"><i>Veranstaltungshinweise</i></a>  ersetzen soll. Damit ist es nun möglich, direkt Termine in das CMS  einzutragen, die dann z.B. in einer <a href="http://smj-fulda.org/termine/kalender">Kalenderansicht</a>  angezeigt oder auf der Startseite als <i>Nächste Termine</i>  angekündigt werden. Auch dort sind selbstverständlich  Diskussionsbeiträge zu den Terminen möglich.</li><li>Ich habe auch einen  Account bei <strong>Twitter</strong> eingerichtet, unter dem immer aktuelle Meldungen  von der SMJ-Fulda gepostet werden. Unsere Tweets sind direkt auf unserer  Startseite sowie unter <a href="http://twitter.com/smjfulda">http://twitter.com/smjfulda</a> zu finden.</li><li><p>Auch bei <strong>Facebook</strong> sind wir mit einer eigenen Seite vertreten: <a href="http://www.facebook.com/pages/SMJ-Fulda/242848884290">SMJ-Fulda</a>. Wer dort angemeldet ist, kann gerne ein Fan werden.</p>
</li><li><p>Die Bilder für unsere Website werden bei <strong>Flickr</strong> gehostet und können von dort aus auch gerne weiterverwendet, runtergeladen und angesehen werden.</p>
</li></ul>
</div>
<p>Das waren jetzt erst mal in kürze die wichtigsten&nbsp;Features. Es wird sich im Laufe der Zeit auch noch sicherlich das ein oder andere weiterentwickeln. Aber zunächst hoffe ich, dass die Seite auf allgemeines Wohlgefallen trifft :) Für Fragen, Anregungen, Kritik, Verbesserungsvorschläge und dergleichen bin ich natürlich jederzeit ansprechbar.</p>

