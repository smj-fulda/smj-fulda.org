---
title: "In 8 Tagen um die Welt"
tagline: Zeltlager der SMJ-Fulda 2019 
author: Christian Schopp
tags: [zeltlager]
image:
  cover: /aktionen/zeltlager/2019/title.jpg
  align: top
---

Dieses Jahr wird unser Zeltlager international. Wir bereisen die Kontinente der Erde, lernen fremde Menschen und Kulturen kennen. Überall gibt es Interessantes zu entdecken. Welches Land gefällt dir am besten? Wie immer erwarten dich wieder unvergessliche Tage mit Lagerfeuer, Sonnenuntergängen, Fussball, Herausforderung und Freundschaft.

Jules Verne reist im Jahre 1873 mit seiner Geschichte in 80 Tagen um die Welt. Seit damals begeistert dieses Abenteuer immer wieder aufs Neue. Stummfilm, Theaterstück, Brettspiel, Musical, Hörspiel, Cartoon oder Spielfilm – das Buch wurde schon auf viele verschiedene Weisen interpretiert. Es gibt sogar eine Jules Verne Trophy für die schnellste Weltumrundung mit einem Segelboot.

Am Sonntag, den 30. Juni, geht die Reise für alle 9 bis 14-jährigen Jungs los. Bis zum 7. Juli übernachten wir gemeinsam auf dem Jugendzeltplatz <em>Am Reith</em> in 97688 Hausen. In den acht Tagen erwarten uns so einige Überraschungen. Ob wir es bis zum Ende schaffen, wieder zrück nach Deutschland zu finden, bleibt abzuwarten. Oder verlieren wir irgendwann die Orientierung und bleiben irgendwo in fernen Ländern stecken?

<img src="/aktionen/zeltlager/2019/teaser.jpg">

Wer unsere Reise nicht verpassen will, sollte sich bei Lagerleiter Christian (<a href="mailto:christian.schopp@smj-fulda.org">christian.schopp@smj-fulda.org</a>) melden oder den <a href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2019.pdf">Anmeldeflyer</a> per Post verschicken.
Für Fragen aller Art stehen wir gerne zur Verfrügung.

Das Zeltlagerteam 2019 freut sich auf dich!

<a href="/zeltlager" class="btn">Mehr zum Zeltlager</a>
