---
title: Startschuss für das Zeltlager 2022
tagline: Neuer Termin und neue Lagerleitung
author:
tags: [zeltlager]
image:
  cover: https://live.staticflickr.com/65535/51433631829_7ac4593ebb_k_d.jpg
---

Am 26.10. trafen sich Diözesanführer Adam Muthig und Lagerleiter Christian Schopp mit einigen Gruppenleitern um in die Planung des Zeltlagers 2022 einzusteigen. Termin und Ort stehen bereits fest. Das Thema wurde an der Zeltlagernachbereitung festgelegt. Jetzt galt es zu klären, wer die Lagerleitung im nächsten Jahr übernimmt.

Nachdem Christian den Wunsch geäußert hatte, seinen Posten abzugeben, wurden mögliche Nachfolger gesucht. Bei den Gesprächen am vergangenen Dienstag wurde sich auf die folgende Konstellation geeinigt: Der diesjährige stellvertretende Lagerleiter Fabian Buhl übernimmt zusammen mit dem erfahrenen Gruppenleiter Lennard Wolf die zukünftige Lagerleitung.

Um einen reibungslosen Ablauf zu gewährleisten, unterstützt Christian im nächsten Jahr die beiden neuen Zeltlager-Chefs. Damit ist die Grundlage für ein erfolgreiches Zeltlager 2022 geschaffen. Voller Motivation gehen die frisch gebackenen Lagerleiter an die Arbeit. Sie freuen sich schon auf die neuen Herausforderungen und Allem voran auf die herausregenden Erfahrungen, Bilder und Eindrücke, die im nächsten Zeltlager auf sie warten.
Vielen Dank, dass ihr euch dieser Aufgabe stellt. Christian danken wir für seine geleistete Arbeit.

Wir sehen uns im Zeltlager 2022:

<ul>
  <li><strong>Termin:</strong> Mittwoch 17. bis Samstag 27. August</li>
  <li><strong>Ort:</strong> Zeltplatz „Am Adamsberg“ in 63869 Heigenbrücken</li>
  <li><strong>Thema:</strong> Im Zauberwald</li>
  <li><strong>Teilnehmer:</strong>	Jungs von 9 bis 14 Jahren</li>
</ul>

<a href="https://files.smj-fulda.org/zeltlager/zeltlager-flyer-2022.pdf">
  Zeltlager-Flyer
</a>
