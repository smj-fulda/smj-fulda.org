---
tags: [schönstatt, jubiläum2014]
migrated:
  node_id: 988
  migrated_at: 2017-02-20 22:28
  alias: artikel/988-100-jahre-schönstatt
  user_id: 1
tagline: Vom 16. bis 19. Oktober war das Jubiläumsfest der internationalen Schönstatt-Bewegung am Ursprungsort in Vallendar
title: 100 Jahre Schönstatt
created_at: 2014-10-23 00:09
excerpt: 'Endlich war es soweit. Der 18. Oktober 2014 stand vor der Tür. Die Schönstattbewegung feierte zum Anlass ihres 100-jährigen Bestehens ein großes Jubiläumsfest an ihrem Ursprungsort in Vallendar. Am Donnerstag, den 16. Oktober, begannen die Feierlichkeiten, die bis zum Sonntag andauerten. Tausende von Pilgern strömten nach Schönstatt um dabei zu sein, wenn es heißt: „ 100 Jahre Liebesbündnis“ Auch die SMJ-Fulda reiste nach Schönstatt. Und das auch schon zur Helferwoche. Einige junge Männer nahmen sich die Zeit und reisten montags bereits nach Schönstatt um die Feierlichkeiten vorzubereiten! Diese Helfer wurden dringend gebraucht, denn es wurden 10.000 Besucher erwartet.'
---
<p>Endlich war es soweit. Der 18. Oktober 2014 stand vor der Tür. Die Schönstattbewegung feierte zum Anlass ihres 100-jährigen Bestehens ein großes Jubiläumsfest an ihrem Ursprungsort in Vallendar.</p>
<p>Am Donnerstag, den 16. Oktober, begannen die Feierlichkeiten, die bis zum Sonntag andauerten. Tausende von Pilgern strömten nach Schönstatt um dabei zu sein, wenn es heißt: &bdquo; 100 Jahre Liebesbündnis&ldquo;</p>
<p>Auch die SMJ-Fulda reiste nach Schönstatt. Und das auch schon zur Helferwoche. Einige junge Männer nahmen sich die Zeit und reisten montags bereits nach Schönstatt um die Feierlichkeiten vorzubereiten!</p>
<p>Diese Helfer wurden dringend gebraucht, denn es wurden 10.000 Besucher erwartet.</p>
<p>Das Fest begann bereits am Donnerstag, als die verschiedenen Nationen die Möglichkeit hatten ins Urheiligtum zu pilgern. Am Abend fand dann die Willkommensfeier in der Pilgerarena statt. Schon da konnte man die besondere Stimmung spüren. Menschen, die aus vielen Ländern der Welt nach Schönstatt gekommen sind, um gemeinsam das Liebesbündnis zu feiern.</p>
<p>Am Freitag reisten weitere junge Männer der SMJ Fulda an, sodass die Teilnehmerzahl aus der Diözese bei 15 lag. Das Highlight am Freitagabend war sicherlich das Ankommen der Fackelläufer.</p>
<p>Die Fackelläufer waren 9 Tage zuvor in Pompeji gestartet und liefen am Abend in die Pilgerarena ein. Christoph Schopp aus dem Jossgrund war einer der insgesamt 90 Läufer.</p>
<p>Die Strecke von Pompeji nach Schönstatt, sage und schreibe 1800 km, lief jeder Läufer auch mit verschiedenen Anliegen im Herzen, die er im Vorfeld bekommen hatte. Das Licht der Fackel wurde bei einer Audienz im Vatikan durch Papst Franziskus gesegnet. Beim Einlauf der Fackelläufer kochten die Emotionen über. Es war ein beeindruckender Moment!</p>
<p>Am späteren Abend gab es unter anderem &bdquo;Die Nacht des Heiligtums&ldquo; am Urheiligtum für die gesamte SMJ.</p>
<p>Die SMJ-Fulda nahm sich in der Nacht eine kurze Zeit für sich. Im Heiligtum &bdquo;Mariengart&ldquo; sangen die Jungmänner und vertrauten der Gottesmutter ihre Anliegen an. Alle waren dankbar für diesen Augenblick und das gemeinsame Erlebnis. Anschließend ließen einige den Abend gemütlich ausklingen.</p>
<p>Der Samstag, genau 100 Jahre nach dem erstmaligen Schließen des Liebesbündnisses einer Gruppe junger Männer um ihren Spiritual Pater Josef Kentenich in der Friedhofskapelle, die zum Urheiligtum der gesamten Bewegung wurde, wurde mit einem Festgottesdienst in der Pilgerarena begonnen. Dabei war auch ein Gesandter des Papstes anwesend: Giovanni Kardinal Lajolo.</p>
<p>Im Laufe der Tage gab es verschiedene Angebote in Schönstatt. So konnte man sich in mehreren Bündniszelten aufhalten, die teilweise sehr informativ gestaltet waren.</p>
<p>Außerdem wurden viele Workshops angeboten oder aber man suchte den Austausch mit anderen Schönstättern. Es war ein Zusammentreffen von Jung und Alt.</p>
<p>Am frühen Abend folgte dann der Höhepunkt des ganzen Wochenendes: Die Liebesbündnisfeier!</p>
<p>Tausende Pilger erneuerten gemeinsam ihr Liebesbündnis mit der Gottesmutter. Diese Feier zeigte, wie sehr ein Bündnis eine Gemeinschaft wachsen lassen kann und zu was der Glaube fähig ist. Alle Teilnehmer beteten die Kleine Weihe in ihrer Landessprache. Dies war ein bewegender Moment.</p>
<p>Nach der Feier gab es noch eine Fiesta open Air und dann natürlich ein großes Fest, das alle sehr genossen.</p>
<p>Der Sonntag stand unter der Überschrift &bdquo;Internationale Begegnungen&ldquo;. Die Jugend traf sich oder auch die Familienbewegung machte etwas gemeinsam.</p>
<p>Der Abschluss war dann das Segensgebet am Urheiligtum am Abend.</p>
<p>Es war ein geniales Jubiläum und es gilt Dank zu sagen an alle, die als Helfer im Vorbereitungsteam waren oder die vielen Helfer vor Ort. Nur mit einer großen Helferschaar ist so ein Fest möglich!</p>
<p>Auf die nächsten 100 Jahre!</p>
