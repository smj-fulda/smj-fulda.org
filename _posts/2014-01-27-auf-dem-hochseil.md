---
tags: [schönstatt, musical, kentenich]
migrated:
  node_id: 976
  migrated_at: 2017-02-20 22:28
  alias: artikel/976-auf-dem-hochseil
  user_id: 17
tagline: Ein Musical über Pater Kentenich, den Gründer der Schönstattbewegung.
image:
  cover: https://farm8.staticflickr.com/7307/12178704086_1fa04b2771_b.jpg
author: Christian
title: Auf dem Hochseil
created_at: 2014-01-27 22:02
excerpt: Am vergangenen Sonntag, den 26. Januar wurde das Musical <em>„Auf dem Hochseil“</em> im Künzeller Gemeindezentrum aufgeführt. Themmatisch setzt sich das Stück mit den jungen Jahren des Schönstatt-Gründers Pater Josef Kentnich auseinander. Die etwa zweistündige Aufführung wurde von Wilfried Röhrig (ein ehemaliger SMJler) geschrieben und in Zusammenarbeit mit vielen anderen Schönstättern verwirklicht. Die Uraufführung war im Oktober 2013 zum Auftakt des Jubiläumsjahres in Schönstatt. In dem bis auf den letzten Platz ausverkauften Gemeindezentrum in Künzell erwartete die Besucher ein Programm, das mit Chorgesang, tänzerischen Vorführungen sowie Solo-Darbietungen aufwarten konnte.
---
<p>Am vergangenen Sonntag, den 26. Januar wurde das Musical <em>&bdquo;Auf dem Hochseil&ldquo;</em> im Künzeller Gemeindezentrum aufgeführt. Themmatisch setzt sich das Stück mit den jungen Jahren des Schönstatt-Gründers Pater Josef Kentnich auseinander. Die etwa zweistündige Aufführung wurde von Wilfried Röhrig (ein ehemaliger SMJler) geschrieben und in Zusammenarbeit mit vielen anderen Schönstättern verwirklicht. Die Uraufführung war im Oktober 2013 zum Auftakt des Jubiläumsjahres in Schönstatt. In dem bis auf den letzten Platz ausverkauften Gemeindezentrum in Künzell erwartete die Besucher ein Programm, das mit Chorgesang, tänzerischen Vorführungen sowie Solo-Darbietungen aufwarten konnte.</p>
<p>Auf Anfrage der Fuldaer Schönstattfamilie war die SMJ mit neun Helfern vor Ort, um bei der Einlasskontrolle sowie beim Verkauf von CDs, Büchern, Texten, T-Shirts und Notenheften zu helfen. Hoffentlich kann sich unser diesjähriges <a href="/zeltlager">Zeltlager unter dem Motto &bdquo;Sherlock Holmes &ndash; Das Verschwinden des picturae nobilis&ldquo;</a> einem genauso hohen Andrang erfreuen.</p>
<p>Es war erst die zweite Aufführung des Musicals und der Beginn einer deutschlandweiten Tournee, weitere Infos und Termine finden sich auf der Website <a href="http://hochseil.rigma.de">hochseil.rigma.de</a>.</p>
