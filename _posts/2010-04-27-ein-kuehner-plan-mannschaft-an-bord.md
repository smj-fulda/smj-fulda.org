---
categories:
- GeWo
- Seefahrer
- Gemeinschaftswochenenden
migrated:
  node_id: 829
  migrated_at: 2017-02-20 22:25
  alias: artikel/829-ein-kühner-plan-–-mannschaft-bord
  user_id: 25
tagline: Gemeinschaftswochenende im April
author: Thomas Limbach
title: Ein kühner Plan – Mannschaft an Bord
created_at: 2010-04-27 21:32
excerpt: 'Unter dem Motto <em>„Ein kühner Plan – Mannschaft an Bord”</em> fanden sich zehn begeisterte Jungs am Freitag in Dietershausen ein. Die ersten Erfahrungen zur Stärke der Gemeinschaft konnte man schon am Abend machen, als das schöne Wetter ausgenutzt wurde und draußen Verschiedenes gespielt wurde. Samstags hörten wir, was für Gefahren auf See existieren und wie wir sie am besten bewältigen können. Getreu nach dem Motto: „Gemeinsam sind wir stark“ machten wir uns nach dem Mittagessen (wie immer sehr gut!) auf die Suche nach einem Schatz, der in einem Nähe gelegenen Wäldchen versteckt wurde.'
---
<p>Unter dem Motto <em>&bdquo;Ein kühner Plan &ndash; Mannschaft an Bord&rdquo;</em> fanden sich zehn begeisterte Jungs am Freitag in Dietershausen ein. Die ersten Erfahrungen zur Stärke der Gemeinschaft konnte man schon am Abend machen, als das schöne Wetter ausgenutzt wurde und draußen Verschiedenes gespielt wurde.</p>
<p>Samstags hörten wir, was für Gefahren auf See existieren und wie wir sie am besten bewältigen können. Getreu nach dem Motto: &bdquo;Gemeinsam sind wir stark&ldquo; machten wir uns nach dem Mittagessen (wie immer sehr gut!) auf die Suche nach einem Schatz, der in einem Nähe gelegenen Wäldchen versteckt wurde.</p>
<p>Nachdem der Schatz restlos aufgegessen wurde, schauten wir uns das Beispiel eines Seefahrers ein, dem schlechtesten Piraten, von dem man je gehört hat, naja immerhin haben wir schon von ihm gehört!</p>
<p>Als Highlight wandelten wir die berühmte TV-Show in &bdquo;Schlag die Gruppenführer&ldquo; um, was den Rest des Abends einen langen Wettkampf nach sich zog. Zuletzt gewannen die Gruppenführer mit großem Abstand&hellip;</p>
<p>Am Sonntag erfuhren wir, was der Einzelne für die Gemeinschaft bedeuten kann, was für Talente er einbringen kann und wie er immer besser werden kann.</p>
<p>Nach dem sonntäglichen Gottesdienst gingen wir zum Mittagessen und danach trennten sich unsere Wege wieder.</p>
