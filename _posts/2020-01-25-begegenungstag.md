---
title: Begegnungstag
tagline: 
author: Christian Schopp
tags: [smj-fulda]
---

Am 26.01. fand der erste Termin der SMJ Fulda im Jahr 2020 statt: Begegnungstag mit der Schönstattfamilie im Bistum. Der rote Saal platzte aus allen Nähten.

Alle Gliederungen stellten sich vor; auch unser Bischof hielt einen Vortrag über die Zukunft unsres Bistums. Nach einem gemeinsamen Austausch bei Kaffee & Kuchen, hielt Bischof Gerber noch eine Heilige Messe in der Dietershäuser Pfarrkirche. Musikalisch mitgestaltet wurde der Gottesdienst von der Band Himmelwärts aus dem Jossgrund.

Höhepunkt der Messe war die Aussendung des Heilig-Geist-Symbols. Die Friedenstaube, die normalerweise im Kappellchen in Dietershausen hängt, wurde in den letzten Monaten in der Goldschmiede in Schönstatt restauriert. Bis zum Kappellchenfest im August durchwandert das Symbol alle Gliederungen unserer Fuldaer Schönstattfamilie.

Auch der Neubau des Josef-Engling-Hauses schreitet mit großen Schritten voran. Die Grundmauern des Erdgeschosses stehen bereits. In einigen Wochen kann Richtfest gefeiert werden. Die Einweihung des neuen Josef-Engling-Hauses soll auch beim Kapellchenfest am 30. August 2020 erfolgen. Man darf also gespannt sein, was sich im nächsten halben Jahr noch entwickeln wird…

Und dann gibt es noch eine Neuigkeit, die viele nicht mehr für möglich gehalten hätten! [Lesen Sie hier mehr dazu](/artikel/2020-01-27-was-lange-waehrt).
