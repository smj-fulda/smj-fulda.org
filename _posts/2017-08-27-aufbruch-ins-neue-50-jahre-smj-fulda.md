---
title: "Aufbruch ins Neue – 50 Jahre Schönstatt-Mannesjugend im Bistum Fulda"
excerpt: Anlässlich des Besuchs von Pater Josef Kentenich wurde 1957 die Schönstatt-Mannesjugend im Bistum Fulda gegründet. Am vergangenen Samstag trafen sich viele Gratulanten und Festgäste an eben jenem Ort, um das 50-jährige Jubiläum der Schönstattjugend im Bistum Fulda zu feiern. Der Diözesanleiter Simon Wawra freute sich darüber, dass Jung und Alt auf der Marienhöhe in Dietershausen bei den Schönstattschwestern zusammengekommen waren.
author: Sebastian Hartmann
tags: [smjfulda, jubiläum]
image:
    cover: "https://farm5.staticflickr.com/4429/36890617305_d19f2142af_k_d.jpg"
---

Das Kapellchen im Künzeller Ortsteil Dietershausen stand bereits zehn Jahre als anno 1967 – anlässlich des Besuchs von Pater Josef Kentenich – die Schönstatt-Mannesjugend im Bistum Fulda gegründet wurde. Ausgehend von der gemeinsamen Idee, den Glauben im Land des heiligen Bonifatius mit der Flamme der Liebe Jesu neu zu entfachen, nahmen die jungen Männer die Herausforderung an, eigene Erfahrungen und christliche Werte an nachfolgende Generationen weiterzugeben. Und so trafen sich am vergangenen Samstag viele Gratulanten und Festgäste an eben jenem Ort, um das 50-jährige Jubiläum der Schönstattjugend im Bistum Fulda zu feiern. Der Diözesanleiter Simon Wawra freute sich darüber, dass Jung und Alt auf der Marienhöhe in Dietershausen bei den Schönstattschwestern zusammengekommen waren.

# Im Alltag wirken

<figure>
  <img src="https://farm5.staticflickr.com/4362/36719827602_6227da0b89_z_d.jpg">
</figure>
Den Auftakt der Feierlichkeiten setzte der Festgottesdienst im Marienkapellchen. Jugendpfarrer Alexander Best, der erst seit kurzem dieses Amt im Bistum Fulda begleitet, gestand in der Predigt, dass er selbst mit Schönstatt bisher wenig Kontakt gehabt hat, jedoch schon oft mit dem Wirken einzelner Mitglieder der Mannesjugend. Dies kann beinahe sinnbildlich für die Arbeit der Mannesjugend gesehen werden, die sich maßgeblich in Taten und Werken, wie beispielsweise den jährlichen Zeltlagern zeigt, gleichwohl aber die Kraftquelle im Glauben und ganz speziell bei der Gottesmutter Maria hat. Neben Best gratulierten auch die Konzelebranten Pfarrer Thomas Renze und Pfarrer Dagobert Vonderau. Schließlich ließen es sich auch die Schönstattbewegung Mädchen und Junge Frauen (MJF) sowie das Leitungsteam der Fuldaer Schönstattfamilien sich nicht nehmen, ihre Glückwünsche zu überbringen und auf die vergangene Zeit zurückzuschauen. Ganz besonders erfreut waren die Schönstattschwestern, die in Dietershausen als Gastgeber die jungen Männer bei Tagungen und Veranstaltungen rund um das Josef-Engling-Tagungshaus stets herzlich empfangen.

# Neue Aufgaben warten

<figure>
  <img src="https://farm5.staticflickr.com/4419/36719799062_5a8e91907d_z_d.jpg">
</figure>
Beim anschließenden gemeinsamen Abendessen bot sich die Gelegenheit zum Austausch. Bei Gegrilltem wurde so die eine oder andere Anekdote aus längst vergangenen Tagen erzählt. Doch auch Neuerungen, zukünftige Projekte und Aufgaben wurden mit Feuereifer besprochen. Die Jungs von der Mannesjugend ließen es sich natürlich nicht nehmen, den Gratulanten einen kleinen Geschmack von ihrer Arbeit mit den Jugendlichen und Kindern zu geben. Und so klang das Jubiläum bei Gitarrenklängen, Stockbrot und einer gemütlichen Lagerfeuerrunde aus.

<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157685774148744">
  <a href="https://www.flickr.com/photos/45962678@N06/36750950541/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4340/36750950541_03edeacdf6_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4340/36750950541_03edeacdf6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056399974/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4347/36056399974_30fbf73b2a_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4347/36056399974_30fbf73b2a_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36750948841/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4397/36750948841_9f29fabb48_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4397/36750948841_9f29fabb48_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36719798402/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4371/36719798402_8feccb51e9_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4371/36719798402_8feccb51e9_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056399124/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4390/36056399124_8cf77f06ed_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4390/36056399124_8cf77f06ed_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36750948101/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4420/36750948101_6ee40d0775_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4420/36750948101_6ee40d0775_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056398444/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4397/36056398444_0a5d5727d9_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4397/36056398444_0a5d5727d9_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056398054/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4405/36056398054_a3454fccba_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4405/36056398054_a3454fccba_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056397814/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4395/36056397814_f1f6885f37_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4395/36056397814_f1f6885f37_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36750947291/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4344/36750947291_bf5758015b_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4344/36750947291_bf5758015b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056397264/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4346/36056397264_5129b509fe_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4346/36056397264_5129b509fe_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056397394/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4338/36056397394_bccff09de6_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4338/36056397394_bccff09de6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36056372234/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4427/36056372234_06298ab21a_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4427/36056372234_06298ab21a_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/36719828262/in/album-72157685774148744"><img src="https://farm5.staticflickr.com/4362/36719828262_f02f78a21c_q.jpg"
alt="" data-src-large="https://farm5.staticflickr.com/4362/36719828262_f02f78a21c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157685774148744" class="flickr-link" title="Bildergalerie 50 Jahre SMJ-Fulda auf Flickr">Bildergalerie</a>
</figure>
