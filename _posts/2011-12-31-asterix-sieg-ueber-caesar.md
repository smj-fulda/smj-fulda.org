---
tags: [smjfulda, gewo]
migrated:
  node_id: 897
  migrated_at: 2017-02-20 22:27
  alias: artikel/897-„asterix-sieg-über-cäsar“
  user_id: 84
tagline: Gemeinschaftswochenende 16.–18. Dezember 2011
author: Marius
title: "„Asterix Sieg über Cäsar“"
created_at: 2011-12-31 01:51
excerpt: Am Freitag kamen 16 Jungen im Alter von 9–13 Jahren nach Dietershausen zum Gemeinschaftswochenende der Schönstatt Mannesjugend. Nach dem Abendessen und der Begrüßung im Heiligtum ging es in den Roten Saal. Hier hatten wir jede Menge Spaß bei Aktions- und Kennlernspielen. Nach dem Abendgebet wurde es sehr schnell sehr still auf den Fluren und die wohlverdiente Nachtruhe begann.
---
<p>Am Freitag kamen 16 Jungen im Alter von 9&ndash;13 Jahren nach Dietershausen zum Gemeinschaftswochenende der Schönstatt Mannesjugend. Nach dem Abendessen und der Begrüßung im Heiligtum ging es in den Roten Saal. Hier hatten wir jede Menge Spaß bei Aktions- und Kennlernspielen. Nach dem Abendgebet wurde es sehr schnell sehr still auf den Fluren und die wohlverdiente Nachtruhe begann.</p>
<p>Nach einer viel zu kurzen Nacht folgte schon der Frühsport, der uns wach und fit für die Aufgaben des Tages machte. Nach dem stärkenden Frühstück folgte ein Referat von Philipp über das Thema &bdquo;sich für andere einsetzen&ldquo;. In der nachfolgenden Gruppenstunde redeten wir darüber, wo wir uns im Alltag für andere einsetzen können oder wie wir anderen helfen könnten. Nach dem Mittagessen folgte die erholende Mittagspause. Nun begann auch schon die &bdquo;Wildsaujagd&ldquo;. Die Jungs mussten Zettel mit Zahlen finden auf deren Rückseite sich ein Code-Wort befand. Nun mussten sie das Wort vortragen und durften bei richtigem Wort und richtig beantworteten Fragen ein weiteres Mal würfeln. Zwischenzeitlich legten wir eine Kuchen-Pause ein um uns zu stärken. Danach ging es zum Endspurt, die Wildsaujagd kam zum Finale und die Sieger standen fest. Nach dem Abendessen ging es in die Kirche. Hier feierten wir mit Pfarrer Jens Clobes die Heilige Messe zum 4. Advent. Den Abschluss fand der anstrengende Tag in der Siegerehrung der Wildsaujagd, ein paar kleinen Spielen und dem Abendgebet.</p>
<p>Es ist Sonntagmorgen, im Kappelchen beginnt der letzte Tag des Gemeinschaftswochenendes mit dem Morgengebet und dem nachfolgenden Frühstück. Im Roten Saal ging es los mit dem Basteln der Weihnachtsengel aus Styropor, Bast, Gips und Holzscheit. Als alle Engel fertig waren, wurden die Zimmer ausgeräumt und es ging zum Mittagessen. Nach dem Essen ging es für alle Jungs zurück nach Hause.</p>
