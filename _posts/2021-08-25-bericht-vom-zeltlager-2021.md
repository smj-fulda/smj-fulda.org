---
title: Mit Göttern und Super Mario in der Rhön
tagline: Bericht vom Zeltlager 2021
author: Christian Schopp
tags: [zeltlager]
image:
  cover: https://live.staticflickr.com/65535/51432890261_7b3b8c7c93_k_d.jpg
  align: top
---

Müde aber glücklich sind wir am Freitag nach Hause gekommen. Hinter uns lagen zwei spannende Wochen voller neuer Erfahrungen. Jeden Tag galt es, unbekannte Herausforderungen zu bestehen. Am Ende aber haben sich alle gefreut, beim Zeltlager 2021 dabei gewesen zu sein.

<figure data-href="https://www.flickr.com/photos/straight-shoota/51433857340/in/album-72157719791153101/">
  <img src="https://live.staticflickr.com/65535/51433857340_aaafdf35f3_w_d.jpg">
</figure>
<figure data-href="https://www.flickr.com/photos/straight-shoota/51433632144/in/album-72157719791153101/">
  <img src="https://live.staticflickr.com/65535/51433632144_c2a29b0209_w_d.jpg">
</figure>
<figure data-href="https://www.flickr.com/photos/straight-shoota/51433130233/in/album-72157719791153101/">
  <img src="https://live.staticflickr.com/65535/51433130233_82e9d2b629_w_d.jpg">
</figure>

Eigentlich hatten wir einen Zeltplatz in Schönstatt bei Vallendar, unserem Ursprungsort, gebucht. Diesen mussten wir aber aufgrund der Pandemie absagen. Die Vorbereitungen gestalteten sich auch alles andere als einfach, weil nicht abzusehen war, unter welchen Voraussetzungen ein Lager überhaupt möglich gemacht werden kann. Und trotzdem waren die Gruppenleiter, ZBVs und alle anderen Helfer während der sechs Vorbereitungstreffen mit großem Eifer dabei. Und wie wir jetzt wissen, sollte sich die Arbeit auch lohnen.

In der ersten Woche (11. bis 13. August) standen zunächst die Lagerbauten an. Das Schwarzes Brett, der Glockenständer und der Fahnenmast mussten von den einzelnen Zeltgruppen gebaut werden. Am Donnerstag fand das Wagenrennen statt. Hierzu mussten wir uns zunächst unsere eignen Streitwagen bauen, mit denen wir dann gegeneinander antraten. Am Freitag folgte ein Orientierungslauf durch die Rhön. Erschöpft kamen dann auch alle Gruppen am Guckaisee an und erholten sich im kühlen Nass. Mit einer gemeinsamen Lagerfeuerrunde ging das Programm schließlich zu Ende. Bestechend schön war in der ersten Woche vor Allem das Wetter. Es regnete lediglich an einem Abend. Und das auch nur kurz.

Die zweite Woche (16. bis 20. August) startete mit einer Runde Mario Party. Da Spielekonsolen bei uns verboten sind, haben wir das Spiel kurzerhand zu einer Echtversion mit Menschen als Spielfiguren umgestaltet. Es hat eine Menge Spaß gemacht. Leider konnten wir aber nur wenige Runden spielen. Überhaupt ist das Wort „leider“ in der zweiten Woche recht häufig gefallen. Meistens hatte das mit dem Wetter zu tun. Den Hauptprogrammpunkt am Dienstag konnten wir nicht stattfinden lassen. Außer dem Titel „Die Eroberung Trojas“ wollen wir aber nicht mehr verraten, weil die Aktion einfach im nächsten Jahr stattfinden soll. Immerhin konnten wir eine abgespeckte Form unseres traditionellen Fußballspiels durchführen. Die Betreuer gewannen knapp mit 2:1 gegen die Jungs. Am Mittwoch stand dann das Tagesspiel auf dem Programm. Alle Betreuer verkleideten sich als Götter oder Fabelwesen der griechischen Welt. Die Jungs hatten dann die Aufgabe, den Herrscherblitz von Zeus zurück zu erobern. Um erstmal herauszufinden, wer diesen gestohlen hatte, mussten allerhand Aufgaben gelöst werden. Am Donnerstag in der zweiten Woche stand dann die Ritterweihe an. Das ist der geistliche Höhepunkt des Lagers. Elf Jungs können sich jetzt voller Stolz „Ritter Mariens“ nennen. Und am Freitag waren schon die Eltern da um die müden Kämpfer nach Hause zu holen.

Auch wenn wir noch einige Einschränkungen hinzunehmen hatten, war es wieder ein sehr sehr schönes Lager.

Vielen Dank an alle Beteiligten.

Hoffentlich sehen wir uns im nächsten Jahr wieder!

<figure class="full-width" data-href="https://www.flickr.com/photos/straight-shoota/51433130468/in/album-72157719791153101/">
  <img src="https://live.staticflickr.com/65535/51433130468_f05e3bf82d_c_d.jpg">
</figure>
