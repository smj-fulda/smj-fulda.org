---
tags: [smjfulda, zeltlager]
migrated:
  node_id: 1006
  migrated_at: 2017-02-20 22:28
  alias: artikel/1006-abenteuer-leben-–-eine-unerwartete-reise
  user_id: 1
tagline: Zeltlager 2015 in Schönstatt
title: Abenteuer Leben – Eine unerwartete Reise
created_at: 2015-08-26 11:52
excerpt: Im Zeltlager der SMJ-Fulda brachen dieses Jahr 32 junge Abenteurer auf zu einer Kreuzfahrt ins Glück. Durch einen Sturm auf einer einsamen Insel gestrandet mussten sie dort ihr Überleben sicherstellen und sich mit den merkwürdig aussehenden Inselbewohnern arrangieren. Die Jungs errichteten ein Lager, erkundeten die Insel und eigneten sich Fähigkeiten an, die ihnen nützlich sein können. Schließlich mussten sie ihre kleine Siedlung auch gegen Eindringlinge verteidigen. Am Ende konnten sie die Insel aber wieder verlassen und gemeinsam mit ihren Eltern in die Heimat zurückkehren. Was für ein Abenteuer!
---
<p>Im Zeltlager der SMJ-Fulda brachen dieses Jahr 32 junge Abenteurer auf zu einer Kreuzfahrt ins Glück. Durch einen Sturm auf einer einsamen Insel gestrandet mussten sie dort ihr Überleben sicherstellen und sich mit den merkwürdig aussehenden Inselbewohnern arrangieren. Die Jungs errichteten ein Lager, erkundeten die Insel und eigneten sich Fähigkeiten an, die ihnen nützlich sein können. Schließlich mussten sie ihre kleine Siedlung auch gegen Eindringlinge verteidigen. Am Ende konnten sie die Insel aber wieder verlassen und gemeinsam mit ihren Eltern in die Heimat zurückkehren. Was für ein Abenteuer!</p>
