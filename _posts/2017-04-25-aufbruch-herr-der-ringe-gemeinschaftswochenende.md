---
title: "Aufbruch! Gemeinschaftswochenende"
author: 
tags: [gewo, smjfulda]
image:
    cover: "!baseurl!/images/2017/2017-04-25-flyer-gewo-mai_front.jpg"
---
Das Gemeinschaftswochenende der SMJ-Fulda beginnt am 19. Mai 2017 um 17:30 Uhr und endet am 20. Mai um 16:30 Uhr nach der Messe.
Es erwarten dich aufregende Aktionen wie Klettern und weitere spannende Spiele. Erlebe die Gemeinschaft des Ringes, übernachte zusammen mit deinen Gefährten und zeigt, wie stark ihr zusammen seid!

<blockquote><p>Sauron ist zurückgekehrt, Orks versammeln sich. Es ist Zeit für die freien Völker Mittelerdes, sich zu vereinen.
Elrond hat dich zu seinem Rat nach Bruchtal gerufen. Werde Teil der Gemeinschaft des Ringes und hilf, das Böse zu bekämpfen.</p></blockquote>

<figure class="full-width"><img src="{{ site.baseurl }}/images/2017/2017-04-25-flyer-gewo-mai_back.jpg" /></figure>
