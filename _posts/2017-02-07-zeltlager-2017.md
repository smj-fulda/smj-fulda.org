---
title: Zeltlager 2017
tags: [smjfulda, zeltlager]
author: Johannes Müller
image:
  cover: "!baseurl!/aktionen/zeltlager/2017/cover_crop.jpg"
link: "!baseurl!/zeltlager"
---
Unser Zeltlager 2017 steht unter dem Thema *Der Herr der Ringe* und findet in den hessischen Sommerferien vom 4. bis 15. Juli statt. Wir sind dieses Jahr zum ersten Mal auf dem Zeltplatz in Thalwenden bei Heilbad Heiligenstadt.
