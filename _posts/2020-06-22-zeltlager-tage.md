---
title: "Zeltlager in Zeiten von Corona"
tagline: Ferienprogramm der SMJ-Fulda 2020
author: Johannes Müller
tags: [zeltlager]
image:
  cover: https://live.staticflickr.com/65535/48759165132_c2598c8653_k_d.jpg
  align: top
---

Wir haben uns lange Gedanken gemacht, wie wir in der aktuellen Situation unser Zeltlager durchführen können.

Viele andere Ferienfreizeiten sind schon früh komplett abgesagt worden sind, aber wir wollten nicht einfach die Flinte ins Korn werfen.
Gespräche mit Eltern haben uns dazu ermutigt, so es irgendwie geht in den Sommerferien ein SMJ-Programm anzubieten.

<strong>Das Zeltlager wird dieses Jahr in anderer Form stattfinden. Auf Basis der vom Bistum Fulda veröffentlichten Richtlinien für die Jugendarbeit
bieten wir einzelne Tagesveranstaltungen ohne Übernachtung an. Das heißt die Teilnehmer kommen morgens und gehen abends wieder.</strong>

Inhaltlich wird das Programm ähnlich wie gewohnt, natürlich mit Berücksichtigung der aktuellen Regelungen zum Infektionsschutz und der Hygienevorschriften.
Manches wird etwas anders laufen als sonst, aber das hindert uns nicht daran, spannende Tage mit Action, Freundschaft und Herausforderung zu erleben.

## Termine

Die Zeltlagertage sind über die gesamten Ferien verteilt. In den ersten beiden Wochen sind es zwei bzw. drei aufeinanderfolgende Tage, danach
jeweils Donnerstags bis zum Ende der Ferien.

Es wird ein durchgehendes Programm geben, aber es ist ohne weiteres möglich, nur an einzelnen Tagen teilzunehmen. Die Anmeldung erfolgt jeweils tagesweise.

* Montag, 6. Juli (Dietershausen)
* Dienstag, 7. Juli (Dietershausen)
* Dienstag, 14. Juli (Dietershausen)
* Mittwoch, 15. Juli (Dietershausen)
* Donnerstag, 16. Juli (Dietershausen)
* Donnerstag, 23. Juli: Wanderung
* Donnerstag, 30. Juli (Dietershausen)
* Donnerstag, 6. August (Dietershausen)
* Donnerstag, 13. August: Fahrradtour

Sofern nicht anders angegeben ist der Beginn jeweils morgens um 11:00 Uhr im Schönstattzentrum Dietershausen. Abends wird es üblicherweise ein Lagerfeuer geben mit variabler Abholzeit zwischen 21:00 und 23:00 Uhr.

Treffpunkte für die Wanderung und Fahrradtour werden noch bekannt gegeben.

Teilnehmergebühr beträgt 20€ pro Veranstaltungstag, ab dem vierten Tag 15€.

<figure class="full-width">
    <img src="https://live.staticflickr.com/65535/50033007438_e0c63cafe5_c_d.jpg" />
    <figcaption>Virtuelle Zeltlagervorbereitung</figcaption>
</figure>

Veranstalter ist die Schönstatt Mannesjugend (SMJ) im Bistum Fulda. Als Teil der internationalen Schönstattbewegung sind wir ein christlicher Jugendverband und bieten Veranstaltungen für Kinder und Jugendliche an. Weitere Informationen und Anmeldung bei Lagerleiter Christian Schopp ([christian.schopp@smj-fulda.org](mailto:christian.schopp@smj-fulda.org)).

<a href="/zeltlager" class="btn">Mehr zum Zeltlager</a>

<a href="https://files.smj-fulda.org/zeltlager/hygienekonzept-SMJ-Fulda-2020.pdf" class="btn">Hygienekonzept</a>

<figure class="full-width">
  <div style="background: #000315;text-align: center;font-family: Arial, Helvetica, sans-serif;color: #fff;padding: 3em 1em;letter-spacing: -.03em; margin-bottom: 1rem; font-weight: bold;">
    <div style="">
      <span style="font-size: 4rem; vertical-align: middle;">
        Die drei
      </span>
      <span style="font-size: 9rem; vertical-align: middle; font-stretch: ultra-condensed; letter-spacing: -0.02em; line-height: 1;">
        <span style="color: #007eff;">?</span>?<span style="color: #fd3434;">?</span>
      </span>
    </div>
    <div style="font-size: 1.5rem; margin: .5rem 0; color: #ddd;">Auf Spurensuche im Zeltlager der SMJ-Fulda</div>
  </div>
</figure>
