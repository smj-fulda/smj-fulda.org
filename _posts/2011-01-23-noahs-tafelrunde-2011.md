---
categories:
- SMJ-Fulda
- Noahs Tafelrunde
- iPray
- '2011'
migrated:
  node_id: 844
  migrated_at: 2017-02-20 22:26
  alias: artikel/844-noahs-tafelrunde-2011
  user_id: 1
tagline: 'Jahresplanungstreffen der SMJ-Fulda: 33 Veranstaltungen geplant. Erstes Mal Lobpreis iPray'
image:
  cover: https://farm6.staticflickr.com/5161/5381286921_20d86ae294_b.jpg
title: Noahs Tafelrunde 2011
created_at: 2011-01-23 23:29
excerpt: Dieses Wochenende kamen die Gruppenführer der SMJ-Fulda zusammen, um im <a href="/zentrum">Dietershausener Schönstattzentrum</a> an Noahs Tafelrunde die Jahresplanung für 2011 zu entwerfen. Insgesamt waren 20 Gruppenführer dem Aufruf von Diözesanführer Tobias Büdel folgend am Freitag abend im Josef-Engling-Haus zusammengekommen. Bevor das eigentliche Programm am Samstag erarbeitet werden sollte, wurde der Abend zur Pflege der Gemeinschaft genutzt. Um sich gegenseitig wieder besser kennen zu lernen und auch die neuen Gruppenführer in die Gemeinschaft zu integrieren, gab es eine Austauschrunde über die aktuelle Lebenssituation der Teilnehmer.
---
<p>Dieses Wochenende kamen die Gruppenführer der&nbsp;SMJ-Fulda zusammen, um im <a href="/zentrum">Dietershausener Schönstattzentrum</a> an Noahs Tafelrunde die Jahresplanung für 2011 zu entwerfen.</p>

<figure data-href="https://www.flickr.com/photos/45962678@N06/5381897952">
  <img src="https://farm6.staticflickr.com/5169/5381897952_8f84a7da37_c.jpg" class="flickr-img flickr-img--medium" alt="" />
  <figcaption> (<time datetime="2011-01-21 21:04:43">2011</time>)</figcaption>
</figure>

<p>Insgesamt waren 20 Gruppenführer dem Aufruf von&nbsp;Diözesanführer Tobias Büdel folgend am Freitag abend im Josef-Engling-Haus zusammengekommen. Bevor das eigentliche Programm am Samstag erarbeitet werden sollte, wurde der Abend zur Pflege der Gemeinschaft genutzt. Um sich gegenseitig wieder besser kennen zu lernen und auch die neuen Gruppenführer in die Gemeinschaft zu integrieren, gab es eine Austauschrunde über die aktuelle Lebenssituation der&nbsp;Teilnehmer.</p>
<!--more--><figure data-href="https://www.flickr.com/photos/45962678@N06/5381314775">
  <img src="https://farm6.staticflickr.com/5163/5381314775_d21de76f52_c.jpg" class="flickr-img flickr-img--medium" alt="" />
  <figcaption> (<time datetime="2011-01-21 22:50:33">2011</time>)</figcaption>
</figure>
<p>Höhepunkt des Abends war die Vorpremiere des Lobpreis-Konzepts <a href="/ipray">iPray</a>, das der Kreis <em>Ritter Mariens</em> an den vergangenen Kreistagungen erarbeitet hat und dieses Jahr als Tournee durch verschiedene Pfarreien ziehen soll. Aufgrund der hohen Raumbelegung kurzerhand in den Speisesaal des Josef-Engling-Hauses gelegt und durchweg noch leicht improvisiert, konnte das Konzept aber durchaus überzeugen und die Arbeit des Kreises hat sich gelohnt und wird sicherlich Früchte tragen.</p>
<p>Der Samstag stand dann ganz im Zeichen der praktischen Arbeit:&nbsp;Wie wollen wir die Aktionen der SMJ-Fulda im kommenden Jahr gestalten?&nbsp;Wer übernimmt Verantwortungen?&nbsp;Und was tun wir, um wieder mehr Zulauf für Zeltlager und Gemeinschaftswochenenden zu bekommen?&nbsp;Rund um diese zentralen Fragen entstanden viele Diskussionen und in kollegialer Atmosphäre konnten viele gute Ideen entstehen.</p>
<p>Der oft vernachlässigte Bereich Werbung stand dieses Jahr besonders im Mittelpunkt:&nbsp;Nach teilnehmermäßig relativ schwachen Zeltlagern in den letzten beiden Jahren, müssen neue Konzepte her, um Kinder und Eltern auf unsere Arbeit aufmerksam zu machen und für unsere Veranstaltungen überzeugen zu können. Im Ergebnis standen mehrere teils sehr unterschiedliche Werbekonzepte:&nbsp;von Präsenz in den Heimatpfarreien über Schönstatt-interne Maßnahmen und Schulaktionen bis hin zu Kuchenverkauf in der Fuldaer Innenstadt.</p>
<p>Auf dem Terminkalender stehen dieses Jahr 33 Veranstaltungen, die von der SMJ&nbsp;ausgerichtet oder mitveranstaltet werden oder deren Teilnahme empfehlenswert ist.</p>
