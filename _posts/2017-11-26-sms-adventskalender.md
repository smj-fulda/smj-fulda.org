---
title: "Alle Jahre wieder: Der SMS-Adventskalender"
tagline: Jeden Tag ein Türchen aufs Handy
tags: [smjfulda, adventskalender]
author: Sebastian Hartmann
image:
    cover: "!baseurl!/images/pages/adventskalender/kerzen.jpg"
link: "!baseurl!/adventskalender"
---
Liebe Bekannte, Freunde und Mitglieder der Schönstatt-Mannesjugend Fulda,
auch in diesem Advent gibt es wieder unseren SMS-Adventskalender. Täglich werden Türchen mit einem geistigen Impuls verschickt. Einen schönen Advent wünscht die SMJ-Fulda!
