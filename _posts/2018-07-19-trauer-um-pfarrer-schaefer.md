---
title: Trauer um unseren Standesleiter Pfarrer Ulrich Schäfer
tags: [smjfulda]
author: Johannes S. Wende
excerpt: >
  Unser langjähriger Standesleiter Pfarrer Ulrich Schäfer ist plötzlich verstorben. Wir trauern um einen wunderbaren Priester. Er war für uns Vorbild, Freund, Begleiter, Weggefährte.
image:
  cover: https://farm5.staticflickr.com/4601/38275289404_3a6e60b39d_o_d.jpg 
---

Gestern Morgen ist unser Standesleiter Pfarrer Ulrich Schäfer im Alter von 50 Jahren viel zu früh gestorben.

Wir können unsere Gefühle derzeit kaum in Worte fassen. 

Wir trauern um einen wunderbaren Priester, der sein ganzes Leben voll Hingabe dem Schönstattwerk geschenkt hat. Er war für uns Vorbild, er hat unsere Arbeit begleitet, war immer für uns da, er wusste immer weiter, wenn wir mit Problemen zu ihm kamen. Er war für uns Freund, Begleiter, Weggefährte.

Er war bei so vielen Zeltlagern dabei, die er als Lagerpriester begleitet hat, davor war er als Junge, als Gruppenleiter, als Diözesanführer dabei. Also eigentlich schon immer.

Zeltlager ohne ihn war und ist nicht zu denken.

Man merkte, dass er mit Freude dabei war. 

Er konnte wunderbar mit den Kindern und Betreuern umgehen - man merkte, dass sein Herz für all diese jungen Menschen schlug.
Er hatte spirituellen Tiefgang, Humor und eine wunderbare Pädagogik, die ganz im Sinne unseres Gründervaters PJK war. Er führte, indem er begleitete ohne dabei vereinnahmend zu sein. Er ließ uns immer die Freiheit und gerade deswegen hörte man besonders auf das, was er uns mitgab.

Auch auf unseren Kreistagungen war er oft und gerne dabei. Wir haben seine Impulse und seine Predigten immer sehr gemocht, denn sie waren so lebensnah und man konnte gar nicht weghören.

Er hat so viele Generationen SMJ´ler geprägt. Jeder war für ihn wichtig, und er nahm jeden an, wie er war.

Uli, wir vermissen Dich sehr!

Ich möchte Euch alle, die Ihr das lest um Euer Gebet für unseren Uli bitten. 
Möge er das Ziel erlangen, worauf er sein Leben hingearbeitet hat, wofür er sich sein ganzes Leben hingegeben hat.

Servus mariae nunquam peribit.

Uli - wir sehen uns!

Mater perfectam habebit curam.

![](https://farm1.staticflickr.com/942/29635647148_691e0dc44b_z_d.jpg)
