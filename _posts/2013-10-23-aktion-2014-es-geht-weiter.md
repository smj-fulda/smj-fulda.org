---
tags: [smjfulda, jubiläum2014, video]
title: Aktion 2014 – Es geht weiter!
author: Kilian Machill
---
Letztes Wochenende fand ein überdiözesanes Planungswochenende zu unserer Aktion 2014 in Dietershausen statt. Es wurden viele Ideen ausgetauscht und angeregt diskutiert. Zuviel über Ergebnisse soll aber noch nicht verraten werden. Man darf auf jeden Fall gespannt sein! Vielleicht gibt ja dieses Bild Aufschluss über anstehende Aktionen.

<iframe width="560" height="315" src="https://www.youtube.com/embed/buryAwgS8d8" frameborder="0" allowfullscreen></iframe>

Kleine Spruchsammlung des Wochenendes:

* „Das geht doch gar nicht!“
* „Sowas gibt’s nur im Fernsehen!“
* „Weiß jemand wo Marius ist?“
* „Warum haben wir bloß solange geschlafen?“
* „Mist, ich hab keinen Wurm!“
