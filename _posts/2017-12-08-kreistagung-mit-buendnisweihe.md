---
title: "Kreistagung mit Bündnisweihe"
excerpt: >
  Am vergangenen Wochenende fand im Schönstattzentrum Dietershausen die Dezember-Kreistagung der SMJ-Fulda statt. Ein besonderer Moment war am Freitagabend die Mitgliederweihe von drei SMJlern.
author: Johannes Müller
tags: [smjfulda, kreise, liebesbündnis]
image:
    cover: "https://farm5.staticflickr.com/4594/24126734517_35390cab62_o_d.jpg"
    align: top
---

Am vergangenen Wochenende fand im Schönstattzentrum Dietershausen die Dezember-Kreistagung der SMJ-Fulda statt. Getroffen haben sich die Kreise Jonas und Steffian, sowie der neugegründete Kreis von Johannes Wende. Ebenso tagte auch der Senat mit älteren SMJlern.

Ein besonderer Moment war am Freitagabend die Mitgliedsweihe von Simon Wawra, Johannes Wende und Jonas Wolf. Die Mitgliedsweihe bedeutet eine Vertiefung des Liebesbündnisses und ist in der SMJ die höchste Stufe nach der Ritterweihe (Liebesbündnis) und der Mitarbeiterweihe. SMJler mit Mitgliedsweihe wollen sich besonders für die SMJ engagieren und Verantwortung übernehmenm, um mit ihrem Einsatz zu einer erfolgreichen Arbeit beizutragen.

<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157667333229579">
  <a href="https://www.flickr.com/photos/45962678@N06/38275289404/in/album-72157667333229579"><img src="https://farm5.staticflickr.com/4601/38275289404_c32cb6e2fa_q.jpg" alt="2017-12-08 Kreistagung-001-0452" data-src-large="https://farm5.staticflickr.com/4601/38275289404_c32cb6e2fa_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/38104576515/in/album-72157667333229579"><img src="https://farm5.staticflickr.com/4545/38104576515_d8e09010e1_q.jpg" alt="2017-12-08 Kreistagung-002-0457" data-src-large="https://farm5.staticflickr.com/4545/38104576515_d8e09010e1_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/24126736087/in/album-72157667333229579"><img src="https://farm5.staticflickr.com/4528/24126736087_278d63884e_q.jpg" alt="2017-12-08 Kreistagung-003-0465" data-src-large="https://farm5.staticflickr.com/4528/24126736087_278d63884e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/38104575865/in/album-72157667333229579"><img src="https://farm5.staticflickr.com/4599/38104575865_0c2ae75217_q.jpg" alt="2017-12-08 Kreistagung-004-0468" data-src-large="https://farm5.staticflickr.com/4599/38104575865_0c2ae75217_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/24126734517/in/album-72157667333229579"><img src="https://farm5.staticflickr.com/4594/24126734517_290cde3162_q.jpg" alt="2017-12-08 Kreistagung-005-0470" data-src-large="https://farm5.staticflickr.com/4594/24126734517_290cde3162_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/24126732627/in/album-72157667333229579"><img src="https://farm5.staticflickr.com/4683/24126732627_d6bf0145cb_q.jpg" alt="2017-12-08 Kreistagung-006-0472" data-src-large="https://farm5.staticflickr.com/4683/24126732627_d6bf0145cb_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/24126731457/in/album-72157667333229579"><img src="https://farm5.staticflickr.com/4536/24126731457_52a99e260d_q.jpg" alt="2017-12-08 Kreistagung-007-43" data-src-large="https://farm5.staticflickr.com/4536/24126731457_52a99e260d_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157667333229579" class="flickr-link" title="Bildergalerie SMJ-Kreistagung und Bündnisweihe | 8. Dezember 2017 auf Flickr">Bildergalerie</a>
</figure>
