---
title: Bonifatiuswallfahrt 2018
tags: [smjfulda, jesus-shirt]
author: Johannes Müller 
image:
  cover: https://farm3.staticflickr.com/2896/14391285072_3bfb0629e0_o_d.jpg 
---

Auch 2018 wollen wir gemeinsam zum Grab des hl. Bonifatius laufen. 

Herzliche Einladung an alle Jugendlichen, junge Erwachsene sich auf den Weg zu machen. Das ist auch eine gute Gelegenheit, sein JESUS-Shirt in die Welt zu tragen.

Start ist wie immer unterhalb der Propstei Johannesberg ([50.522612, 9.661714](https://www.google.de/maps/@50.5225792,9.6617301,20z)).

Los geht's um 8.00 Uhr. Wir laufen etwa eine Stunde durch die Johannisaue zum Fuldaer Dom. Auf dem Domplatz beginnt dann um 9:30 das feierliche Pontifikalamt mit Bischof Heinz Josef Algermissen und Festprediger Bischof Dr. Georg Bätzing (Limburg) sowie tausenden Pilgern aus dem ganzen Bistum.
