---
tags: [smjfulda, jubiläum2014]
migrated:
  node_id: 951
  migrated_at: 2017-02-20 22:27
  alias: artikel/951-die-menschen-sind-erstaunt:-wann-erlebt-man-das-heute-noch
  user_id: 8
tagline: Ein Interview mit Steffen Büdel über die Aktion 2014
author: kilian
title: 'Die Menschen sind erstaunt: Wann erlebt man das heute noch?'
created_at: 2013-08-14 21:19
excerpt: Ein Interview mit Steffen Büdel über die Aktion 2014.
---
<p>Ein Interview mit Steffen Büdel über die Aktion 2014.</p>
<p><strong>Kurz und knapp: Was ist die Aktion 2014, was macht sie besonders und was bedeutet sie persönlich für Dich?</strong><br />
	Die Aktion 2014 haben wir als Schönstatt-Mannesjugend Fulda im Oktober 2012 für das 100-jährige Jubiläum der Schönstatt-Bewegung gestartet. Wir wollen 2014 ehrenamtlich geleistete Stunden sammeln. Diese Stunden können Gebetsstunden, Arbeitseinsätze oder ganz verschiedene Dinge sein, die man nicht oder vielleicht nicht so oft macht. Dafür haben wir auch ein Puzzle mit 2000 Teilen entworfen. Jedes Teil soll eine Stunde symbolisieren und der Rahmen dann die letzten 14 Teile. Das Bild zeigt den Säulenplatz der SMJ in Schönstatt. Bei dieser Aktion wollen wir besonders anderen Menschen helfen oder an sie im Gebet denken. Gutes tun. Das ist das was mich dabei bewegt. Weil wir in einer Gesellschaft leben, wo das oft zu kurz kommt. Sich einander helfen und unterstützen. Ich finde diese Aktion einfach klasse!</p>
<p><strong>Was für Aktionen gab es bisher?</strong><br />
	Aktionen wie Gebetsstunden laufen immer mal wieder, Streichaktionen, Arbeitseinsätze im Schönstattzentrum Dietershausen oder das Helfen bei der Betreuung im Antoniusheim in Fulda. Die Erneuerung des Bildstockes in Dietershausen war auch eine super Aktion. Bei solchen Aktionen ist es cool, dass man in Gemeinschaft etwas zusammen macht und dabei natürlich auch ne Menge Spaß hat. Daher suchen wir auch immer nach möglichen Aktionen, die wir machen können.</p>
<p><strong>Was würdest du am liebsten mal machen?</strong><br />
	Ich würde auf jeden Fall mal gerne eine Aktion mit vielen Leuten gleichzeitig machen. Wenn 20 Leuten oder so dabei sind., das wäre echt mal genial! Ich fände auch mal cool einfach von Haus zu Haus zu gehen und mit fremden Menschen ins Gespräch zu kommen. Vielleicht besonders ältere Menschen, die auch einsam sind, aber viel zu erzählen haben. Erzählungen, die auch für uns junge Menschen ein Impuls sein können! Einfach da sein, zuhören und einander erzählen.</p>
<p><strong>Wie ist der Stand der Stunden? Hand aufs Herz, packt ihr das noch?</strong><br />
	Wir schauen nicht jede Woche auf die Anzahl. Im Moment denke ich haben wir ca. 600-700 Stunden gesammelt. Das ist schon echt klasse. Aber natürlich wollen wir mehr. Bis zum Oktober 2014 wird das ein hartes Stück Arbeit, aber ich bin optimistisch, dass, wenn wir zusammen anpacken, wir das packen können und werden. Und wenn nicht, geht die Welt auch nicht unter, wir werden auf jeden Fall mindestens diese 2014 Stunden sammeln.</p>
<p><strong>Wie reagieren Menschen die von eurer Aktion hören?</strong><br />
	Oftmals sind Sie erstaunt! Denn wann erlebt man das heute noch? Sie fragen uns immer nach unserem Hintergrund, wieso und warum wir das machen. Die meisten sagen dann, dass wir wieder kommen können und so weiter machen sollen. Das ist für uns auch immer schön, solche Dankesworte und Ermutigungen zu hören. Denn wir sind überzeugt davon, dass das eine sehr gute Sache ist.</p>
<p><strong>Wenn im Oktober nächsten Jahres alles rum ist, wird es dann nochmal so eine Aktion geben?</strong><br />
	Aktionen wird es natürlich weitere geben. Welche genau ist schwer abzuschätzen. Ich könnte mir aber vorstellen, dass diese Aktion weiter fortgeführt wird und wir weiterhin versuchen wollen gutes zu tun. Wir als SMJ-Fulda wollen nicht nur unter uns sein, sondern wollen auch nach Außen treten und zeigen wer wir sind.</p>
<p>Das Interview führte Kilian Machill</p>
