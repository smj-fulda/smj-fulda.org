---
title: "Red Hearts beim SMJ-Cup 2017"
author: Johannes Müller
tags: [smj-cup, smjfulda, redhearts]
image:
    cover: "https://farm5.staticflickr.com/4229/35146378540_1161c139df_k_d.jpg"
---

Am 24. Juni traten im Vallendarer Vredestein-Stadion die Fußball-Mannschaften der Diözesen zum SMJ-Cup an.
Auch die Red Hearts waren natürlich vertreten. Leider waren wir nur eine kleine Mannschaft ohne Auswechselspieler und dann fiel Michael im ersten Spiel gegen SMJ Oberland verletzungsbedingt für den Rest des Turniers aus.

Es war dennoch ein tolles Erlebnis und hat uns viel Freude bereitet, besonders der mit 4:1 deutliche Sieg gegen die starke schweizer Nationalauswahl, die sonst nur gegen den dominierenden Turniersieger Oberland verloren hatte.
Dennoch hat es nach einer unglücklichen Partie gegen Trier nur für den 5. Platz gereicht.
Den zweiten Platz belegte die SMJ-Speyer, die Eidgenossen wurden Dritter.

<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157685501151325">
  <a href="https://www.flickr.com/photos/45962678@N06/34723718053/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4290/34723718053_460f7f1f2a_q.jpg" alt="1944920
9_1473896689319934_1825967490_o" data-src-large="https://farm5.staticflickr.com/4290/34723718053_460f7f1f2a_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/35365898642/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4207/35365898642_2802e8afef_q.jpg" alt="1949006
8_1473896262653310_2135575318_o" data-src-large="https://farm5.staticflickr.com/4207/35365898642_2802e8afef_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/34723715093/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4256/34723715093_320db0891d_q.jpg" alt="1949021
4_1473896315986638_898358729_o" data-src-large="https://farm5.staticflickr.com/4256/34723715093_320db0891d_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/35493889216/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4212/35493889216_e3cefb0411_q.jpg" alt="1949021
4_1473896499319953_865228456_o" data-src-large="https://farm5.staticflickr.com/4212/35493889216_e3cefb0411_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/35146384830/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4282/35146384830_6051d9fbcf_q.jpg" alt="1949570
1_1473846709324932_836768494_o" data-src-large="https://farm5.staticflickr.com/4282/35146384830_6051d9fbcf_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/34723711813/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4229/34723711813_c7f28349ce_q.jpg" alt="1949588
4_1473896729319930_674445178_o" data-src-large="https://farm5.staticflickr.com/4229/34723711813_c7f28349ce_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/34723710443/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4281/34723710443_a6c5eff15f_q.jpg" alt="1949597
5_1473896309319972_1086522449_o" data-src-large="https://farm5.staticflickr.com/4281/34723710443_a6c5eff15f_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/34723709393/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4288/34723709393_5e5e88e588_q.jpg" alt="1949599
7_1473896272653309_372343471_o" data-src-large="https://farm5.staticflickr.com/4288/34723709393_5e5e88e588_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/35146381770/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4211/35146381770_65c132db7e_q.jpg" alt="1949622
2_1473896712653265_322846509_o" data-src-large="https://farm5.staticflickr.com/4211/35146381770_65c132db7e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/34723707683/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4289/34723707683_5eea0e2ee7_q.jpg" alt="1949625
4_1473896289319974_1721655827_o" data-src-large="https://farm5.staticflickr.com/4289/34723707683_5eea0e2ee7_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/35146380510/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4255/35146380510_eb661ebdc5_q.jpg" alt="1949629
2_1473896312653305_2066030651_o" data-src-large="https://farm5.staticflickr.com/4255/35146380510_eb661ebdc5_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/35146379390/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4283/35146379390_8e42b99121_q.jpg" alt="1949640
2_1473896325986637_2093912635_o" data-src-large="https://farm5.staticflickr.com/4283/35146379390_8e42b99121_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/35146378540/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4229/35146378540_a3cdc10e08_q.jpg" alt="1953184
4_1473896825986587_652891886_o" data-src-large="https://farm5.staticflickr.com/4229/35146378540_a3cdc10e08_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/34691720754/in/album-72157685501151325"><img src="https://farm5.staticflickr.com/4255/34691720754_18f211d792_q.jpg" alt="1954979
0_1473897879319815_708679427_o" data-src-large="https://farm5.staticflickr.com/4255/34691720754_18f211d792_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157685501151325" class="flickr-link" title="Bildergalerie SMJ-Cup 2017 auf Flickr">Bildergalerie</a>
</figure>