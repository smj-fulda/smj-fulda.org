---
title: "2019: Begeistert Leben gestalten"
tagline: Durchblick im neuen Jahr
author: Johannes Müller
tags: [smjfulda, durchblick]
image:
  cover: https://farm8.staticflickr.com/7887/45968823344_90c3cb8bd1_k_d.jpg
  align: top
---

Nicht nur das Fuldaer Domkapitel, auch die SMJ Fulda hat gewählt und seit Dezember eine neue Diözesanführung.
Bisher bestand diese aus dem Diözesanführer und dessen Stellvertreter. Erstmals wurden außerdem noch fünf weitere Mitglieder der Diözesanführung gewählt, die für einzelne Ressorts zuständig sind und gemeinsam im Team arbeiten. Damit wollen wir im neuen Jahr unsere Arbeit auf breitere Schultern stellen.

Die Wahlberechtigten der SMJ-Fulda haben Steffen Büdel zum neuen Diözesanführer gewählt. Er hatte dieses Amt bereits von 2012 bis 2015 inne und bringt somit bereits einige Erfahrung mit. Sein Stellvertreter ist Jonas Wolf, der auch schon von 2015 bis 2017 diese Aufgabe hatte.

Der Vorgänger Simon Wawra und Stellvertreter Johannes Wende unterstützen die Diözesanführung gemeinsam mit weiteren Ressortverantwortlichen:

* Lagerleiter: Christian Schopp
* Kassenwart: René Kohl
* Tagungsleiter: Simon Wawra
* Geistliche Begleitung: Johannes Wende
* Kommunikation: Johannes Müller

Durch den plötzlichen [Tod von Pfarrer Ulrich Schäfer](/artikel/2018-07-19-trauer-um-pfarrer-schaefer) im vergangenen Jahr fehlt uns nicht nur ein guter Freund. Für die Aufgabe des Standesleiters suchen wir auch noch einen Nachfolger.

# Was bisher geschah...

Auf der Jahreskonferenz vom 27. bis 31. Dezember hat die SMJ-Deutschland eine neue Jahresparole gefunden:

> Zeit für dich &ndash; begeistert leben gestalten <small>Jahresparole der SMJ-Deutschland</small>

<figure>
    <img src="https://farm5.staticflickr.com/4863/46487607771_4e5019ce5c_z_d.jpg">
</figure>

Vor Weihnachten sind wir am traditionellen Termin 23. Dezember zur [Winterwanderung auf den Kreuzberg](/artikel/2018-12-27-der-berg-ruft-auch-dich.md) gelaufen.

<figure class="float-right">
    <img src="https://farm8.staticflickr.com/7847/46436163262_1c9ed3f352_z_d.jpg" >
</figure>

Am 13. Dezember wurde der neue Bischof von Fulda verkündet: Der Freiburger Weihbischof Michael Gerber. Er ist Schönstatt-Verbandspriester und Standesleiter der MJF Freiburg.

Am zweiten Adventswochenende war Kreistagung in Dietershausen, mit den Kreisen Steffian (Steffen & Christian), WOLF-Gang (Jonas), Wendekreis (Johannes) und der Senat war auch vertreten. Am Freitagabend war die Diözesanführerwahl und anschließend ein gemeinsamer Abend mit Rückblick und Ausblick auf die Arbeit der SMJ-Fulda. Am Samstagabend waren einige Kreise beim Nightfever in Fulda. Kreis WOLF-Gang [bereitet sich auf die Mitarbeiterweihe](/artikel/2018-12-09-mitarbeiter-gesucht) vor.

# Ausblick

In den kommenden Monaten macht sich die neue Diözesanführung an die Arbeit.
Das Zeltlager muss geplant werden, ebenso die Gemeinschaftswochenenden und weitere Veranstaltungen.

* Am 12. Januar nehmen einige Gruppenleiter an einer **Präventionsschulung** teil, die für uns und die MJF in Dietershausen angeboten wird.
* **Begegnungstag** am 27. Januar 2019 ab 13:30 im Schönstattzentrum Dietershausen: Treffen der gesamten Schönstatt-Familie Fuldas. Es werden verschiedene Projekte vorgestellt, unter anderem unsere Arbeitseinsätze am Zentrum und die Apfelsaftaktion der MJF.
* Am Josef-Engling-Haus in Dietershausen wird es dieses Frühjahr ernst: Der Erweiterungsbau nimmt konkrete Formen an. Es soll bald losgehen! Gebraucht werden dafür auch wieder einsatzkräftige Helfer.
* Vom 8. bis 10. März haben wir gleich zwei Veranstaltungen im Josef-Engling-Haus: Das **Gemeinschaftswochenende** für 9 bis 14-jährige. 
* Parallel dazu findet der **Glaubenskurs** für Jugendliche ab 16 statt. Dieser wird gemeinsam mit der SchönstattMJF, der Katholischen Jugend im Bistum Fulda und der OMI-Jugend ausgerichtet.
* Eine Woche später ist schon die erste **Kreistagung**. Mitglieder von Kreis WOLF-Gang wollen am Samstag das Mitarbeiterbündnis schließen.
* Im März und April starten wir mit einem neuen Format: Unter dem Titel **Die SMJ stellt sich vor** bieten wir an verschiedenen Orten Informationstreffen an, um interessierte Eltern und Kinder über unsere Arbeit zu informieren und zu unseren Veranstaltungen einzuladen. 17. März 14:30 in Dietershausen, 24. März 17:00 in Jossgrund-Oberndorf, 7. April 14:00 in Hilders-Eckweisbach.
* Am 31. März wird unser neuer **Bischof Michael Gerber** feierlich in sein Amt eingeführt. Als Schönstätter werden wir natürlich auch teilnehmen und ihn in unserem Bistum willkommen heißen.
* Für die Woche nach Ostern planen wir eine **Regiofahrt** nach Cambrai. Gemeinsam mit der SMJ Mainz/Limburg und der SMJ Speyer wollen wir dort den Spuren Josef Englings folgen. Eingeladen sind alle SMJler ab Kreisalter. Genauere Infos gibt es demnächst.

Man sieht, es ist schon einiges los im neuen Jahr!

Weitere Termine findet ihr in unserem Jahresflyer und immer aktuell unter [smj-fulda.org/termine](https://smj-fulda.org/termine/).

<img src="/images/2019/smj-jahresflyer-2019-web2.jpg" />

<img src="/images/2019/smj-jahresflyer-2019-web.jpg" />
