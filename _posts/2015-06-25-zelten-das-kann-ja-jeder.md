---
tags: [smjfulda, kreise, kreissteffian]
migrated:
  node_id: 1004
  migrated_at: 2017-02-20 22:28
  alias: artikel/1004-zelten-das-kann-ja-jeder
  user_id: 1
tagline: Kreistreffen von „Stefian“ vom 19. Bis 21. Juni 2015
image:
  cover: https://farm1.staticflickr.com/478/19121402446_2fdea9b23f_b.jpg
title: Zelten? Das kann ja jeder!
created_at: 2015-06-25 16:06
excerpt: Endlich wieder ein Treffen vom Kreis,<br> leider war das Wetter nicht so heiß!<br> Mit 10 Leuten ging es rund,<br> aber es blieben alle gesund. Zu Beginn in Dietershausen<br> bekamen wir dann aber Muffensausen!<br> Der Regen machte uns Sorgen,<br> daher fühlten wir uns lieber im Haus geborgen. So luden wir alles ein<br> nur die Zelte blieben daheim.<br> Los ging es nach Döllbach,<br> da war der Yannick ja vom Fach! Im Jugendraum richteten wir uns ein<br> und bestellten auch gleich unser Essen fein.<br> Vor der Lieferung gings zum Fußball runter,<br> so wurden wir alle richtig munter.
---
<p>Endlich wieder ein Treffen vom Kreis,<br />
	leider war das Wetter nicht so heiß!<br />
	Mit 10 Leuten ging es rund,<br />
	aber es blieben alle gesund.</p>
<p>Zu Beginn in Dietershausen<br />
	bekamen wir dann aber Muffensausen!<br />
	Der Regen machte uns Sorgen,<br />
	daher fühlten wir uns lieber im Haus geborgen.</p>
<p>So luden wir alles ein<br />
	nur die Zelte blieben daheim.<br />
	Los ging es nach Döllbach,<br />
	da war der Yannick ja vom Fach!</p>
<p>Im Jugendraum richteten wir uns ein<br />
	und bestellten auch gleich unser Essen fein.<br />
	Vor der Lieferung gings zum Fußball runter,<br />
	so wurden wir alle richtig munter.</p>
<p>Der Hunger stieg nun mehr und mehr,<br />
	das Essen kam, dass freute uns sehr.<br />
	Pizza , Schnitzel oder Döner,<br />
	es wurde immer schöner!</p>
<p>Nach dem Essen ging es weiter,<br />
	in Fulda mit Lasertag, das machte alle heiter.<br />
	Laufen, schießen und verstecken,<br />
	dabei als Team sich neu entdecken.</p>
<p>Am Abend dann noch Lagerfeuer,<br />
	Zeltlagerfeeling ist uns geheuer.<br />
	Gespräche und Abendgebet bei der Wärme,<br />
	davon können wir noch lange schwärme!</p>
<p>Nach der Nacht im Haus,<br />
	ging die Müdigkeit langsam raus.<br />
	So waren wir dann bald alle fit<br />
	und es ging los ins Schwimmbad, das war der Hit!</p>
<p>Ob rutschen oder Springen, ganz egal,<br />
	der Bademeister ermahnte uns mindestens einmal.<br />
	Alle hatten ihren Spaß im Becken,<br />
	nur der Steffen musste sich ab und zu verstecken!</p>
<p>Auf der Heimfahrt gab es dann noch ein Eis für Alle,<br />
	das hat den Jungs sehr gut gefalle.<br />
	In Döllbach wieder angelangt,<br />
	den Fahrern sei gedankt.</p>
<p>Nun ging es zur Grotte hin,<br />
	zum Gottesdienst, das war ein Gewinn.<br />
	Jugendpfarrer und Gemeinde kamen auch,<br />
	so bekam alles einen feierlichen Hauch.</p>
<p>Würstchen und Getränke gab es dann,<br />
	passend für uns als Mann!<br />
	Am Abend dann noch gemütlich Zeit erleben<br />
	und uns einander vieles geben.</p>
<p>Dann war auch schon wieder der Sonntag angebrochen,<br />
	es ging aber noch weiter wie versprochen.<br />
	Über 100 Jahre Schönstatt in der Welt,<br />
	aber wie genau, dass es uns heute so gefällt?</p>
<p>Viele Leute haben Schönstatt geprägt,<br />
	wurden aber auch leider abgesägt.<br />
	Josef Engling oder Franz Reinisch, diese Beiden,<br />
	taten viel Gutes in der Welt, mussten aber auch leiden.</p>
<p>So war also unser schönes Treffen<br />
	Mit 8 Jungs und den Kreisleitern Christian und Steffen.<br />
	Wir freuen uns schon auf die kommenden Aktionen,<br />
	die werden sich sicher wieder lohnen!</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157655060426755">
  <a href="https://www.flickr.com/photos/45962678@N06/19141717852/in/album-72157655060426755"><img src="https://farm4.staticflickr.com/3758/19141717852_e8d6960cab_q.jpg" alt="PICT0231" data-src-large="https://farm4.staticflickr.com/3758/19141717852_e8d6960cab_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/19147472545/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/403/19147472545_792a5aa2ae_q.jpg" alt="PICT0224" data-src-large="https://farm1.staticflickr.com/403/19147472545_792a5aa2ae_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/18524943404/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/388/18524943404_496d430e31_q.jpg" alt="PICT0223" data-src-large="https://farm1.staticflickr.com/388/18524943404_496d430e31_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/18959897698/in/album-72157655060426755"><img src="https://farm4.staticflickr.com/3852/18959897698_6128fea238_q.jpg" alt="PICT0219" data-src-large="https://farm4.staticflickr.com/3852/18959897698_6128fea238_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/18961368029/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/545/18961368029_15d12832d9_q.jpg" alt="PICT0213" data-src-large="https://farm1.staticflickr.com/545/18961368029_15d12832d9_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/19121331056/in/album-72157655060426755"><img src="https://farm4.staticflickr.com/3872/19121331056_1e8d1794db_q.jpg" alt="PICT0206" data-src-large="https://farm4.staticflickr.com/3872/19121331056_1e8d1794db_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/18959925108/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/546/18959925108_9290da8178_q.jpg" alt="PICT0200" data-src-large="https://farm1.staticflickr.com/546/18959925108_9290da8178_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/19141774732/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/287/19141774732_514dee0a61_q.jpg" alt="PICT0199" data-src-large="https://farm1.staticflickr.com/287/19141774732_514dee0a61_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/18526904303/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/412/18526904303_e68496b812_q.jpg" alt="PICT0197" data-src-large="https://farm1.staticflickr.com/412/18526904303_e68496b812_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/18959953498/in/album-72157655060426755"><img src="https://farm4.staticflickr.com/3955/18959953498_d8e2a4e9f4_q.jpg" alt="PICT0196" data-src-large="https://farm4.staticflickr.com/3955/18959953498_d8e2a4e9f4_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/18525017374/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/404/18525017374_42559caa78_q.jpg" alt="PICT0193" data-src-large="https://farm1.staticflickr.com/404/18525017374_42559caa78_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/19147561295/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/457/19147561295_2bf94b4b29_q.jpg" alt="PICT0192" data-src-large="https://farm1.staticflickr.com/457/19147561295_2bf94b4b29_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/19121402446/in/album-72157655060426755"><img src="https://farm1.staticflickr.com/478/19121402446_2fdea9b23f_q.jpg" alt="PICT0190" data-src-large="https://farm1.staticflickr.com/478/19121402446_2fdea9b23f_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/19151042081/in/album-72157655060426755"><img src="https://farm4.staticflickr.com/3843/19151042081_3eb05a5546_q.jpg" alt="PICT0180" data-src-large="https://farm4.staticflickr.com/3843/19151042081_3eb05a5546_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157655060426755" class="flickr-link" title="Bildergalerie Kreistreffen Stefian Juni 2015 auf Flickr">Bildergalerie</a>
</figure>
