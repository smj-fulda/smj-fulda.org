---
tags: [smjfulda, ipray]
migrated:
  node_id: 947
  migrated_at: 2017-02-20 22:27
  alias: artikel/947-ipray-somborn
  user_id: 1
tagline: Lobpreisabend iPray war am 20. April 2013 in Freigericht-Somborn bei Pfarrer Schäfer.
image:
  cover: https://farm9.staticflickr.com/8262/8678651668_e912f37c31_b.jpg
title: iPray in Somborn
created_at: 2013-04-24 18:06
excerpt: Die Besucher des achten iPray-Lobpreises erwartete am Samstag ein abwechslungsreiches Programm. Die Lobpreisgruppe war diesmal in Somborn(Freigericht) zu Gast um den Abend zum Thema <em>Vertrauen</em> zu gestalten. Kerzen entzünden, Livemusik, Anbetung, Bildbetrachtung, eine Vertrauensübung und ein persönliches Zeugnis sind nur einige Punkte des eineinhalbstündigen Programms. Wie schon in einigen Gemeinden zuvor brachte der Lobpreis auch in Somborn frischen Wind und spirituelle Anstöße in die Kirche. Wann und wo der nächste Lobpreis stattfindet steht noch nicht fest.
---
<p>Die Besucher des achten iPray-Lobpreises erwartete am Samstag ein abwechslungsreiches Programm. Die Lobpreisgruppe war diesmal in Somborn(Freigericht) zu Gast um den Abend zum Thema <em>Vertrauen</em> zu gestalten. Kerzen entzünden, Livemusik, Anbetung, Bildbetrachtung, eine Vertrauensübung und ein persönliches Zeugnis sind nur einige Punkte des eineinhalbstündigen Programms. Wie schon in einigen Gemeinden zuvor brachte der Lobpreis auch in Somborn frischen Wind und spirituelle Anstöße in die Kirche.</p>
<p>Wann und wo der nächste Lobpreis stattfindet steht noch nicht fest.</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157633315665763">
  <a href="https://www.flickr.com/photos/45962678@N06/8678651668/in/album-72157633315665763"><img src="https://farm9.staticflickr.com/8262/8678651668_e912f37c31_q.jpg" alt="" data-src-large="https://farm9.staticflickr.com/8262/8678651668_e912f37c31_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8678650726/in/album-72157633315665763"><img src="https://farm9.staticflickr.com/8123/8678650726_35ea5cf315_q.jpg" alt="" data-src-large="https://farm9.staticflickr.com/8123/8678650726_35ea5cf315_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8678649948/in/album-72157633315665763"><img src="https://farm9.staticflickr.com/8522/8678649948_a72558ac33_q.jpg" alt="" data-src-large="https://farm9.staticflickr.com/8522/8678649948_a72558ac33_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157633315665763" class="flickr-link" title="Bildergalerie iPray Somborn auf Flickr">Bildergalerie</a>
</figure>
