---
tags: [smjfulda, mjffulda, kjf, bonifatiuskloster, omijugend, glaubenskurs]
migrated:
  node_id: 906
  migrated_at: 2017-02-20 22:27
  alias: artikel/906-glaubenskurs:-viel-los-hünfeld
  user_id: 1
tagline: 'Der erste Glaubenskurs 2012 war mit 35 Teilnehmern gut besucht. Themen: Maria und Mann &amp; Frau'
image:
  cover: https://farm8.staticflickr.com/7154/6782098311_7d972820b4_b.jpg
title: 'Glaubenskurs: Viel los in Hünfeld'
created_at: 2012-01-29 14:57
excerpt: 'Der erste Glaubenskurs in diesem Jahr hatte am vergangenen Wochenende großen Zuspruch: 35 junge Christen sind im Bonifatiuskloster Hünfeld zusammengekommen um sich in Gemeinschaft im Glauben weiterzubilden. Während es am ersten Abend um die Rolle der Gottesmutter Maria und ihre Funktion für die Kirche ging, stand der Samstag ganz unter dem Thema, warum es den Mensch als Mann und Frau gibt und was das bedeutet für die Kirche, für den Alltag und für Beziehungen. Als Referenten waren Pfarrer Ulrich Schäfer und Dipl. theol. Simone Twents gekommen, zur geistlichen Begleitung Pater Felix Rehbock OMI und Kaplan Florian Böth. Neben den inhaltlichen Programmblöcken nahmen aber auch das gemeinsame Gebet und Geselligkeit mit Spiel &amp; Spaß eine wichtige Position ein.'
---
<p>Der erste Glaubenskurs in diesem Jahr hatte am vergangenen Wochenende großen Zuspruch: 35 junge Christen sind im Bonifatiuskloster Hünfeld zusammengekommen um sich in Gemeinschaft im Glauben weiterzubilden. Während es am ersten Abend um die Rolle der Gottesmutter Maria und ihre Funktion für die Kirche ging, stand der Samstag ganz unter dem Thema, warum es den Mensch als Mann und Frau gibt und was das bedeutet für die Kirche, für den Alltag und für Beziehungen. Als Referenten waren Pfarrer Ulrich Schäfer und Dipl. theol. Simone Twents gekommen, zur geistlichen Begleitung Pater Felix Rehbock OMI und Kaplan Florian Böth. Neben den inhaltlichen Programmblöcken nahmen aber auch das gemeinsame Gebet und Geselligkeit mit Spiel &amp; Spaß eine wichtige Position ein.</p>
<p>Dies war bereits der dritte Glaubenskurs für Jugendlich ab 16 Jahren, der gemeinsam vom Jugendbüro der Hünfelder Oblaten, der Katholische Jugend im Bistum Fulda (KJF) sowie der Schönstattbewegung Mädchen/Junge Frauen (SchönstattMJF) und der Schönstatt-Mannesjugend (SMJ) veranstaltet wurde. Das nächste Glaubenskurswochenende wird es im Herbst 2012 geben.</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157629143642811">
  <a href="https://www.flickr.com/photos/45962678@N06/6806929841/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7010/6806929841_168b6fe207_q.jpg" alt="Pfarrer Ulrich Schäfer" data-src-large="https://farm8.staticflickr.com/7010/6806929841_168b6fe207_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6806932357/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7027/6806932357_0de3a527c4_q.jpg" alt="Teilnehmer der Größ nach aufgereiht" data-src-large="https://farm8.staticflickr.com/7027/6806932357_0de3a527c4_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6806934737/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7022/6806934737_28d7a5c887_q.jpg" alt="Aufteilspiel" data-src-large="https://farm8.staticflickr.com/7022/6806934737_28d7a5c887_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6806938175/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7162/6806938175_866e82a95a_q.jpg" alt="Beschäftig wurde sich auch mit dem Youcat" data-src-large="https://farm8.staticflickr.com/7162/6806938175_866e82a95a_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6806941203/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7005/6806941203_1d0a36a400_q.jpg" alt="Laura mit Youcat" data-src-large="https://farm8.staticflickr.com/7005/6806941203_1d0a36a400_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6806944789/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7160/6806944789_ecc0515a62_q.jpg" alt="Simone Twents" data-src-large="https://farm8.staticflickr.com/7160/6806944789_ecc0515a62_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6806948215/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7141/6806948215_4616fefbec_q.jpg" alt="Katharina & Martha" data-src-large="https://farm8.staticflickr.com/7141/6806948215_4616fefbec_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6806951999/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7164/6806951999_07b628927f_q.jpg" alt="Gruppenbild" data-src-large="https://farm8.staticflickr.com/7164/6806951999_07b628927f_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6862904397/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7048/6862904397_a69a60ae44_q.jpg" alt="" data-src-large="https://farm8.staticflickr.com/7048/6862904397_a69a60ae44_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6862911993/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7198/6862911993_0faa556507_q.jpg" alt="" data-src-large="https://farm8.staticflickr.com/7198/6862911993_0faa556507_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6862915823/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7199/6862915823_2728d19b08_q.jpg" alt="" data-src-large="https://farm8.staticflickr.com/7199/6862915823_2728d19b08_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6862923201/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7181/6862923201_2c856e0266_q.jpg" alt="" data-src-large="https://farm8.staticflickr.com/7181/6862923201_2c856e0266_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6862929191/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7180/6862929191_1c4758dcc0_q.jpg" alt="" data-src-large="https://farm8.staticflickr.com/7180/6862929191_1c4758dcc0_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/6862939275/in/album-72157629143642811"><img src="https://farm8.staticflickr.com/7188/6862939275_8e1cbaa3fd_q.jpg" alt="" data-src-large="https://farm8.staticflickr.com/7188/6862939275_8e1cbaa3fd_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157629143642811" class="flickr-link" title="Bildergalerie Glaubenskurs Januar 2012 auf Flickr">Bildergalerie</a>
</figure>
