---
categories:
- Kreisarbeit
- SMJ-Fulda
- Mitgliederbündnis
- Mitarbeiterbündnis
- Liebebsbündnis
- Kreistagung
- Diözesanführer
- Bündniskreistagung
migrated:
  node_id: 759
  migrated_at: 2017-02-21 14:39
  alias: artikel/759-bündniskreistagung-mit-diözesanführerwahl
  user_id: 1
tagline: 12 neue Liebesbündnisse und Diözesanführung im Amt bestätigt
image:
  cover: https://farm5.staticflickr.com/4001/4285143108_a589f77613_b.jpg
title: Bündniskreistagung mit Diözesanführerwahl
created_at: 2009-11-16 22:56
excerpt: Die dritte Kreistagung der SMJ-Fulda 2009 bildete mit seiner Bündnisfeier, an dem 12 Jugendliche ihr Liebesbündnis erweiterten, den Höhepunkt der diesjährigen Kreisarbeit, worauf sich die älteren Kreise das ganze Jahr vorbereitet hatten. An der Tagung nahmen die Kreise von Thomas, Benjamin &amp; Alexander, Matthias und Ulrich mit insgesamt 25 Jugendlichen teil. Da neben der normalen Kreisarbeit viele der älteren Teilnehmer im Gottesdienst am Samstag Abend ein erweitertes Liebesbündnis schließen wollten, nahmen sie sich nachmittags freie Zeit für sich, um sich innerlich darauf einzustellen, eventuell noch das in oder andere Gespräch zu führen.
---
<div class="section">
<p>Die dritte Kreistagung der SMJ-Fulda 2009 bildete mit seiner Bündnisfeier, an dem 12 Jugendliche ihr Liebesbündnis erweiterten, den Höhepunkt der diesjährigen Kreisarbeit, worauf sich die älteren Kreise das ganze Jahr vorbereitet hatten.</p>
<p>An der Tagung nahmen die Kreise von Thomas, Benjamin &amp; Alexander, Matthias und Ulrich mit insgesamt 25 Jugendlichen teil. Da neben der normalen Kreisarbeit viele der älteren Teilnehmer im Gottesdienst am Samstag Abend ein erweitertes Liebesbündnis schließen wollten, nahmen sie sich nachmittags freie Zeit für sich, um sich innerlich darauf einzustellen, eventuell noch das in oder andere Gespräch zu führen.</p>
<p>Den Bündnisgottesdienst zelebrierte Pfarrer Ulrich Schäfer, als Standesleiter der Mannesjugend im Bistum Fulda, zusammen mit Pater Thomas Jocheim, dem Standesleiter der Schönstatt Mannesjugend Deutschland. Außer den Mitgliedern der Kreise waren auch noch weitere Gäste anwesend, unter denen besonders Schwester Hilltraude hervorzuheben ist, die als Zeitzeuge von der Gründung der SMJ-Fulda durch Pater Joseph Kentenich im Jahr 1967 berichten konnte.</p>
</div>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157623235039782">
  <a href="https://www.flickr.com/photos/45962678@N06/4284401165/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4063/4284401165_a80f93fd96_q.jpg" alt="Pfarrer Schäfer" data-src-large="https://farm5.staticflickr.com/4063/4284401165_a80f93fd96_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285144472/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4020/4285144472_a7763d0651_q.jpg" alt="Diözesanführer Tobias Büdel verliest Gründungsurkunde" data-src-large="https://farm5.staticflickr.com/4020/4285144472_a7763d0651_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285143966/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4060/4285143966_aacbb60262_q.jpg" alt="Kandidaten für Mitgliederweihe" data-src-large="https://farm5.staticflickr.com/4060/4285143966_aacbb60262_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4284399621/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4004/4284399621_982d18f5e9_q.jpg" alt="Christian und Christoph Schopp" data-src-large="https://farm5.staticflickr.com/4004/4284399621_982d18f5e9_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285143108/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4001/4285143108_a589f77613_q.jpg" alt="Ullrich Schott" data-src-large="https://farm5.staticflickr.com/4001/4285143108_a589f77613_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285142600/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4006/4285142600_c6ea3a94ee_q.jpg" alt="Philipp Müller" data-src-large="https://farm5.staticflickr.com/4006/4285142600_c6ea3a94ee_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285142104/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4030/4285142104_434d89bacd_q.jpg" alt="Matthias Schott" data-src-large="https://farm5.staticflickr.com/4030/4285142104_434d89bacd_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4284397587/in/album-72157623235039782"><img src="https://farm3.staticflickr.com/2787/4284397587_f284df2352_q.jpg" alt="Matthias Schott" data-src-large="https://farm3.staticflickr.com/2787/4284397587_f284df2352_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285141142/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4001/4285141142_ec11a52d26_q.jpg" alt="Gruppenphoto Mitgliederbündnis" data-src-large="https://farm5.staticflickr.com/4001/4285141142_ec11a52d26_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285140692/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4068/4285140692_9276b12e10_q.jpg" alt="Gruppenphoto Mitarbeiterbündnis" data-src-large="https://farm5.staticflickr.com/4068/4285140692_9276b12e10_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285140256/in/album-72157623235039782"><img src="https://farm3.staticflickr.com/2793/4285140256_fac14fcfcf_q.jpg" alt="Gruppenphoto" data-src-large="https://farm3.staticflickr.com/2793/4285140256_fac14fcfcf_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4285139752/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4020/4285139752_89a089feda_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4020/4285139752_89a089feda_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/4284395075/in/album-72157623235039782"><img src="https://farm5.staticflickr.com/4063/4284395075_22ced55a1c_q.jpg" alt="Unterschriften der Bündnispartner" data-src-large="https://farm5.staticflickr.com/4063/4284395075_22ced55a1c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157623235039782" class="flickr-link" title="Bildergalerie Bündniskreistagung 2009 auf Flickr">Bildergalerie</a>
</figure>
<figure data-href="https://www.flickr.com/photos/45962678@N06/4285143108">
  <img src="https://farm5.staticflickr.com/4001/4285143108_a589f77613_c.jpg" class="flickr-img flickr-img--medium" alt="Ullrich Schott" />
  <figcaption>Ullrich Schott (<time datetime="2009-11-14 19:01:21">2009</time>)</figcaption>
</figure>
<div class="section"> <p>Stellvertretend für die Gottesmutter und Pater Kentenich nahm Pfarrer Schäfer die Mitarbeiterbündnisse an, die fünf Jugendliche aus dem Kreis Benjamin &amp; Alexander schlossen. Thomas Limbach, Steffen Büdel, Kilian Machill sowie Christian &amp; Christoph Schopp schlossen ein Mitarbeiterbündnis, durch das sie als Mitarbeiter in der SMJ aktiv sein wollen, und sich für die Arbeit der Mannesjugend engagieren</p>
<p>Einen Schritt weiter geht das Mitgliederbündnis, das eine noch stärkere Verbundenheit mit der SMJ ausdrückt, volles Engagement und die Übernahme von Verantwortung. Dieses Bündniss schlossen sieben Jugendliche, Philipp &amp; Johannes Müller, Ulrich Schott, Tobias Büdel, Klaus Schmitt, Matthias Schott und Alexander Dücker.</p>
</div>
<figure data-href="https://www.flickr.com/photos/45962678@N06/4285139752">
  <img src="https://farm5.staticflickr.com/4020/4285139752_89a089feda_c.jpg" class="flickr-img flickr-img--medium" alt="" />
  <figcaption> (<time datetime="2009-11-14 21:19:34">2009</time>)</figcaption>
</figure>
<p>Zusammen brachte diese Bündnisfeier nach längerer Durststrecke 12 neue Bündnisse für die Schönstatt Mannesjugend, was einen zuversichtlichen Blick in die Zukunft und Weiterentwicklung der SMJ ermöglicht. Auf dieser Basis lässt sich eine gut strukturierte und kraftvolle Jugendarbeit aufbauen, die allein vom starken Engagement der ehrenamtlichen Gruppenleiter abhängt. Der Büdnisgottesdienst zeigte klar, dass eine gute Gemeinschaft ungemein wichtig ist, gegenseitig trägt und ermuntert, sich selbst für die Kirche und besonders die Schönstatt-Mannesjugend einzusetzen.</p>
<p>Im Anschluss fand die Wahl zum Diözesanführer und dessen Stellvertreter statt, an der nun auch die SMJler mit neu geschlossenen Bündnis teilnehmen durften, denn aktives Wahlrecht in der SMJ erfordert mindestens eine Mitarbeiterweihe. Von insgesamt 13 Wahlberechtigten wurde die bisherige Diözesanführung aus Tobias Büdel und seinem Stellvertreter Klaus Schmitt im ersten Wahlgang einstimmig bestätigt.</p>
<p>Nachdem die Führungsverantwortung für die nächsten zwei Jahre geklärt war, luden die 12 Bündnisschließer die übrigen Kreismitglieder und Gäste zu einer gemeinsamen Feier mit Festbüffet und gemütlichem Zusammensitzen ein. Den Abschluss der Bündnisveranstaltung bildete eine Fackelwanderung zum SMJ-Bildstock am Dassenrasen, der aus der Ursprungszeit der SMJ im Bistum Fulda stammt.</p>
