---
tags: [smjfulda, adventskalender]
migrated:
  node_id: 921
  migrated_at: 2017-02-20 22:27
  alias: artikel/921-der-smj-sms-adventskalender
  user_id: 9
tagline: Das Warten hat ein Ende! Hier ist der brandneue SMJ-SMS-Adventskalender!
author: Sebastian Hartmann
title: Der SMS-Adventskalender
created_at: 2014-11-05 10:06
excerpt: Liebe Bekannte, Freunde und Mitglieder der Schönstatt-Mannesjugend Fulda, die SMS-Adventskalender-Aktion 2016 ist in vollem Gange. Täglich werden unzählige SMS-Türchen verschickt. Eine Anmeldung ist leider nicht mehr möglich, wir freuen uns aber schon jetzt auf den SMS-Adventskalender im nächsten Jahr. Allen, die mitmachen, wünschen wir viel Freude mit den einzelnen Türchen. Einen schönen Advent wünscht die SMJ-Fulda!
---
<p>Liebe Bekannte, Freunde und Mitglieder der Schönstatt-Mannesjugend Fulda,</p>
<p>die SMS-Adventskalender-Aktion 2016 ist in vollem Gange. Täglich werden unzählige SMS-Türchen verschickt. Eine Anmeldung ist leider nicht mehr möglich, wir freuen uns aber schon jetzt auf den SMS-Adventskalender im nächsten Jahr. Allen, die mitmachen, wünschen wir viel Freude mit den einzelnen Türchen. Einen schönen Advent wünscht </p>
<p>die SMJ-Fulda!</p>

<p>Anmerkung zum Datenschutz:<br />
	Die Daten werden ausschließlich im Zeitraum vom 1. bis 24. Dezember 2016 im Rahmen der Aktion zum Versenden der SMS verwendet und anschließend gelöscht. Die Daten werden vertraulich behandelt und nicht zu Werbezwecken genutzt oder an Dritte weitergegeben. Ein Anspruch auf Teilnahme besteht nicht. Die Aktion steht unter dem Vorbehalt der technischen Durchführbarkeit.</p>
<p><strong><br />
	Um sich vom SMJ-SMS-Adventskalender wieder abzumelden, genügt eine E-Mail an: <br />
	adventskalender@smj-fulda.org&nbsp;</strong></p>
<h3>Wichtige Hinweise:&nbsp;</h3>
<ul>
	<li>Die Anmeldung ist nur bis zum <strong>30. November 2016</strong>&nbsp;möglich.</li>
	<li>Zur Teilnahme ist eine <strong>deutsche Mobilfunknummer</strong> notwendig. Ein Versand der Nachrichten ins Ausland ist leider ausgeschlossen.</li>
	<li>Eine Anmeldung von Freunden/Verwandten etc. ist leider nicht möglich. Eine Anmeldung kann <strong>nur persönlich</strong> für die eigene Rufnummer erfolgen.</li>
</ul>
<h3>Presseschau:</h3>
<ul>
	<li><a href="http://schoenstatt.de/de/news/1729/112/Adventskalender-per-SMS-die-SMJ-macht-s-moeglich.htm">Adventskalender per SMS &ndash; die SMJ macht&lsquo;s möglich</a> (23. November 2012, <em>schoenstatt.de</em>)</li>
	<li><a href="https://www.dropbox.com/s/t4pay3tc1vpl7gp/SMJ-SMS-Adventskalender.jpg">SMS als Adventskalender</a> (24. November 2012, <em>Die Tagespost</em>)</li>
	<li><a href="http://www.firstlife.de/der_etwas_andere_adventskalender/">Der etwas andere Adventskalender </a>(26. November 2013, f1rstlife.de)</li>
</ul>
