---
tags: [smjfulda, gewo]
migrated:
  node_id: 1001
  migrated_at: 2017-02-20 22:28
  alias: artikel/1001-gestrandet-dietershauen
  user_id: 1
tagline: Gemeinschaftswochenende an Fastnacht im Josef-Engling-Haus in Dietershausen.
title: Gestrandet in Dietershauen
created_at: 2015-02-19 23:45
excerpt: Mal wieder ist ein klasse GeWo vorbei. Mit „klein, aber fein“ könnte man das diesjährige Faschingswochenend-GeWo vom 13.02.15 bis 15.02.15 sehr treffend bezeichnen. Denn mit 11 Teilnehmern und 3 Betreuern war die Zahl der wilden Kinder und Jugendlichen im Josef-Engling-Haus wohl eher überschaubar, aber nichtsdestotrotz gerade deshalb für einige Kinder „womöglich das beste GeWo, dass ich (sie) bisher besucht habe(n)“
---
<p>Mal wieder ist ein klasse GeWo vorbei. Mit &bdquo;klein, aber fein&ldquo; könnte man das diesjährige Faschingswochenend-GeWo vom 13.02.15 bis 15.02.15 sehr treffend bezeichnen. Denn mit 11 Teilnehmern und 3 Betreuern war die Zahl der wilden Kinder und Jugendlichen im Josef-Engling-Haus wohl eher überschaubar, aber nichtsdestotrotz gerade deshalb für einige Kinder &bdquo;womöglich das beste GeWo, dass ich (sie) bisher besucht habe(n)&ldquo;</p>
<p>Dieses GeWo begann, wie alle anderen auch, mit einem tollen Abendessen und anschließend lustigen Spielen. Ein Großteil der Teilnehmer gehörte zu den &bdquo;alten Hasen&ldquo;, lediglich ein neues Gesicht hatte den Weg nach Dietershausen gefunden. Somit viel der Kennlernteil entsprechend kurz aus. Das war allerdings nur gut so, da so mehr Zeit für wilde Spiele und einen klasse Film zur thematischen Einleitung war: Die Abendteuer des Robinson Crusoe.</p>
<p>Mit Spannung sahen wir alle den für die meisten unbekannten Film. Wir sahen den armen Robinson, wie dieser allein auf einer Insel strandete, auf der er 28 Jahre verbringen würde, und keinen blassen Schimmer hatte, wie er nun überleben sollte.</p>
<p>Doch nach und nach lernte er sich zurecht zu finden. Nach einer sehr langen Zeit gewann Robinson dann jedoch mit Freitag neue Gesellschaft und einen neuen Freund.</p>
<p>Nach 20 Jahren Einsamkeit hatte er endlich jemanden, mit dem er sich unterhalten konnte, jedenfalls nachdem er ihm das Sprechen beigebracht hatte. Zusammen war das überleben für sie viel einfacher. Freitag beherrschte das Fischen viel besser als Robinson, wurde zu einem hervorragenden Schützen und das Leben wurde viel angenehmer.</p>
<p>Gegen Ende des Filmes kam dann schließlich einmal ein Schiff in die Nähe der Insel Robinsons und setzte in einer Bucht, nach einer Meuterei an Bord, Anker. Die Meuterer wollten den nun abgesetzten Kapitän und die diesem treuen Leute auf der Insel aussetzten. Jedoch retten Robinson und Freitag diese und fesselten die Meuterer, um sie anschließend auf der Insel zurückzulassen. Robinson verließ sie mit den Worten:&ldquo; Ihr habt einen sehr großen Vorteil im Vergleich zu mir: Ihr habt Freunde, ihr seid eine Gemeinschaft. Wenn ihr zusammenarbeitet habt ihr eure Fesseln im Handumdrehen gelöst.&ldquo;</p>
<p>Mit diesen Worten gingen auch wir ins Bett, um Kraft zu tanken für den kommenden Tag.</p>
<p>Nach dem Wecken, dem Frühsport und dem Frühstück am Samstagmorgen erkundeten wir den Wert von Gemeinschaft und Zusammenarbeit. In Gruppenarbeiten lasen wir anschließend einzelne Bibelstellen, die das gelernte untermauerten und sprachen über diese.</p>
<p>Anschließend spielten wir noch ein wenig und gingen dann zum guten Mittagessen.</p>
<p>Nach der Mittagspause trafen wir uns draußen zum Nachmittagsprogramm: Geländestratego. Dies spielten wir ausgiebig bis zur Kakao und Kuchen Zeit. Anschließend ging es ans Eingemachte: Wir lernten übers Überleben.</p>
<p>Wir gingen davon aus, auf einer einsamen Insel oder mitten in der Wüste, in den Bergen oder in einem großen Wald allein gestrandet zu sein und überlegten, wie wir nun überleben konnten.</p>
<p>Wir lernten und probierten viele Methoden, uns zu orientieren, Wasser zu finden und dieses zu reinigen, Nahrung zu sammeln, ein Unterschlupf zu bauen, ein Feuer zu entzünden, uns nicht zu verletzten und am Ende gerettet zu werden. Den ausführlichen Survival Guide gibt&rsquo;s im Anschluss als Download.</p>
<p>Nach dem Abendessen hatten wir dann jede menge Spaß beim Showabend mit dem Angeberspiel:</p>
<p>3 Gruppen von Kindern spielten gegeneinander, bekamen zunächst von den Betreuern eine Aufgabe gestellt und mussten dann reizen, wer die Aufgabe in weniger Zeit bzw. besser erfüllen konnte. Konnte derjenige, der das Reizen gewann seiner Angeberei schlussendlich gerecht werden und die Aufgabe meistern, bekam sein Team einen Punkt, wenn nicht, bekamen die beiden Anderen einen. Allen Teams standen jeweils Gruppenleiterjoker zur Verfügung, bei deren Einlösung sie jeweils einen Gruppenleiter zur Erfüllung ihrer Aufgabe einsetzen konnten.</p>
<p>Das Spiel endete denkbar knapp. So gewann Gruppe 3 mit 16 Punkten, dicht gefolgt von Gruppe 1 mit 15 Punkten und mit nur 3 Punkten Abstand landete Gruppe 2 auf Platz 3 (12 Punkte).</p>
<p>Wir beschlossen den Tag und gingen ein letztes Mal schlafen mit dem Wissen, dass das GeWo dann schon bald wieder vorbei sein würde.</p>
<p>Am Sonntagmorgen dann räumten wir unsere Zimmer, fegten grob durch, und machten uns bereit für den Gottesdienst oben im Provinzhaus.</p>
<p>Nach diesem aßen wir vorerst noch ein letztes Mal zusammen und verabschiedeten uns dann an einem sehr schönen und sonnigen Sonntagmachmittag nach Hause.</p>
