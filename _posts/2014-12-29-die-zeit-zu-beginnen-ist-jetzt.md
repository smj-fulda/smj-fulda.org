---
tags: [smjfulda]
migrated:
  node_id: 992
  migrated_at: 2017-02-20 22:28
  alias: artikel/992-die-zeit-zu-beginnen-ist-jetzt
  user_id: 1
tagline: Mit diesem Impuls zum Thema Zeit wünscht die SMJ-Fulda Allen einen guten Start in das Jahr 2015! Wir bedanken uns bei allen, die uns im Jahr 2014 bei unseren Aktionen unterstützt haben.
image:
  cover: https://farm4.staticflickr.com/3857/14872166676_68a0410ac5_b.jpg
title: Die Zeit zu beginnen ist jetzt
created_at: 2014-12-29 15:03
excerpt: "„Ich habe keine Zeit“, „Ich habe so viel zu tun“ oder „Ich muss das jetzt machen“, das sind alles Aussagen, die mir in den letzten Wochen bei mir selbst und auch bei anderen Menschen sehr aufgefallen sind! Das Thema Zeit ist besonders in der heutigen „Zeit“ ein Thema, welches viele Menschen beschäftigt. Der Tag hat 24 Stunden! 8-10 Stunden schlafen…also bleiben ca. 14 Stunden für den Tag! 14, das hört sich nach verdammt viel an, aber wäre es nicht schön, wenn wir drei oder vier Stunden mehr hätten?"
---
<p>&bdquo;Ich habe keine Zeit&ldquo;, &bdquo;Ich habe so viel zu tun&ldquo; oder &bdquo;Ich muss das jetzt machen&ldquo;, das sind alles Aussagen, die mir in den letzten Wochen bei mir selbst und auch bei anderen Menschen sehr aufgefallen sind!</p>
<p>Das Thema Zeit ist besonders in der heutigen &bdquo;Zeit&ldquo; ein Thema, welches viele Menschen beschäftigt. Der Tag hat 24 Stunden! 8-10 Stunden schlafen&hellip;also bleiben ca. 14 Stunden für den Tag! 14, das hört sich nach verdammt viel an, aber wäre es nicht schön, wenn wir drei oder vier Stunden mehr hätten?</p>
<p>Denn jammern wir nicht ständig, dass die Zeit zu schnell vergeht oder dass wir einfach zu wenig Zeit an einem Tag haben? Wie definiert man Zeit oder besser gefragt wie definiere ich den Begriff Zeit in meinem Leben? Woher kommt es, dass das Thema Zeit in der Gesellschaft heute ein so wichtiges Thema ist? Liegt es einfach daran, dass jeder von uns ja Zeit hat, also 24 Stunden? Oder hat es nicht nur damit zu tun, dass jeder Zeit hat, sondern damit wie wir Menschen heute auf der Erde leben? Warum soll es mit unserem Leben zu tun haben?</p>
<p>Ich stehe auf, mache mich für den Tag fertig, besuche die Schule, gehe zur Arbeit oder kümmere mich um den Haushalt&hellip; Mein Tag ist strukturiert und mir geht es gut! Aber warum geht es mir gut? Ich habe ein Haus, Familie, Geld, Freunde&hellip;</p>
<p>Also, was soll denn da fehlen? Ich bin doch glücklich, auch mit den alltäglichen Problemen, und so wie mein Leben läuft kann es auch weiter gehen!</p>
<p>Ich bin selber ein Typ, der Veränderungen eher kritisch beäugt! Doch ich frage mich schon länger was denn den Mittelpunkt unserer Gesellschaft ausmacht. Ich sage, dass unsere heutige Gesellschaft durch Druckausübung, Macht, Wohlstand und Bildung ausgezeichnet wird! Fühlen wir uns nicht manchmal in einem Rad gefangen aus dem wir nur schwer wieder heraustreten können? Und hier liegt laut meiner Ansicht der Fehler! Muss nicht der Mensch im Mittelpunkt stehen?</p>
<p>Und nicht wie heute, den Mensch als Nummer sehen, sondern der Mensch als ein Wesen ansehen, welches nach einem gelingenden Leben strebt. Manche sagen vielleicht, dass das heute nicht mehr ganz so einfach ist, da wir uns als eine große Gemeinschaft irgendwie organisieren müssen&hellip;</p>
<p>Vielleicht können wir die kommende Zeit dazu nutzen uns mal Gedanken zu machen über unser Leben. Was heißt für mich der Begriff &bdquo;Zeit&ldquo;? Welche Menschen sind mir wichtig? Wer bin ich in dieser Welt?</p>
<p><strong>Mit diesem Impuls zum Thema Zeit wünscht die SMJ-Fulda Allen einen guten Start in das Jahr 2015! Wir bedanken uns bei allen, die uns im Jahr 2014 bei unseren Aktionen unterstützt haben!</strong></p>
