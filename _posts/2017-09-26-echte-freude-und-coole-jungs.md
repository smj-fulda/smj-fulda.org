---
title: "Echte Freunde und coole Jungs – Kreistreffen in Dietershausen"
excerpt: >
  Warum sind Freunde wichtig für uns? Was macht eine Freundschaft aus und gibt es einen Unterschied zwischen Mädchen und Jungen? Das alles und viel mehr beschäftige uns vom 22. bis 24. September 2017 im Schönstattzentrum in  Dietershausen.
author: Rainer M. Gotter
tags: [smjfulda, kreise]
image:
    cover: "https://farm5.staticflickr.com/4477/37486979842_4d52cbcde7_k_d.jpg"
    align: top
---

<figure>
  <img src="https://farm5.staticflickr.com/4469/36807884144_7b24927c1e_z_d.jpg">
</figure>

Warum sind Freunde wichtig für uns? Was macht eine Freundschaft aus und gibt es einen Unterschied zwischen Mädchen und Jungen? Das alles und viel mehr beschäftige uns vom 22. bis 24. September 2017 im Schönstattzentrum in  Dietershausen.

Gleich zu Beginn unseres Kreistreffens stellten wir fest, je älter wir werden, desto wichtiger werden Freunde. Als Kind sind Mama und Papa noch die besten Freunde, das wandelt sich dann aber als Jugendlicher. Vor allem dann, wenn Gleichaltrige ein wichtiger Bestandteil des Lebens werden. Freundschaften mit Jungs, klar auch mit Mädchen, bieten viel Raum für Spaß und Freude. Im gegenseitigen Kennenlernen und sich auszuprobieren, entwickelt man sich weiter. Bestimmte Werte, wie Ehrlichkeit und Zuverlässigkeit und Respekt werden von Freunden neu eingeordnet und bringen so Abwechslung in das eigene Leben.

Auf unserer Wanderung und den guten Gesprächen konnten wir miteinander feststellen, wie wichtig es ist Freunde zu haben, aber noch mehr einen besten Freund zu haben der einen versteht.

Für viele Jungs ist das Treffen mit älteren Jugendlichen wie eine zweite Familie. Denn hier entdecken wir miteinander viele Gemeinsamkeiten oder hören, dass wir oft die gleichen Probleme haben. Ganz bewusst grenzen wir in unserem Kreis niemanden aus. Denn hier soll sich jeder wohlfühlen und als Mitglied aufgehoben wissen. Dieses Zusammenhörigkeitsgefühl ist uns sehr wichtig, dass konnten wir beim Spielen, aber ebenso im lebendigen Austausch unseres Glaubens hautnah erleben.

<figure>
  <img src="https://farm5.staticflickr.com/4509/37486986892_9a257f4b18_z_d.jpg">
</figure>

Viele junge Menschen, die wir kennen, geben damit an, viele Freunde im Internet zu haben. Doch haben die Kontakte nichts mit wirklichen Freunden zu tun. Denn manchmal kennt man den- oder diejenige gar nicht richtig.

Natürlich kann jeder auch Internet-Freundschaften haben und sich gegenseitig schreiben oder miteinander chatten oder wie wir es auch machen, mit denen regelmäßig zocken. Doch sind es in den meisten Fällen nur virtuelle Freunde, die für einen nicht da sind, wenn man Hilfe braucht. Daher freuen wir uns, dass wir in unserem Kreis echte Freunde sind, die sich Zeit füreinander nehmen und sich auf einander verlassen können. Schon jetzt freuen wir uns darauf, dass wir uns im Dezember alle wieder sehen werden.
