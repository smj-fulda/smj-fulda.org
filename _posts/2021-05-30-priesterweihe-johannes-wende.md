---
title: "Priesterweihe von Johannes Wende"
tagline:
author: Johannes Müller
tags: [smj-fulda]
image:
  cover: https://live.staticflickr.com/65535/51219343545_d8ec559478_k_d.jpg
---

Am Pfingstsamstag weihte Bischof Michael Gerber im Fuldaer Dom zwei junge Männer zu Priestern. Einer davon ist Johannes Wende aus Bronnzell. Einer von uns.

<figure>
  <img src="https://live.staticflickr.com/65535/51219339295_f8a5e6cf3c_w_d.jpg" class="flickr-img flickr-img--medium" />
  <figcaption></figcaption>
</figure>

Johannes ist seit vielen Jahren in der SMJ unterwegs. Als kleiner Bub war er schon im Zeltlager dabei, und war auch fleißig in seiner Lieblingsbeschäftigung: Holz machen.
Später wurde er Gruppenführer, dann Kreisleiter und ist nun als geistlicher Begleiter tätig. Wir hoffen, dass du uns noch viele Jahre in der SMJ erhalten bleibst, und
freuen uns schon auf deine Zeltlagerprimiz im Sommer.

Cornabedingt waren die Kapazitäten im Dom beschränkt. Trotzdem haben wir mit einer starken Fahnenabordnung, inklusive der großen JESUS-Fahne, an der Feier teilgenommen und damit auch alle verbundenen Schönstätter vertreten, die gern dabei gewesen wären. Immerhin war es möglich, im Livestream mitzufeiern.

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51218495983_dbb82f3ec6_c_d.jpg">
</figure>

<figure data-href="https://www.flickr.com/photos/straight-shoota/51219335655/in/album-72157719318223638/">
  <img src="https://live.staticflickr.com/65535/51219335655_4de334f56c_w_d.jpg">
</figure>
<figure data-href="https://www.flickr.com/photos/straight-shoota/51218477273/in/album-72157719318223638/">
  <img src="https://live.staticflickr.com/65535/51218477273_4b0215831d_w_d.jpg">
</figure>

Am nächsten Tag, dem Pfingstsonntag, stand direkt die nächste große Festlichkeit an: Die Primiz in der Pfarrkirche St. Peter Fulda-Bronzell. Der Neupriester Johannes feierte seine erste heilige Messe in seiner Heimatpfarrei. In dieser Kirche wurde er schon getauft, war Ministrant und Küster.

Neben seiner Heimatgemeinde nahmen auch viele Mitbrüder und Weggefährten aus dem ganzen Bistum an der Feier teil. Auch einige Schönstätter waren von weiter her angereist. Besonders zu nennen sind die Gruppen der SMJ Mainz/Limburg und der SMJ Speyer, mit denen wir in der Regio Mitte verbunden sind.

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51218264811_35885072a4_c_d.jpg">
</figure>

<figure data-href="https://www.flickr.com/photos/straight-shoota/51218477388/in/album-72157719318223638/">
  <img src="https://live.staticflickr.com/65535/51218477388_834aec1f79_c_d.jpg">
</figure>

Der Primiziant wurde vor der Messe in seinem Elternhaus abgeholt und in feierlicher Prozession zur Kirche begleitet. Mit Weihrauch, Fahnen und Blaskappelle, versteht sich.
Vor dem Kirchenportal war ein Blumenteppich gelegt, der als Symbole die Opfergaben Wein und Brot in einem Schönstatt-Kapellchen zeigt. Dieser Blumenteppich war eine freudige Überraschung für Johannes, die wir ihm als Gemeinschaftsaktion der Schönstattjugend Fulda bereitet haben.

<figure data-href="https://www.flickr.com/photos/straight-shoota/51217554067/in/album-72157719318223638/">
  <img src="https://live.staticflickr.com/65535/51217554067_020e8e7636_w_d.jpg">
</figure>

Am Samstag haben wir nach der Weihe noch stundenlang Blumen gepflückt. Man glaubt gar nicht, wie schwierig es sein kann, ausreichende Mengen an Blüten in den richtigen Farben zu finden.
Mit einem ausreichenden Vorrat ausgestattet, ging es dann am Sonntagmorgen um kurz nach sieben los. Keiner von uns hatte irgendwelche Erfahrung mit dem Legen von Blumenteppichen, also starteten wir ziemlich im Blindflug. Ein paar Erkundigungen hatten wir immerhin eingeholt, wie das ungefähr funktioniert. Ein Plan für das Motiv war auch vorab vorbereitet.
Und irgendwie hat es auch ziemlich gut geklappt. Die gesammelten Blüten haben ziemlich gut ausgereicht, insbesondere die Verteilung auf die verschiedenen Farben. Das Resultat konnte sich durchaus sehen lassen. Es hat auch den Primizianten so sehr erfreut, dass er sich extra nochmal vergewisserte, ob er da auch wirkich drüberlaufen solle. Klar, dafür ist es ja gedacht. Und es waren hinterher auch keine Fußspuren zu sehen.

<figure class="full-width" data-href="https://www.flickr.com/photos/straight-shoota/51219336175/in/album-72157719318223638/">
  <img src="https://live.staticflickr.com/65535/51219336175_22714c5cc8_c_d.jpg">
</figure>

Die SMJ-Fulda hat somit nun eine neue Kernkompetenz erworben, die man so vielleicht nicht von einer Mannesjugend erwartet hätte. Vielleicht gibt's ja demnächst im <a href="/zeltlager">Zeltlager</a> auch einen Blumenteppich-Workshop. Wer weiß&hellip;

Wir freuen uns auf jeden Fall ungemein über den neugeweihten SMJ-Priester aus unseren Reihen.
Am 18. Juni findet im Rahmen der Bündnisfeier eine Nachprimiz am Schönstattheiligtum in Dietershausen statt.

<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/straight-shoota/albums/72157719318223638">
  <a href="https://www.flickr.com/photos/straight-shoota/51219336185/in/album-72157719318223638/"><img src="https://live.staticflickr.com/65535/51219336185_9ccf8c99e6_q.jpg" data-src-large="https://live.staticflickr.com/65535/51219336185_9ccf8c99e6_b.jpg"></a>
  <a href="https://www.flickr.com/photos/straight-shoota/51217554157/in/album-72157719318223638/"><img src="https://live.staticflickr.com/65535/51217554157_822ca6bfba_q.jpg" data-src-large="https://live.staticflickr.com/65535/51217554157_822ca6bfba_b.jpg"></a>
  <a href="https://www.flickr.com/photos/straight-shoota/51219336400/in/album-72157719318223638/"><img src="https://live.staticflickr.com/65535/51219336400_030bc45e08_q.jpg" data-src-large="https://live.staticflickr.com/65535/51219336400_030bc45e08_b.jpg"></a>
  <a href="https://www.flickr.com/photos/straight-shoota/51218265256/in/album-72157719318223638/"><img src="https://live.staticflickr.com/65535/51218265256_a06db07e9e_q.jpg" data-src-large="https://live.staticflickr.com/65535/51218265256_a06db07e9e_b.jpg"></a>
  <a href="https://www.flickr.com/photos/straight-shoota/51219030739/in/album-72157719318223638/"><img src="https://live.staticflickr.com/65535/51219030739_7873f35245_q.jpg" data-src-large="https://live.staticflickr.com/65535/51219030739_7873f35245_b.jpg"></a>
  <a href="https://www.flickr.com/photos/straight-shoota/51218477998/in/album-72157719318223638/"><img src="https://live.staticflickr.com/65535/51218477998_ee9855a649_q_d.jpg" data-src-large="https://live.staticflickr.com/65535/51218477998_ee9855a649_b.jpg"></a>
  <a href="https://www.flickr.com/photos/straight-shoota/albums/72157719318223638" class="flickr-link" title="Bildergalerie Zeltlager 2009 auf Flickr">Bildergalerie</a>
</figure>



