---
title: "Arbeitseinstatz im Schönstattzentrum Dietershausen"
excerpt: >
  Eine Truppe SMJler hat sich zum Arbeitseinsartz im Schönstattzentrum getroffen um den Schwestern bei der Gartenarbeit und anderen Dingen zu helfen.
author: Christian Schopp
tags: [smjfulda, schönstattzentrum-dietershausen]
image:
    cover: "https://farm5.staticflickr.com/4457/37630264060_60e9dacae7_k_d.jpg"
    align: top
---

<figure>
  <img src="https://farm5.staticflickr.com/4468/24035357408_2a84d9480f_z_d.jpg">
</figure>

Herzlich Willkommen zum Arbeitseinsatz. Schon beim Wildschweinwettrennen am Donnerstagabend konnten wir unser großes Können unter Beweis stellen und gewannen mit einem beachtlichen Vorsprung. Das lag hauptsächlich an unserer sportlichen Leistung. Nur nebenbei sei erwähnt, dass die vier Wildschweine die korrekte Rennstrecke nicht kannten und deshalb disqualifiziert wurden.

Am Abend ging es also los mit dem großen Schaffen. Tischtennisplatte, Großeinkauf und Kelleraufräumung stellten für uns natürlich nur kleine Hindernisse dar, die wir in kurzer Zeit bewältigen konnten. Danach gönnten wir uns eine wohlverdiente kurze Pause bis zum nächsten Morgen. Einen Dank an dieser Stelle an Johannes und Steffen, die hierbei schon so viel Engagement bewiesen, dass sie sich in den nächsten Tagen in Petersberg und Köln ausruhen mussten.

<figure>
  <img src="https://farm5.staticflickr.com/4460/37838955566_01076b9b2d_z_d.jpg">
</figure>

Freitagmorgen, 8:15 Uhr. Frühstück. Wer hat eben beim Wecken eigentlich die Dragonball-Melodie abgespielt? Immerhin: Musik macht müde Menschen munter! Nach dem Frühstück verabschiedeten wir uns von einem Viertel unserer starken Truppe und gingen mit geballter Manneskraft an die Aufgaben des Tages.

Nur kam der Herr Bleuel, der auf uns aufpassen sollte, erst um 11 Uhr. Also mussten wir uns selbst beschäftigen und nutzten die Gelegenheit unter Anderem um den Schwestern andere Reifen auf die Autos zu montieren (ob die das schon gemerkt haben?), die Tischtennisplatte einzuweihen, die Früchte des Zettelbaumes zu ernten und den Aufsitzrasenmäherführerschein zu absolvieren.

Als dann endlich Herr Bleuel kam, starteten wir mit dem Hauptprogramm. Die Außenanlage des Josef-Engling-Hauses musste verschönert werden. Und da wir in der SMJ gerne verschönern (Verschönerung muss man mit den Synonymen Zerstörung, Beschädigung oder - wie in diesem Fall - Zurückschneiden von Gebüsch und Ästen ersetzen), machten wir uns ans Werk.

Bis in die Nacht konnten wir unsere Stärke um erstaunliche 166% in Bezug auf die drei Arbeiter zu Beginn steigern. Das gab uns einen erstaunlichen Aufschwung, weshalb wir nach dem Abendessen zufrieden auf den Tag zurück blicken konnten. Ein Dank geht hier an die MJF, deren Lilienstübchen wir mitsamt der zu kurzen Betten benutzen durften. Auch die Schnellstraße zu den Toiletten sei hier erwähnt, ohne die unsere Arbeit am Samstag vermutlich mit einem Putzeimer gestartet wäre.

<figure>
  <img src="https://farm5.staticflickr.com/4445/37167498983_d0dcfd7e62_z_d.jpg">
</figure>

Am Samstag waren wir nun endlich vollzählig. Wir nahmen also die Arbeit vom Freitag wieder auf und widmeten uns der äußerst kleinen Außenanlage unseres Schönstattzentrums. Heckenschere, Laubgebläse, Astschere, Gabel, Rechen, Laubkratzer, Säge und Besen leisteten hierbei gute Dienste. Vor Allem die Hecke unterhalb des Jugendheims musste unter dem großen Druck unserer Anwesenheit leiden. Um diesem Druck Einhalt zu gebieten, waren diesmal sogar zwei erfahrenere Aufsichtspersonen nötig. Herr Bleuel und Herr Jestädt halfen uns, das gesammelte Laub und die zurückgeschnittenen Äste auf den Traktoranhänger von Herrn Jestädt zu laden.

Zu guter Letzt konnten die beiden Herren unserem Druck doch nicht ganz standhalten und eine 20m lange Fichte wurde in die Waagrechte verschoben, zersägt und auf zwei Haufen sowie eine Ansammlung von ca. 4 m<sup>3</sup> Holz verteilt. Wir bedanken uns beim Nachbarn, dessen Namen wir nicht kennen, für die Bereitstellung der Heckenschere, mit deren Hilfe der Hecke keine Chance mehr blieb. Außerdem wollen wir uns bei den Eltern, Großeltern, Verwandten, Bekannten und den Schönstatt-Schwestern aus Dietershausen für die Bereitstellung der Werkzeuge bedanken, die wir großzügig zu nutzen wussten. Auch Herrn Bleuel und Herrn Jestädt sei an dieser Stelle ein Dank für die fachmännische Unterstützung und die gute Zusammenarbeit ausgesprochen.

<figure>
  <img src="https://farm5.staticflickr.com/4498/37177358854_330c998b41_z_d.jpg">
</figure>

Um ein Dankeschön ging es dann am Samstagabend. Mit einer kleinen Feier bedankten wir uns bei der MJF für die farbliche Neuorientierung des Rhönstübchens. Der geschmackvoll und hoch professionell eingerichtete Raum strahlte in einem stilechten Mix aus Halloween- und Einhornambiente. Pizza, Einhornpinata und Einhornpupskuchen (genial!) konnten unseren Hunger schließlich nahezu stillen. Es bleibt lediglich die Frage, wo und wann die verschwundenen Pommes wieder auftauchen werden. Aber das klären wir in der nächsten Folge… Ein Dankeschön nochmal an die MJF für deren Mühen und einen gelungenen Abend.

Auch der Sonntag bot mit Aufstehen (vielen Dank an die meisterliche musikalische Untermalung), Frühstück, Aufräumen, Gottesdienst, Aufräumen, Mittagessen und Aufräumen ein reichhaltiges Programm. Nachdem wir erschöpft aber glücklich zuhause angekommen sind, wollen wir nun auch den Schwestern für deren Vertrauen in unser handwerkliches Geschick und die gekonnte Versorgung mit allen nötigen Klein- und Großigkeiten ein herzliches Dankeschön aussprechen.

Zum Abschluss bleibt mir nur eins zu sagen: Es war schön mit euch, ich freue mich auf den nächsten Einsatz. Und vergesst nicht: Trinkt euren Sekt!

