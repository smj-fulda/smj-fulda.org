---
tags: [smjfulda, gewo]
migrated:
  node_id: 922
  migrated_at: 2017-02-21 14:41
  alias: artikel/922-auf-königlicher-mission
  user_id: 179
tagline: Jungs am Gemeinschaftswochenende in Dietershausen
image:
  cover: https://farm9.staticflickr.com/8068/8164606170_e1aab37f0b_b.jpg
author: steffen höfer
title: Auf königlicher Mission
created_at: 2012-11-07 20:19
excerpt: 'Das Gemeinschaftswochenende der SMJ-Fulda begann am 2.November mit dem Thema „Auf königlicher Mission“. Nachdem sich alle Jungs auf ihren Zimmern eingerichtet hatten, gab es um 6 Uhr Abendessen. Alle zeigten ihre Spannung auf das Wochenende durch zahlreiche Gespräche und Fragen über den Verlauf der nächsten Tage. Nach dem Abendessen trafen sich alle im Heiligtum, um dort gemeinsam das Wochenende zu beginnen. Anschließend trafen wir uns im Roten Saal und begannen das Gemeinschaftswochenende mit einigen Kennenlernspielen wie Zeitungsschlagen und chinesisch Fußball. Wir ließen den Tag mit Spaß und Spiel schließlich dann durch ein Abendgebet im Heiligtum ausklingen. Um 22 Uhr hieß es dann für die Jungs der SMJ: Nachtruhe.'
---
<p>Das Gemeinschaftswochenende der SMJ-Fulda begann am 2.November mit dem Thema &bdquo;Auf königlicher Mission&ldquo;.</p>
<p>Nachdem sich alle Jungs auf ihren Zimmern eingerichtet hatten, gab es um 6 Uhr Abendessen. Alle zeigten ihre Spannung auf das Wochenende durch zahlreiche Gespräche und Fragen über den Verlauf der nächsten Tage. Nach dem Abendessen trafen sich alle im Heiligtum, um dort gemeinsam das Wochenende zu beginnen.</p>
<p>Anschließend trafen wir uns im Roten Saal und begannen das Gemeinschaftswochenende mit einigen Kennenlernspielen wie Zeitungsschlagen und chinesisch Fußball. Wir ließen den Tag mit Spaß und Spiel schließlich dann durch ein Abendgebet im Heiligtum ausklingen. Um 22 Uhr hieß es dann für die Jungs der SMJ: Nachtruhe.</p>
<p>Am nächsten Morgen wurden die Jungs dann um 7:30 Uhr geweckt und anschließend begannen wir den Tag mit Frühsport und einem Morgengebet im Heiligtum. Danach gab es ein leckeres Frühstück mit Brötchen, Milch, Kakao und Aufschnitt. Nach dem Frühstück bekamen die Jungs Zeit für die Körperpflege.</p>
<p>Einige Zeit später hörten wir dann ein Referat von Lukas über u.a. Rittertugenden und beschäftigten uns mit diesem Thema dann in der Gruppenstunde. Dort fertigten wir eine Mind-Map an, die einen Ritter in der Mitte zeigte, der von Rittertugenden umringt war. Die Ergebnisse der einzelnen Gruppen wurden dann vorgetragen. Die Ergebnisse wurde dann nochmal zusammengefasst und um 12 Uhr hieß es dann: Mittagessen.</p>
<figure data-href="https://www.flickr.com/photos/45962678@N06/8164606170">
  <img src="https://farm9.staticflickr.com/8068/8164606170_e1aab37f0b_c.jpg" class="flickr-img flickr-img--medium" alt="Kickerturnier in der Freudenquellarena" />
  <figcaption>Kickerturnier in der Freudenquellarena (<time datetime="2012-11-03 20:16:23">2012</time>)</figcaption>
</figure>
<p>Nach der Mittagspause wurden dann im Roten Saal Ritterschilder gebastelt. Sie waren aus Holz und wurden dann von den Jungs bemalt. Die Farbenfrohen Schilder wurden dann zum Trocknen hingelegt. Um 17:30 Uhr folgte dann die Heilige Messe im Heiligtum.</p>
<p>Nach dem Abendessen folgte dann der &bdquo;Top Act&ldquo;. Es fand ein spannendes Kicker-Turnier statt und alle Jungs hatten Spaß dabei. Der Abend wurde dann mit einer Nachtwanderung und dem Abendgebet im Heiligtum abgeschlossen.</p>
<p>Nach dem Frühstück am Sonntagmorgen wurden dann die Zimmer fertig für die Abreise gemacht. Die Betten wurden abgezogen und die Zimmer wurde durchgefegt. Anschließend wurden die Ritterschilder noch mit Haltegriffen versehen und die Zeit bis zum Mittagessen wurde mit einer kurzen Zusammenfassung des Wochenendes von Steffen gefüllt. Nach dem Mittagessen wurden einige Jungs von ihren Eltern abgeholt und alle anderen blieben mit ihren Eltern auf dem Elternnachmittag, der um 13:30 Uhr begann. Dort wurden unter anderem die Bilder des Zeltlagers angeschaut.</p>
<p>Ein sehr lustiges und spannendes Gemeinschaftswochenende ging zu Ende und die Jungs nahmen auch nützliche Kenntnisse über Gemeinschaft und Hilfsbereitschaft mit nach Hause.<br />
	<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157631954723440">
  <a href="https://www.flickr.com/photos/45962678@N06/8164718318/in/album-72157631954723440"><img src="https://farm9.staticflickr.com/8488/8164718318_c3fb80c89b_q.jpg" alt="Gruppenrunde" data-src-large="https://farm9.staticflickr.com/8488/8164718318_c3fb80c89b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164709758/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7109/8164709758_ac9c7a21a3_q.jpg" alt="Gruppenarbeit" data-src-large="https://farm8.staticflickr.com/7109/8164709758_ac9c7a21a3_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164666525/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7271/8164666525_28291df46d_q.jpg" alt="Gruppenarbeit" data-src-large="https://farm8.staticflickr.com/7271/8164666525_28291df46d_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164695002/in/album-72157631954723440"><img src="https://farm9.staticflickr.com/8338/8164695002_50ba889bf8_q.jpg" alt="Gruppenarbeit" data-src-large="https://farm9.staticflickr.com/8338/8164695002_50ba889bf8_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164689126/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7128/8164689126_d7dd27c775_q.jpg" alt="Vorbereitung für das Kickerturnier" data-src-large="https://farm8.staticflickr.com/7128/8164689126_d7dd27c775_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164682536/in/album-72157631954723440"><img src="https://farm9.staticflickr.com/8484/8164682536_f1c540d467_q.jpg" alt="Gruppenarbeit" data-src-large="https://farm9.staticflickr.com/8484/8164682536_f1c540d467_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164676740/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7108/8164676740_9c52de32b6_q.jpg" alt="Gruppenrunde" data-src-large="https://farm8.staticflickr.com/7108/8164676740_9c52de32b6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164633519/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7246/8164633519_b9c59a3e99_q.jpg" alt="Gruppenrunde" data-src-large="https://farm8.staticflickr.com/7246/8164633519_b9c59a3e99_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164626279/in/album-72157631954723440"><img src="https://farm9.staticflickr.com/8197/8164626279_1fa690873d_q.jpg" alt="Mittagstisch" data-src-large="https://farm9.staticflickr.com/8197/8164626279_1fa690873d_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164653826/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7109/8164653826_4e1467b3b6_q.jpg" alt="Waffenschmiede" data-src-large="https://farm8.staticflickr.com/7109/8164653826_4e1467b3b6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164627238/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7139/8164627238_25aca5366d_q.jpg" alt="Waffenschmiede" data-src-large="https://farm8.staticflickr.com/7139/8164627238_25aca5366d_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164584545/in/album-72157631954723440"><img src="https://farm9.staticflickr.com/8347/8164584545_f63c1e25c6_q.jpg" alt="Waffenschmiede" data-src-large="https://farm9.staticflickr.com/8347/8164584545_f63c1e25c6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164613000/in/album-72157631954723440"><img src="https://farm8.staticflickr.com/7136/8164613000_cc5e06af35_q.jpg" alt="Waffenschmiede" data-src-large="https://farm8.staticflickr.com/7136/8164613000_cc5e06af35_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/8164606170/in/album-72157631954723440"><img src="https://farm9.staticflickr.com/8068/8164606170_e1aab37f0b_q.jpg" alt="Kickerturnier in der Freudenquellarena" data-src-large="https://farm9.staticflickr.com/8068/8164606170_e1aab37f0b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157631954723440" class="flickr-link" title="Bildergalerie Gemeinschaftswochenende November 2012 auf Flickr">Bildergalerie</a>
</figure>
