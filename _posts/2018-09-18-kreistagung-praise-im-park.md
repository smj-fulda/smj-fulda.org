---
titel: "Kreistagung der Wolf-Gang mit Praise im Park"
tags: [kreise]
author: Jonas Wolf
image:
  cover: https://farm2.staticflickr.com/1874/30892210668_04439293ea_k_d.jpg
excerpt: >
  Die Wolf-Gang hat sich zum Kreiswochenende getroffen und gemeinsam am Praise im Park mitgeholfen, sowie thematische Vorbereitung auf die Mitarbeiterweihe gemacht.
---

Die vergangene Kreistagung vom 7. bis 9. September war geprägt vom gleichzeitig stattfindenden Festival Praise im Park in Hünfeld. Das Fest für junge Christen aus dem ganzen Bistum wir auch von der SMJ-Fulda mitorganisiert.

Dort halfen wir den ganzen Samstag mit: Zuerst beim Aufbauen und während dem Fest beim Betreuen unseres Bubble-Soccer-Spielfeldes. Natürlich spielten wir auch selbst die ein oder andere Runde Bubble Soccer.

![](https://farm2.staticflickr.com/1885/30892211838_948b27012a_z_d.jpg)

Ebenso nahmen wir an den Highlights im Programm teil, dem Gottesdienst mit Jugendpfarrer Alexander Best, und den abendlichen Konzerten von *Good Weather Forecast* und *One Collective*.

Den Freitag und Sonntag haben wir als Selbstversorgerwochenende im Lilienstübchen verbracht. Freitag abend gab es Toast Hawaii und Sonntags wurden Steaks gebraten.

Als thematische Einheit lag der Fokus auf der Vorbereitung für die Mitarbeiterweihe und vor allem bei der Wiederholung der [Fünf Säulen der SMJ]({{ "/säulen" | relative_link }}). Bei leckeren Getränken und verschiedenen Kartenspielen habe wir die Tage im Rhönstübchen ausklingen lassen.

![](https://farm2.staticflickr.com/1871/44762712811_cb145f3ef3_z_d.jpg)
![](https://farm2.staticflickr.com/1882/44762711971_89756079ba_z_d.jpg)
