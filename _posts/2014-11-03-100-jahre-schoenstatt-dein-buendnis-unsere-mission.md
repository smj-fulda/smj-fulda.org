---
tags: [schönstatt, jubiläum2014]
migrated:
  node_id: 989
  migrated_at: 2017-02-20 22:28
  alias: artikel/989-100-jahre-schönstatt:-dein-bündnis-–-unsere-mission
  user_id: 132
tagline: Pilgerwallfahrt nach Rom
author: Steffen Büdel
title: '100 Jahre Schönstatt: Dein Bündnis – unsere Mission'
created_at: 2014-11-03 16:21
excerpt: 'Nach dem feierlichen Jubiläumswochenende vom 17. Bis 19. Oktober in Schönsatt fand nun eine Woche später die Begegnung der ganzen internationalen Schönstattfamilie mit Papst Franziskus statt! Vom 24. Bis 26. Oktober trafen sich in Rom tausende Pilger aus der ganzen Welt um gemeinsam des 100-jährigen Bestehens der Schönstattbewegung zu gedenken. Auch aus der Diözese Fulda waren einige Gläubige mit dem Bus nach Rom gepilgert. Desweiteren waren auch drei Jungmänner aus Fulda mit dabei: Christoph und Christian Schopp flogen zusammen mit Steffen Büdel nach Rom um dort das Wochenende zu verbringen und an den Feierlichkeiten teilzunehmen.'
---
<p>Nach dem feierlichen Jubiläumswochenende vom 17. Bis 19. Oktober in Schönsatt fand nun eine Woche später die Begegnung der ganzen internationalen Schönstattfamilie mit Papst Franziskus statt!</p>
<p>Vom 24. Bis 26. Oktober trafen sich in Rom tausende Pilger aus der ganzen Welt um gemeinsam des 100-jährigen Bestehens der Schönstattbewegung zu gedenken.</p>
<p>Auch aus der Diözese Fulda waren einige Gläubige mit dem Bus nach Rom gepilgert. Desweiteren waren auch drei Jungmänner aus Fulda mit dabei: Christoph und Christian Schopp flogen zusammen mit Steffen Büdel nach Rom um dort das Wochenende zu verbringen und an den Feierlichkeiten teilzunehmen.</p>

<p>Der Freitag stand unter dem Motto &bdquo;Pilgernde Kirche&ldquo;. Es gab das Angebot verschiedene Marienkirchen zu besuchen oder auch zu den Schönstattheiligtümern zu pilgern.</p>
<p>Am Abend stand das Jugendprogramm an, das zunächst mit der Eröffnungsmesse mit anschließender Wallfahrt zum Petersplatz begann. Organisiert wurde es von den Jugendgemeinschaften.</p>

<p>Im Mittelpunkt des Samstags mit dem Motto &bdquo;Communio &ndash; Kirche als Familie&ldquo; stand natürlich die Begegnung mit Papst Franziskus! Die Audienz mit dem Heiligen Vater begann um 11 Uhr. Ca. 8000 Pilger kamen zu diesem Ereignis und versammelten sich in der Audienzhalle Paul VI. im Vatikan.</p>
<p>Nach einem kurzen Rückblick auf das Jubiläumswochenende in Schönstatt kam der Papst und stellte sich nach der Eröffnungsrede von Pater Heinrich Walter einigen Fragen der Schönstattfamilie. Insgesamt war Papst Franziskus 2 Stunden bei den Pilgern. Es war eine Zeit des Dialogs, in dem der Papst immer wieder erwähnte, dass Maria die Mutter der Kirche und damit auch die Mutter aller Gläubigen ist.</p>
<p>Franziskus forderte die Schönstattfamilie auf, stärker missionarisch zu wirken und nach außen zu gehen um den Glauben weiter zu tragen. Auch an die Jugend hatte der Papst etwas zu sagen: Sie sollen ein lebendiges Zeugnis für den christlichen Glauben geben. Zum Abschluss der bewegenden und sehr interessanten Audienz legte Franziskus noch einen Blumenstrauß als Symbol seiner Verehrung für die Gottesmutter vor das Marienbild.</p>
<p>Am Nachmittag und frühen Abend gab es die Möglichkeit Gottesdienste in vielen unterschiedlichen Sprachen zu besuchen. Dabei gab es verschiedene Angebote durch die vielen Nationen, in denen Schönstatt vertreten ist.</p>
<p>Die SMJ`ler aus Fulda gingen um 17 Uhr in die Basilika Sant`Andrea della Valle. Der weitere Abend verbrachten sie in der Innenstadt und ließen dort den Tag gemütlich ausklingen.</p>

<p>&bdquo;Missionarische Kirche&ldquo; - so lautete die Überschrift für den Sonntag. Damit eng verbunden war auch der Aussendungsgottesdienst im Petersdom. Hierbei stand nochmal der Auftrag aller Pilger im Mittelpunkt, missionarisch in der Welt zu wirken.</p>
<p>Zur Mittagsstunde versammelten sich die Pilger auf dem Petersplatz um gemeinsam mit dem Papst den Angelus zu beten. Der Papst grüßte nochmal alle Pilger und segnete sie.</p>

<p>Mit dem Segen des Papstes und der Erfahrung dieser Pilgergemeinschaft war es Zeit für den Aufbruch in die jeweilige Heimat!</p>

<p>Dein Bündnis &ndash; unsere Mission</p>
