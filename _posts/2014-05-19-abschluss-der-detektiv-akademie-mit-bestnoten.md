---
tags: [smjfulda, gewo]
migrated:
  node_id: 980
  migrated_at: 2017-02-20 22:28
  alias: artikel/980-abschluss-der-detektiv-akademie-mit-bestnoten
  user_id: 1084
tagline: Nach langem, ausführlichem Training verabschiedet sich ein weiterer Jahrgang mit ausschließlich Bestnoten von der Detektiv Akademie der SMJ Fulda.
author: lukas.helfrich
title: Abschluss der Detektiv-Akademie mit Bestnoten
created_at: 2014-05-19 22:26
excerpt: Mit Bravur haben 15 Jungdetektive ihre Abschlussprüfung am Mai-GeWo der SMJ Fulda bestanden und stehen somit nun fast auf einer Stufe mit Sherlock Holmes. Am Freitag Abend, nach der Anreise, begann das Gemeinschaftswochenende wie gewohnt mit einer guten Stärkung im Speisesaal des Josef-Engling-Hauses und dem anschließenden Besuch im Heiligtum. Danach verbrachte man die Zeit mit kennenlernen bzw. wieder kennenlernen, denn viele waren nicht zum ersten Mal dabei. Nach vielen anstrengenden, spaßigen und lustigen Spielen und dem Abendgebet ging es erstmal ins Bett, denn es sollte ein langer Samstag folgen.
---
<p>Mit Bravur haben 15 Jungdetektive ihre Abschlussprüfung am Mai-GeWo der SMJ Fulda bestanden und stehen somit nun fast auf einer Stufe mit Sherlock Holmes.</p>
<p>Am Freitag Abend, nach der Anreise, begann das Gemeinschaftswochenende wie gewohnt mit einer guten Stärkung im Speisesaal des Josef-Engling-Hauses und dem anschließenden Besuch im Heiligtum. Danach verbrachte man die Zeit mit kennenlernen bzw. wieder kennenlernen, denn viele waren nicht zum ersten Mal dabei. Nach vielen anstrengenden, spaßigen und lustigen Spielen und dem Abendgebet ging es erstmal ins Bett, denn es sollte ein langer Samstag folgen.</p>
<p>Nachdem alle spätestens durch eine ganze Woche &bdquo;Laurentia&ldquo; beim Frühsport komplett wach waren, wurde sich bis zum Mittagessen erst einmal mit dem auseinander gesetzt, was ein waschechter Detektiv denn überhaupt finden will: Keine verborgenen Schätze, keine Bananenschalen, nein: Die Wahrheit und nichts anderes! Nach dem eher theoretischen Morgen mit jedoch ausreichend Spielen und Freizeit zwischendurch, ging es nach dem Mittagessen und einer langen Mittagspause voller Fußball und Kletterei ans Eingemachte: Das Erlernen der investigativen Fähigkeiten eines Detektives. Hierzu mussten versteckte Hinweise gefunden, Rätsel gelöst, Kriminalgeschichten aufgedeckt und eiserne Detektivsgeduld bewiesen werden. Am Ende hatte jeder alle Teile seines persönlichen Puzzles zusammen und musste den darauf niedergeschriebenen Code knacken, was allen ohne größere Probleme gelang. Die Lösung des Codes war die jeweilige Sitzposition zum Abendessen und die entsprechende Gruppenzugehörigkeit für das Abendprogramm danach: Die Jagd nach <em>Mr. X</em> mit den Kollegen vom Scotland Yard. Der erste Mr. X war auch aufgrund der tadellosen Zusammenarbeit der Detektive im Handumdrehen geschnappt, der zweite überzeugte jedoch mit seiner Cleverness und entrann seinen Verfolgern, aber nur um Haaresbreite!</p>
<p>Am Sonntag bereiteten sich alle auf die heilige Messe um 10:30 Uhr vor, die in der Gott-Vater Kirche mit zelebriert wurde. Nach dem anschließenden aufräumen, packen und Mittagessen war das Gemeinschaftswochenende dann aber leider schon vorbei und alle gingen voller Vorfreude auf das <a href="/zeltlager">Zeltlager 2014 (29. Juli bis 9. August)</a> nach Hause.</p>
<p>Hochs des Wochenendes:</p>
<ul>
	<li>Die Kletterkinder, im wahrsten Sinne des Wortes</li>
	<li>4 Leute Namens Lukas und zwei namens Elias, was zur ein oder anderen Verwirrung führte</li>
	<li>Das Spitzenwetter</li>
</ul>
<p>Tiefs:</p>
<ul>
	<li>gab&rsquo;s keine!</li>
</ul>
