---
title: "Zeltlager-Überfälle"
tagline: "Nachtwache im Zeltlager 2019"
author: Christian Schopp
tags: [smjfulda, zeltlager]
image:
    cover: "!baseurl!/images/2017/2017-06-28-zeltlager.jpg"
---

Unser Zeltlager bietet viele Highlights. Eines davon ist die Nachtwache am Lagerfeuer. Jeder von uns bekommt einmal die Aufgabe, auf den Lagerplatz, die Zelte und alle Teilnehmer aufzupassen. Eine große Verantwortung!

Aber was wäre Nachtwache ohne nächtlichen Besuch? Wir freuen uns über euren Überfall auf das Zeltlager 2019. Wir tun unser Bestes um Fahne, Glocke und das gesamte Lager vor euch zu beschützen.

Dieses Jahr zelten wir vom 30. Juni bis 7. Juli auf dem Zeltplatz *Am Reith* in Hausen (Rhön). Die Koordinaten sind [50.508416, 10.144408](https://goo.gl/maps/LRExjeWq3SmNoW817).

Überfälle sind in jeder Nacht möglich außer in der Nacht vom 3. auf 4. Juli (Mittwoch/Donnerstag) und vom 5. auf 6. Juli (Freitag/Samstag).
Die Nachtwache und Überfallszeit beginnt jeweils nach Programmende gegen 23:15 Uhr und endet im Morgengrauen um 4:00 Uhr.

Zur Sicherheit aller Beteiligter müssen Überfälle vorab beim Lagerleiter angemeldet werden. Dieser behält die Information für sich. Kein anderer Betreuer erhält Kenntnis über angemeldete Überfälle. Deshalb bitten wir euch, euren Überfall rechtzeitig beim Lagerleiter Christian Schopp unter der Telefonnummer 0175 8733388 anzumelden.

## Überfallcodex

1. Überfälle sollen Spaß machen und dazu tragen alle ihren Teil bei. Es sollen keine Beeinträchtigungen im Lageralltag entstehen.
2. Jeder Überfall muss beim Lagerleiter angemeldet werden (Tel.: 0175 8733388). Außer dem Lagerleiter ist im Lager niemand über bevorstehende Überfälle informiert.
3. Überfälle sind grundsätzlich von 23:15 Uhr bis 4:00 Uhr möglich.
4. Bei einem Überfall können verschiedene Dinge entwendet werden wie z. B. die Glocke oder die Fahne. Weiherhin soll dabei darauf geachtet, dass die entwendeten Gegenstände spätestens am nächsten Morgen wieder zum Eintauschen zum Zeltplatz gebracht werden.
5. Generell ist bei den Überfällen auf ein gewisses Maß zu achten wie das Benutzen von Pyrotechnik oder der Lautstärke generell.
6. Als gefangener Überfäller wird sich zum und in das Küchenzelt begeben und es wird nicht mehr in den laufenden Überfall eingegriffen.
7. Es ist unerwünscht, dass Lagerteilnehmer nach vorzeitigem Verlassen des Lagers überfallen. Auch ist es nicht möglich, den Platz vorab zu besichtigen und in einer späteren Nacht zu überfallen.
