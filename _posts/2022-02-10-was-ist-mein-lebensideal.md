---
title: Kreistagung vom 4. bis 6. Februar 2022
subtitle: Was ist mein Lebensideal?
author: Jonas Wolf
tags: [kreise]
image:
  cover: https://live.staticflickr.com/65535/51893238688_8577482711_o_d.jpg
---

Mit der Frage nach dem Lebensidel beschäftigten sich an der vergangene Kreistagung 17 junge Männer der SMJ Fulda in Dietershausen.
Los ging es am Freitagabend mit einer sehr inspirierenden Einführung von Christoph Götz unter dem Motto: *Was läuft in meinem Leben gerade ideal?*
Gefolgt von einer ordentlichen Portion Pizza zum Abendessen. Dieses Wochenende hatten wir nun auch die Möglichkeit die schönen Zimmer des Neubaus zu beziehen.

Am Samstag übernahmen Steffen Büdel, Kaplan Johannes Wende und Pfarrer Rudolf Liebig die Gestaltung des Tages.
Hier wurde der Fokus wieder individuell auf jeden einzelnen Teilnehmer gelegt, welche sich vor allem mit den Fragen: Was prägt mich? Und was ist für mein Leben entscheiden? beschäftigten.

Am Abend hieß es dann: *Aushalten, nicht lachen!* Nach Vorbild von Zirkus Halligalli spielten zwei Teams gegeneinander.
Die Regeln sind denkbar einfach: Ein Team führt einen Sketch vor und wenn aus dem anderen Team jemand lacht, verliert dieses einen Punkt. Am Ende des spannenden und vor allem sehr witzigen Abend hieß es dann überraschenderweise: Unentschieden! Denn beide Teams zeigten ein erstaunliches Durchhaltevermögen.
 
An dieser Kreistagung konnten wir einen neuen Kreis unter der Leitung von Simon Wawra begrüßen. Sein Kreis beschäftigte sich vor allem mit dem Thema Gemeinschaft und dem Bauen eines eigenen Hausheiligtums.

<figure class="full-width">
  <img src="https://live.staticflickr.com/65535/51893802855_8e4fd7572f_o_d.jpg" />
</figure>
