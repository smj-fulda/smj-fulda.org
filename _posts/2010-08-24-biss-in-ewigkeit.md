---
categories:
- Fest des Glaubens
- Kilian Machill
- Bis(s) in Ewigkeit
- '2010'
migrated:
  node_id: 834
  migrated_at: 2017-02-20 22:26
  alias: artikel/834-bis(s)-ewigkeit
  user_id: 1
tagline: Eingeladen zum Fest des Glaubens
title: Bis(s) in Ewigkeit
created_at: 2010-08-24 17:48
excerpt: Am 11. September findet im Bistum Fulda wieder das <em>Fest des Glaubens </em>statt, dieses Jahr unter dem an die Twilight-Saga angelehnten Motto <em>Bis(s) in Ewigkeit</em>. Zu diesem Glaubensfest werden Hunderte von Jugendlichen ab 14 Jahren im Schönstattzentrum Dietershausen erwartet. Auf dem Programm stehen eine Katechese von P. Stephan Möllmann OMI, ein Konzert mit der Band <em>1 + 1 = 11</em> aus Würzburg, Jugendgottesdienst sowie viele verschiedene Workshop-Angebote zu den unterschiedlichsten Themen. Beginn ist am Samstag den 11. September um 9:30 Uhr und die Veranstaltung endet gegen 21:00 Uhr – im Rahmen des Helferwochenendes gibt es auch für weiter angereiste Teilnehmer eine Übernachtungsmöglichkeit im Schönstattzentrum.
---
<p>Am 11. September findet im Bistum Fulda wieder das <em>Fest des Glaubens </em>statt, dieses Jahr unter dem an die Twilight-Saga angelehnten Motto <em>Bis(s) in Ewigkeit</em>. Zu diesem Glaubensfest werden Hunderte von Jugendlichen ab 14 Jahren im Schönstattzentrum Dietershausen erwartet. Auf dem Programm stehen eine Katechese von P. Stephan Möllmann OMI, ein Konzert mit der Band <em>1 + 1 = 11</em> aus Würzburg, Jugendgottesdienst sowie viele verschiedene Workshop-Angebote zu den unterschiedlichsten Themen. Beginn ist am Samstag den 11. September um 9:30 Uhr und die Veranstaltung endet gegen 21:00 Uhr &ndash; im Rahmen des Helferwochenendes gibt es auch für weiter angereiste Teilnehmer eine Übernachtungsmöglichkeit im Schönstattzentrum.</p>
<div class="section"><h3>&bdquo;Die Vorbereitungen laufen auf Hochtouren&rdquo;</h3>
<p>Veranstaltet wird das Jugendfest seit 15 Jahren als Gemeinschaftsprojekt von OMI-Jugend, Katholische Jugend Fulda, Schönstatt-Mannesjugend, Schönstatt-Bewegung Mädchen und Junge Frauen und der Malteserjugend mit Unterstützung durch das Bischöfliche Jugendamt.</p>
<p>Kilian Machill (19) von der Schönstatt-Mannesjugend ist dieses Jahr erstmals im K.O.-Team (Kern-Organisations-Team) dabei, dessen Mitglieder das Fest seit Anfang des Jahres vorbereiten. Seinen Eindruck von der Vorbereitungsarbeit schildert er so:</p>
<blockquote>&bdquo;Jeder im Team hat seine Aufgaben und Verantwortungen. Wir wollen den Jugendlichen, die aus dem ganzen Bistum extra nach Dietershausen kommen, ein wahres FEST DES GLAUBENS präsentieren. Aktiv Glauben erfahren, neue Leute kennen lernen und einfach einen Tag Spaß haben. Und um das zu schaffen ziehen alle im K.O.-Team an einem Strang.&rdquo;</blockquote><p>Es sind noch knapp zwei Wochen und das Fest des Glaubens kommt langsam immer deutlicher in Sichtweite, die Anmeldezahlen steigen Tag für Tag. Am 10. September beginnt dann das Helferwochenende mit rund 30 fleißigen Helfern, die sich um Technik, Deko, Platzgestaltung, Anmeldung und alle anderen kleinen Dinge kümmern, die vorzubereiten sind.</p>
</div>
<div class="section"><h3>Programm</h3>
<p>Der Referent ist gebucht: P. Stephan Möllmann OMI wird uns etwas über die Ewigkeit erzählen und eventuelle Biss-&bdquo;Verletzungen&rdquo; auf dem Weg dorthin. Die Gesprächskreise fallen dieses Jahr zu Gunsten einer großartigen, weltrekordverdächtigen Überraschungsaktion aus, die sich das K.O.-Team extra einfallen lassen hat. Der Nachmittag wird aber wieder mit interessanten, unterhaltsamen, kreativen, sportlichen, actionreichen und entspannenden Workshop-Angeboten gefüllt sein und es sind bereits viele tolle Workshop-Leiter mit vielfältigen Angeboten verpflichtet worden.</p>
<p>Eine Gruppe von knapp 30 Fuldaern konnte sich vergangenes Wochenende an der&nbsp;<em>Nacht des Heiligtums</em> in Schönstatt wieder einmal von der Qualität der dieses Jahr erstmalig am <em>Fest des Glaubens</em> spielenden Band&nbsp;<em>1 + 1 = 11</em> überzeugen, die dort für ausgezeichnete Stimmung sorgte. Auch die Band freut sich auf ein Wiedersehen in drei Wochen in Dietershausen, wenn sie dort die Zeltbühne rocken werden. Ihnen dürfte auch klar sein, mit welcher Liedauswahl sie in Fulda punkten werden!</p>
</div>
<div class="box extra"><p>Weitere Infos und Anmeldung unter <a href="http://festdesglaubens.de">festdesglaubens.de</a></p>
</div>
