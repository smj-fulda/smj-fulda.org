---
tags:
- smjfulda
- liebesbündnis
- kreise
- diözesanführer
migrated:
  node_id: 959
  migrated_at: 2017-02-20 22:28
  alias: artikel/959-zukunftsweisender-weg:-diözesanführerwahl-und-neue-bündnisse
  user_id: 1
tagline: Im Rahmen der Bündniskreistagung wurden 16 neue Mitarbeiter- und Mitgliederbündnisse geschlossen und die Diözesanführung im Amt bestätigt.
image:
  cover: https://farm4.staticflickr.com/3691/11310677215_38e01033cc_b.jpg
title: 'Zukunftsweisender Weg: Diözesanführerwahl und neue Bündnisse'
created_at: 2013-12-10 18:37
excerpt: Die Schönstatt-Mannesjugend im Bistum Fulda (SMJ) wählte am vergangenen Wochenende im Rahmen der Bündniskreistagung ihre Diözesanleitung. Der amtierende Diözesanführer Steffen Büdel und sein Stellvertreter Christoph Schopp wurden für zwei weitere Jahre im Amt bestätigt. Zuvor hatten 16 junge Männer ihr Engagement in der SMJ durch Mitarbeiter- und Mitgliederbündnisse bekräftigt.
---
<p>Die Schönstatt-Mannesjugend im Bistum Fulda (SMJ) wählte am vergangenen Wochenende im Rahmen der Bündniskreistagung ihre Diözesanleitung. Der amtierende Diözesanführer Steffen Büdel und sein Stellvertreter Christoph Schopp wurden für zwei weitere Jahre im Amt bestätigt. Zuvor hatten 16 junge Männer ihr Engagement in der SMJ durch Mitarbeiter- und Mitgliederbündnisse bekräftigt.</p>
<p>Der Samstag stand unter der Vorbereitung auf die Bündnismesse, die am Abend stattfinden sollte. Pfarrer Ulrich Schäfer, der Standesleiter der SMJ-Fulda erläuterte in seinem Vortrag zum Thema &bdquo;Geistliches Leben&ldquo;, wie jeder Einzelne sein spirituelles Leben pflegen kann und mit welchen Hilfsmitteln diese Lebensführung einfacher zu gestalten ist. Rund 30 Teilnehmer, Jugendliche und junge Männer zwischen 14 und 30 Jahren, waren zu dem Wochenende ins Schönstattzentrum Dietershausen gekommen. Sie gehören derzeit fünf Kreisen mit etwa Gleichaltrigen an, die sich in regelmäßigen Treffen mit ihrem Glauben, der Schönstatt-Bewegung und der Bedeutung für ihr Leben beschäftigen.</p>
<p>Am Abend schlossen in einem feierlichen Bündnisgottesdienst elf junge Männer das Mitarbeiterbündnis und fünf das darauf aufbauende Mitgliederbündnis. Damit bekunden sie für sich selbst und nach außen hin eine stärkere Vertiefung der Gemeinschaft und die Übernahme von Verantwortung für die SMJ. Eine Folgerung daraus ist die mit dem Mitarbeiterbündnis einhergehende Berechtigung zur Wahl der Diözesanführung. Diese fand im Anschluss an den Gottesdienst statt, sodass mit den neuen insgesamt 22 Wahlberechtigte teilgenommen haben.</p>
<p>Vor wenigen Jahren noch gab es bei den Diözesanführerwahlen meist nur einer Handvoll Wahlberechtigte, die das Mitarbeiterbündnis geschlossen hatten. Über die steigende Zahl von engagierten Mitarbeitern freut sich der wiedergewählte Diözesanführer Steffen Büdel: &bdquo;In den letzten Jahren haben sich aus den Kreisen heraus Viele zu Mitarbeiter- und Mitgliederbündnissen entschlossen. Das bedeutet natürlich auch viele Mitwirker, die sich entschlossen haben, sich für die Zukunft der SMJ einzubringen und unsere Jugendarbeit aktiv mitzugestalten.&rdquo;</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157638550657255">
  <a href="https://www.flickr.com/photos/45962678@N06/11310722546/in/album-72157638550657255"><img src="https://farm8.staticflickr.com/7381/11310722546_aac632929c_q.jpg" alt="20131207_170536" data-src-large="https://farm8.staticflickr.com/7381/11310722546_aac632929c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/11310722406/in/album-72157638550657255"><img src="https://farm8.staticflickr.com/7330/11310722406_5359b95b47_q.jpg" alt="20131207_170850" data-src-large="https://farm8.staticflickr.com/7330/11310722406_5359b95b47_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/11310752674/in/album-72157638550657255"><img src="https://farm6.staticflickr.com/5518/11310752674_94e79a8457_q.jpg" alt="20131207_171957" data-src-large="https://farm6.staticflickr.com/5518/11310752674_94e79a8457_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/11310721886/in/album-72157638550657255"><img src="https://farm4.staticflickr.com/3760/11310721886_f4b21c9a54_q.jpg" alt="20131207_173258" data-src-large="https://farm4.staticflickr.com/3760/11310721886_f4b21c9a54_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/11310677215/in/album-72157638550657255"><img src="https://farm4.staticflickr.com/3691/11310677215_38e01033cc_q.jpg" alt="20131207_174359" data-src-large="https://farm4.staticflickr.com/3691/11310677215_38e01033cc_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/11310676995/in/album-72157638550657255"><img src="https://farm8.staticflickr.com/7310/11310676995_741a036835_q.jpg" alt="20131207_175659" data-src-large="https://farm8.staticflickr.com/7310/11310676995_741a036835_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/11310751874/in/album-72157638550657255"><img src="https://farm3.staticflickr.com/2817/11310751874_40662aa04a_q.jpg" alt="20131207_180539" data-src-large="https://farm3.staticflickr.com/2817/11310751874_40662aa04a_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/11310676155/in/album-72157638550657255"><img src="https://farm6.staticflickr.com/5473/11310676155_16fae1c60c_q.jpg" alt="Steffen und Christoph" data-src-large="https://farm6.staticflickr.com/5473/11310676155_16fae1c60c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157638550657255" class="flickr-link" title="Bildergalerie Bündniskreistagung mit Diözesanführerwahl auf Flickr">Bildergalerie</a>
</figure>
<p>Zum Programm der SMJ-Fulda gehören unter anderem das jährliche Zeltlager und mehrere Gemeinschaftswochenenden für 9- bis 13-jährige, sowie in Zusammenarbeit mit anderen Jugendgemeinschaften das jährliche Fest des Glaubens und Glaubenskurs-Wochenenden. Über einen eigenen Online-Shop werden Produkte mit einem JESUS-Logo, dessen Bedeutung erst auf den zweiten Blick ersichtlich wird, angeboten. Derzeit kommen auf zahlreichen Mobiltelefonen in ganz Deutschland per SMS-Adventskalender täglich kleine Botschaften.</p>
<p>Das größte Projekt derzeit ist die Aktion 2014 zum hundertjährigen Jubiläum der Schönstatt-Bewegung im kommenden Jahr, bei der sich Mitglieder der SMJ-Fulda über das normale Engagement hinaus gehend für gute Zwecke engagieren und zusammen 2014 Stunden sammeln wollen.</p>
