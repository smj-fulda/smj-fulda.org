---
categories:
- Gemeinschaftswochenenden
migrated:
  node_id: 836
  migrated_at: 2017-02-20 22:26
  alias: artikel/836-zurück-die-heimat
  user_id: 84
tagline: Bericht vom dritten Gemeinschaftswochenende vom 29. bis 31. Oktober 2010
image:
  cover: https://farm2.staticflickr.com/1256/5158640929_5e9e7cfc0c_b.jpg
author: Marius
title: Zurück in die Heimat
created_at: 2010-11-06 10:22
excerpt: "„Die Lage ist verfahren. Das Schiff ist fast versenkt. Die Heimatrufe werden lauter. Werden wir das Ufer noch erreichen? …“ Wieder einmal war es so weit. Freitag stiegen wieder 13 mutige Jungs ein, um ein abenteuerliches Wochenende zu erleben. Schon früh wurde allen klar, es geht „Zurück in die Heimat“. Nachdem die Kajüten bezogen und der erste Hunger gestillt war, machte sich die Mannschaft auf, sich kennenzulernen und den ersten Abend mit viel Spiel und Spaß anzugehen. Erst nach einem langen Abend fiel die Mannschaft erschöpft, aber glücklich, ins Bett."
---
<div class="section">
<p>&bdquo;Die Lage ist verfahren. Das Schiff ist fast versenkt. Die Heimatrufe werden lauter. Werden wir das Ufer noch erreichen? &hellip;&ldquo;</p>
<p>Wieder einmal war es so weit. Freitag stiegen wieder 13 mutige Jungs ein, um ein abenteuerliches Wochenende zu erleben. Schon früh wurde allen klar, es geht &bdquo;Zurück in die Heimat&ldquo;.  Nachdem die Kajüten bezogen und der erste Hunger  gestillt war, machte sich die Mannschaft auf, sich kennenzulernen und den ersten Abend mit viel Spiel und Spaß anzugehen. Erst nach einem langen Abend fiel die Mannschaft erschöpft, aber glücklich, ins Bett.</p>
</div>
<figure data-href="https://www.flickr.com/photos/45962678@N06/5159303876">
  <img src="https://farm2.staticflickr.com/1060/5159303876_4d16536866_c.jpg" class="flickr-img flickr-img--medium" alt="" />
  <figcaption> (<time datetime="2010-10-30 15:57:13">2010</time>)</figcaption>
</figure>
<div class="section">
<p>Samstag ging es gleich morgens turbulent weiter: Etwas Sport um fit zu bleiben, Morgengebet im Kapellchen und anschließend das erwartete Frühstück. Danach gab es einen Beitrag zum Thema &bdquo;Das Heiligtum &ndash; meine Heimat&ldquo; und eine Gruppenstunde, die sich mit dem Gedanken der eigenen Heimat beschäftigte. Dabei kamen Fragen auf wie: Ist Heimat ein Gefühl oder ein Ort? Gibt es nur eine Heimat? Was ist für mich Heimat?</p>
<p>Vor und nach dem Mittagessen hatte die Mannschaft Zeit für Fußball, Poker oder einfach nur zum chillen. Nach der Freizeit begannen die Jungs mit dem Bau ihres eigenen Hausheiligtums aus Holz. Hierbei entstand das ein oder andere Kunstwerk und ungeahnte Talente wurden entdeckt. Nachdem die Heiligtümer fertig waren, war es Zeit für die Heilige Messe und die Segnung der Heiligtümer durch Pfarrer Thomas Maleja. Nach der Messe gab es das wohl verdiente Abendessen.</p>
<p>Am Abend wurden gemeinsam die Bilder des Zeltlagers 2010 in Hausen angeschaut und viele erinnerten sich noch gut an dieses 12-tägige Abenteuer. Während die einen spielten, gingen die Mutigsten raus, um eine Nachtwachtwanderung zu unternehmen. Dies brachte jedoch einige Schwierigkeiten mit sich. Nach völliger Orientierungslosigkeit schaffte es die Mannschaft doch noch zurück um gemeinsam das Abendgebet anzugehen. Nach diesem abenteuerlichen Tag kehrte Ruhe in den Kajüten ein.</p>
</div>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157625217278821">
  <a href="https://www.flickr.com/photos/45962678@N06/5158766825/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1119/5158766825_713360c826_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1119/5158766825_713360c826_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5158762669/in/album-72157625217278821"><img src="https://farm5.staticflickr.com/4036/5158762669_7c2494bd17_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4036/5158762669_7c2494bd17_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5159367410/in/album-72157625217278821"><img src="https://farm5.staticflickr.com/4050/5159367410_ea84d22f79_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4050/5159367410_ea84d22f79_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5158755423/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1065/5158755423_f1b9ff1934_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1065/5158755423_f1b9ff1934_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5159359576/in/album-72157625217278821"><img src="https://farm5.staticflickr.com/4004/5159359576_1d011d4965_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4004/5159359576_1d011d4965_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5158747535/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1098/5158747535_e221b220d7_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1098/5158747535_e221b220d7_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5158743187/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1105/5158743187_a87a25588e_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1105/5158743187_a87a25588e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5158738845/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1360/5158738845_a93f6b4aa6_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1360/5158738845_a93f6b4aa6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5158734877/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1375/5158734877_038aa109cd_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1375/5158734877_038aa109cd_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5159339502/in/album-72157625217278821"><img src="https://farm5.staticflickr.com/4009/5159339502_b83572627b_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4009/5159339502_b83572627b_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5158727129/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1121/5158727129_3d986cf679_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1121/5158727129_3d986cf679_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5159332254/in/album-72157625217278821"><img src="https://farm5.staticflickr.com/4005/5159332254_c856f04512_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4005/5159332254_c856f04512_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5159328666/in/album-72157625217278821"><img src="https://farm2.staticflickr.com/1079/5159328666_4856be0342_q.jpg" alt="" data-src-large="https://farm2.staticflickr.com/1079/5159328666_4856be0342_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/5159325428/in/album-72157625217278821"><img src="https://farm5.staticflickr.com/4108/5159325428_84516a4dc6_q.jpg" alt="" data-src-large="https://farm5.staticflickr.com/4108/5159325428_84516a4dc6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157625217278821" class="flickr-link" title="Bildergalerie zurück in die Heimat - GeWo III auf Flickr">Bildergalerie</a>
</figure>
<div class="section">
<p>Es war Sonntagmorgen und Zeit für Frühsport und ein Morgengebet im Freien. Nach dem Frühstück gab es einen zweiten Beitrag zum Thema &bdquo;Heimat&ldquo; und eine Gesprächsrunde, in der die Meinungen der Jungs zur Frage &bdquo;Warum ist Heimat so wichtig für uns?&ldquo;  ausgetauscht wurden.</p>
<p>Ein letztes Mal wurde eine Runde Fußball gespielt und anschließend  Mittag gegessen. Danach wurde es Zeit, die Heimat war erreicht. Zahlreiche Eltern begrüßten die Mannschaft, die sich nun trennte.</p>
<p>Nach dieser letzten aufregenden Reise schaut die Mannschaft schon voller Erwartung auf das nächste Gemeinschaftswochenende im Februar 2011.</p>
</div>
