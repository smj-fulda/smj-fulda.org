---
tags: [smjfulda, zeltlager]
migrated:
  node_id: 1005
  migrated_at: 2017-02-20 22:28
  alias: artikel/1005-überfallcodex
  user_id: 132
author: Steffen Büdel
title: Überfallcodex
created_at: 2015-07-22 01:48
excerpt: Hinweise zu „Überfälle“ im Zeltlager der SMJ Fulda
---
<p>Hinweise zu &bdquo;Überfälle&ldquo; im Zeltlager der SMJ Fulda</p>
<ol>
	<li>Überfälle sollen Spaß machen und dazu tragen alle ihren Teil bei. Es sollen keine Beeinträchtigungen im Lageralltag entstehen.</li>
	<li>Jeder Überfall muss beim Lagerleiter angemeldet werden (Tel.: 015128045824). Dies hat den Grund damit die Anzahl der Überfälle und die Zeitpunkte koordiniert werden können und es außerdem Tage gibt, an denen keine Überfälle möglich sind. Außer dem Lagerleiter ist im Lager niemand über bevorstehende Überfälle informiert.</li>
	<li>Überfälle sind grundsätzlich nur von 23.15 Uhr bis 04.00 Uhr möglich.</li>
	<li>Bei einem Überfall können verschiedene Dinge entwendet werden wie z. B. die Glocke oder die Fahne. Weiherhin soll dabei darauf geachtet, dass die entwendeten Gegenstände spätestens am nächsten Morgen wieder zum Eintauschen zum Zeltplatz gebracht werden.</li>
	<li>Generell ist bei den Überfällen auf ein gewisses Maß zu achten wie das Benutzen von Pyrotechnik oder der Lautstärke generell.</li>
	<li>Als gefangener Überfäller wird sich zum und in das Küchenzelt begeben und es wird nicht mehr in den laufenden Überfall eingegriffen.</li>
	<li>Es ist unerwünscht, dass Lagerteilnehmer nach vorzeitigem Verlassen des Lagers überfallen. Auch ist es nicht möglich den Platz vorab zu besichtigen und in einer späteren Nacht zu überfallen.</li>
</ol>
<p>Wir freuen uns auf spannende Überfälle (gerne auch für die Jungs).</p>
<p>Auf ein gutes Zeltlager 2015<br />
	Euer Steffen</p>
