---
tags: [smjfulda, glaubenskurs, mjffulda, kjf, omijugend, bonifatiuskloster, trinität, urkirche]
migrated:
  node_id: 952
  migrated_at: 2017-02-20 22:27
  alias: artikel/952-glaubenskurs-auch-dieses-mal-wieder-ein-gelungenes-wochenende
  user_id: 0
tagline: Im Kloster Hünfeld nahmen 21 Teilnehmer am gemeinsamen Glaubenskurs mit KJF, OMI-Jugend und MJF teil.
image:
  cover: https://farm8.staticflickr.com/7405/9547694359_a809ed5057_b.jpg
title: Glaubenskurs auch dieses Mal wieder ein gelungenes Wochenende
created_at: 2013-08-20 16:19
excerpt: 21 Jugendliche und junge Erwachsene kamen vergangenes Wochenende im Hünfelder Kloster zum Glaubenskurs zusammen.
---
<p>21 Jugendliche und junge Erwachsene kamen vergangenes Wochenende im Hünfelder Kloster zum Glaubenskurs zusammen.</p>
<p>Somit ging bereits der sechste Glaubenskurs an den Start, der gemeinsam von der Katholischen Jugend im Bistum Fulda (KJF), dem Jugendbüro der Hünfelder Oblaten (OMI), der Schönstatt Bewegung Mädchen und junge Frauen (MJF) und der Schönstatt-Mannesjugend (SMJ) organisiert und ausgerichtet wurde. Der Glaubenskurs richtet sich an Jugendliche und junge Erwachsene im Alter von 16 bis 30 Jahren, die tiefer in ihren Glauben eintauchen und ihn verstehen möchten. Dieses Mal standen die Themen Trinität und Urkirche im Mittelpunkt. Pater Dirk Fey regte als Referent für das Thema Dreieinigkeit am Samstagvormittag zum Nachdenken an. Er sprach von der Beziehung zwischen Vater, Sohn und dem hl. Geist, wie sie in der katholischen Kirche verstanden wird, und näherte sich dem komplexen Thema vom Alltag der Jugendlichen aus.</p>
<p>Am Nachmittag hieß es gemeinsam mit Pater Jens Watteroth und Florian Böth &quot;Back to the roots-Wie entstand die Kirche eigentlich?&quot; dem Ursprung der Kirche auf die Schliche zu kommen. Der Schwerpunkt lag bei beiden Themen darauf, den Alltagsbezug zum Leben der jungen Gläubigen herzustellen und förderte angeregte Diskussionen und Gespräche.</p>
<p>Gemeinsame Gebete, Gottesdienste, Lobpreis und Anbetung beim Abend der Versöhnung, Spiel und Spaß und ein Besuch im hauseigenen Niederseilgarten rundeten das Treffen ab und machten es zu einem gelungenen Wochenende. <figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157635141237205">
  <a href="https://www.flickr.com/photos/45962678@N06/9550496686/in/album-72157635141237205"><img src="https://farm3.staticflickr.com/2864/9550496686_4921bd95e8_q.jpg" alt="Glaubenskurs August 2013-4" data-src-large="https://farm3.staticflickr.com/2864/9550496686_4921bd95e8_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547707775/in/album-72157635141237205"><img src="https://farm6.staticflickr.com/5328/9547707775_55651e1596_q.jpg" alt="Glaubenskurs August 2013-5" data-src-large="https://farm6.staticflickr.com/5328/9547707775_55651e1596_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547707105/in/album-72157635141237205"><img src="https://farm6.staticflickr.com/5522/9547707105_397d52fcd1_q.jpg" alt="Glaubenskurs August 2013-6" data-src-large="https://farm6.staticflickr.com/5522/9547707105_397d52fcd1_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9550494394/in/album-72157635141237205"><img src="https://farm4.staticflickr.com/3776/9550494394_f32b7292b2_q.jpg" alt="Glaubenskurs August 2013-7" data-src-large="https://farm4.staticflickr.com/3776/9550494394_f32b7292b2_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547648335/in/album-72157635141237205"><img src="https://farm8.staticflickr.com/7448/9547648335_6c2a2f8141_q.jpg" alt="Glaubenskurs August 2013-8" data-src-large="https://farm8.staticflickr.com/7448/9547648335_6c2a2f8141_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547705519/in/album-72157635141237205"><img src="https://farm4.staticflickr.com/3702/9547705519_6b23fddd02_q.jpg" alt="Glaubenskurs August 2013-9" data-src-large="https://farm4.staticflickr.com/3702/9547705519_6b23fddd02_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547704687/in/album-72157635141237205"><img src="https://farm6.staticflickr.com/5442/9547704687_392b90ee77_q.jpg" alt="Glaubenskurs August 2013-10" data-src-large="https://farm6.staticflickr.com/5442/9547704687_392b90ee77_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9550491494/in/album-72157635141237205"><img src="https://farm4.staticflickr.com/3676/9550491494_c7f6926c98_q.jpg" alt="Glaubenskurs August 2013-11" data-src-large="https://farm4.staticflickr.com/3676/9550491494_c7f6926c98_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9550490336/in/album-72157635141237205"><img src="https://farm8.staticflickr.com/7366/9550490336_8244f05175_q.jpg" alt="Glaubenskurs August 2013-12" data-src-large="https://farm8.staticflickr.com/7366/9550490336_8244f05175_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547700867/in/album-72157635141237205"><img src="https://farm8.staticflickr.com/7404/9547700867_84ae8a87d9_q.jpg" alt="Glaubenskurs August 2013-13" data-src-large="https://farm8.staticflickr.com/7404/9547700867_84ae8a87d9_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547700011/in/album-72157635141237205"><img src="https://farm3.staticflickr.com/2836/9547700011_560f917460_q.jpg" alt="Glaubenskurs August 2013-14" data-src-large="https://farm3.staticflickr.com/2836/9547700011_560f917460_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9550487188/in/album-72157635141237205"><img src="https://farm4.staticflickr.com/3807/9550487188_81d4e30dc9_q.jpg" alt="Glaubenskurs August 2013-15" data-src-large="https://farm4.staticflickr.com/3807/9550487188_81d4e30dc9_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9547647223/in/album-72157635141237205"><img src="https://farm8.staticflickr.com/7454/9547647223_5f2f3a2b76_q.jpg" alt="Glaubenskurs August 2013-16" data-src-large="https://farm8.staticflickr.com/7454/9547647223_5f2f3a2b76_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/9550486416/in/album-72157635141237205"><img src="https://farm8.staticflickr.com/7375/9550486416_e323a0465e_q.jpg" alt="Glaubenskurs August 2013-17" data-src-large="https://farm8.staticflickr.com/7375/9550486416_e323a0465e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157635141237205" class="flickr-link" title="Bildergalerie Glaubenskurs August 2013 auf Flickr">Bildergalerie</a>
</figure>
