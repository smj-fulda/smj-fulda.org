---
title: Bei bestem Wetter im Zauberwald
tagline: Bericht vom Zeltlager 2022
author: Benedikt Waibel
tags: [zeltlager]
image:
    cover: https://live.staticflickr.com/65535/52543049709_3b86119f7a_o_d.jpg
    align: top
---

Das Zeltlager in diesem Sommer ist bereits eine Weile her. Mit diesem Bericht möchten wir unsere Erlebnisse festhalten und Erinnerungen wecken. Es war eine Woche mit vielen Spielen, Ausflügen, Zeit zum Ausruhen und mit hervorragendem Wetter. Wir haben gemeinsam den Zauberwald erkundet und den Waldbewohnern geholfen.

<figure data-href="https://www.flickr.com/photos/straight-shoota/52543237800/in/album-72177720304217883/">
  <img src="https://live.staticflickr.com/65535/52543237800_9e85a8622c_w_d.jpg">
</figure>
<figure data-href="https://www.flickr.com/photos/straight-shoota/52542755321/in/album-72177720304217883/">
  <img src="https://live.staticflickr.com/65535/52542755321_28a64ce83b_w_d.jpg">
</figure>
<figure data-href="https://www.flickr.com/photos/straight-shoota/52543300743/in/album-72177720304217883/">
  <img src="https://live.staticflickr.com/65535/52543300743_d194c6d106_w_d.jpg">
</figure>
<figure data-href="https://www.flickr.com/photos/straight-shoota/52542294307/in/album-72177720304217883/">
  <img src="https://live.staticflickr.com/65535/52542294307_1f527d8c4d_w_d.jpg">
</figure>
<figure data-href="https://www.flickr.com/photos/straight-shoota/52542754671/in/album-72177720304217883/">
  <img src="https://live.staticflickr.com/65535/52542754671_af5ce48dec_w_d.jpg">
</figure>

Ursprünglich war das Zeltlager in diesem Jahr für einen Waldzeltplatz in Heigenbrücken und eine Dauer von 11 Tagen geplant. Aufgrund kleinerer Teilnehmerzahlen und einer geringen Besetzung an Gruppenleitern haben wir stattdessen für eine Woche erneut auf den Wiesen um das Heiligtum herum gezeltet. Wir haben uns von der kleineren Größe der Lagergemeinschaft aber nicht beeindrucken lassen und dennoch einen großen Fahnenmast aufgestellt, laut den Schlachtruf gebrüllt und viele spannende Aktionen umgesetzt.

Die Jungs sind am Sonntag auf dem Zeltplatz eingetroffen. Zuerst haben wir die Zelte bezogen und einige Kennenlernspiele gespielt. Nach dem Mittagessen haben wir im Anschluss die Lagerbauten gebaut. Das waren in diesem Jahr der Fahnenmast, ein schwarzes Brett und eine Halterung für die Glocke. Am ersten vollen Tag haben wir eine Expedition rund um den Ort zur Haunequelle unternommen. Als wir am Nachmittag wieder angekommen sind, haben wir eine Runde Werwolf begonnen. In der ersten Nacht des Spiels wurde das Dorf allerdings von Wasserbomben geweckt. In diesem Jahr hatten wir besonderes Glück mit dem Wetter. Nahezu die ganze Zeit hatten wir gutes Wetter und hohe Temperaturen. Deshalb haben wir uns viele Aktionen zur Abkühlung überlegt. Den Tag haben wir mit einer Nachtwanderung abgeschlossen. Unser Expeditionsleiter war nicht vom Wald zurückgekehrt, sodass wir ihm gemeinsam gefolgt sind. Nachdem wir ihn wiedergefunden hatten, haben wir unser Abendgebet im Waldheiligtum gehalten und den Rückweg angetreten. 

Am Dienstagmorgen wurden wir mit verwirrenden Ansagen geweckt. Statt dem üblichen Tagesablauf gab es nach dem Aufstehen als erstes Gegrilltes zum Mittagessen. Unser Expeditionsleiter war sehr verwirrt und hat in einer willkürlichen Reihenfolge zu Aktionen geklingelt. Am Nachmittag hat uns unser Wahrsager ausgeholfen. Der Expeditionsleiter sei durch einen magischen Stein, den er am vorigen Abend vom Wald mitgenommen hatte, verflucht. Die Jungs haben deshalb zunächst in verschiedenen Übungen Kraft und Geschick gezeigt. Mit dieser Übung haben wir am Abend dem Expeditionsleiter den Stein abgenommen und ihn an die Waldbewohner zurückgegeben. Unsere neuen Bekannten sind am Mittwochmorgen erneut zum Zeltplatz gekommen und haben den Jungs weitere Herausforderungen gestellt. Am Nachmittag haben wir das gute Wetter für eine Wasserbombenschlacht verwendet. Diese Aktion war im letzten Jahr schon unter dem Namen „Eroberung von Troja“ vorbereitet gewesen und wegen schlechtem Wetter ausgefallen ([siehe](http://smj-fulda.org/artikel/2021-08-25-bericht-vom-zeltlager-2021/)). Dieses Jahr war die Abkühlung sehr willkommen. 

Am Donnerstag haben wir uns am Morgen gemeinsam über Freundschaft und das Liebesbündnis unterhalten. Das Liebesbündnis ist für uns wie eine Freundschaft mit Maria. Wir setzen uns für Maria ein und vertrauen ihr und können gleichzeitig immer Hilfe bei ihr suchen. Als Nachmittagsprogramm sind wir zum Guckaisee gefahren und haben uns in diesem Badesee abgekühlt. Freitag war in diesem Jahr bereits der letzte volle Tag des Zeltlagers. Wir sind auf die Wasserkuppe gefahren und sind dort in den Klettergarten gegangen. Da darauf leider ein Gewitter begonnen hat, haben wir spontan umgeplant und sind erstmal ins Segelflugmuseum gegangen. Als der Regen nachgelassen hat, haben wir noch einige Fahrten in Attraktionen wie dem Rhönbob machen können, bevor wir uns auf den Rückweg gemacht haben. Auch in diesem Jahr haben wir einen Weihegottesdienst gefeiert, der in diesem Jahr in toller Weise von Jugendpater Andre Kulla gehalten wurde. Zwei der Jungs haben neu die Ritterweihe geschlossen, der Rest von uns hat sie gemeinsam erneuert. Die Weihe haben wir im Anschluss mit einem Festessen gemeinsam gefeiert. Dabei haben wir uns vom starken Regen kaum beeindrucken lassen und einfach gemeinsam im Küchenzelt gegessen. Nach dem Regen haben wir es am Abend trotzdem geschafft, ein Lagerfeuer anzuzünden. Am folgenden Tag haben wir die Zelte nur noch ausgeräumt und unsere Sachen gepackt. Der Samstag war nämlich der letzte Tag unseres Zeltlagers. Vor dem Mittagessen sind die Eltern zur Abholung gekommen, wir haben gemeinsam eine Andacht gehalten und im Lagerzirkus von unseren Erlebnissen berichtet.

Vielen Dank an alle, die am Zeltlager in diesem Jahr teilgenommen haben oder es unterstützt haben! Danke an diejenigen, die uns netterweise Kuchen gebacken haben, an die diesjährigen Überfäller, und dass wir wieder auf den Wiesen ums Heiligtum zelten durften. 

Das Zeltlager 2023 findet vom 24.07 - 30.07 statt. Wir freuen uns auf euch!

<figure class="full-width" data-href="https://www.flickr.com/photos/straight-shoota/52542754646/in/album-72177720304217883/">
  <img src="https://live.staticflickr.com/65535/52542754646_84b3433cea_k_d.jpg">
</figure>
