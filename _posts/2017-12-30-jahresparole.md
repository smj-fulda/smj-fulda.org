---
title: "Jahreskonferenz der SMJ-Deutschland: Haltung zeigen - Gott wirkt durch dich"
tags: [jako, smj, smjdeutschland, schönstatt]
author: Johannes Müller
image:
  cover: "https://farm5.staticflickr.com/4594/24126734517_35390cab62_o_d.jpg"
---

Jedes Jahr treffen sich zwischen Weihnachten und Silvester die Delegierten der einzelnen Diözesen zur Jahreskonferenz der SMJ-Deutschland. Dort wurde auch die Jahresparole der für das Jahr 2018 festgelegt:

*Haltung zeigen - Gott wirkt durch dich*

Es gibt auch wieder ein Lied zur Jahresparole:

<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/376995164&amp;color=ff5500"></iframe>
