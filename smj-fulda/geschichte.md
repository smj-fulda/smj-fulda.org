---
layout: page
title: Geschichte der SMJ-Fulda
permalink: /geschichte/
section: smj-fulda
cover_image: gründungsurkunde_cover.jpg
---

Die ersten Jungmänner im Bistum Fulda sammelten sich um Stadtpfarrer Hermann Schmidt (1895–1962), der von 1945 bis 1959 an der Stadtpfarrkirche St. Blasius in Fulda tätig war. Unter ihnen befanden sich u.a. der heutige Pfarrer Franz-Peter Breidbach, Dieter Bohl und Ludolph Kettler (beide Familienliga), ebenso die Brüder Edwin (+) und Gerold Langsch.
Gerold wurde Schönstattpater und ist in USA tätig.

Es bestand nur die eine Gruppe, die sich – wohl angeregt durch die Arbeit der Mädchenjugend - in den 50er Jahren (1953, 1954, 1955) regelmäßig getroffen haben.
Traditionell gab es durch die Arbeit der Marienschwestern mehr Mädchen- als Jungen-Gruppen.

1964 kam für die Mädchen Schwester M. Hiltraude als Jugendschwester in die Diözese Fulda. Angeregt durch Familien, die gerne auch ihre Jungen in die Schönstattjugendarbeit eingebunden hätten, begann Schwester M. Hiltraude 1965 mit einer Gruppe von Jungen auf dem Florenberg.

Mit dieser Gruppe begann ein neuer Abschnitt der Schönstatt-Mannesjugend im Bistum.
Als Gruppensymbol wählte jeder Junge einen mit dem MTA-Bild geschmückten Stein aus Marmor für die Hosentasche. Die Gottesmutter Maria sollte in diesem Zeichen immer mit gehen. So begleitete es z. B. einen der Jungen später auf seiner Afrikareise nach Kenia.

# Gründungsurkunde
<figure>
    <img src="https://farm5.static.flickr.com/4064/4225903967_6e216400a8_z.jpg" />
    <figcaption>Pater Josef Kentenich unterzeichnet die Gründungsurkunde der SMJ Fulda</figcaption>
</figure>
<figure>
    <img src="https://farm3.static.flickr.com/2719/4226672078_133a53c660_z.jpg" />
    <figcaption>Pater Josef Kentenich mit Jungmännern im Kapellchen</figcaption>
</figure>

Zum Besuch Pater Josef Kentenichs im Januar 1967 in Dietershausen verfasste die Gruppe eine Urkunde, die jedes Mitglied unterschrieb. Die Jungen stellten sich am 22. Januar 1967 als neue Gründergeneration im Heiligtum dem Gründer vor. Pater Kentenich unterschrieb auch gerne diese Urkunde, die als Gründungs-Urkunde der Schönstatt-Mannesjugend im Bistum Fulda gilt.
Verwahrt wird sie bis heute vom jeweils amtierenden Diözesanführer der Mannesjugend.

1968 nahmen die ersten Jungmänner an einem Zeltlager am Canisiushof in Eichstätt teil, 1970 am Zeltlager in Schönstatt.

# 70er Jahre
1970 gab es drei Jungmänner-Gruppen im Bistum: Homburg/Efze, Dietershausen und Fulda St. Elisabeth (hier war immer eine Jungmänner-Hochburg). In diesem Jahr stieß auch Rudolf Liebig aus Horas zu den Jungmännern. Die Gruppe Engelhelms existierte zu diesem Zeitpunkt nicht mehr. Seit Beginn der 70er Jahre wurden die Jungmänner durch priesterliche Mitarbeiter begeleitet: Pater Abt, Pfarrer Breidbach, Pfarrer Brähler, Pfarrer Eufinger.

## Bildstock als Neuanfang
Es ging in der nächsten Zeit mit den Jungmännern jedoch in einer Weise bergab, dass 1973 nur noch sieben Jungen übrig waren, die aus den drei genannten Gruppen stammten: Stefan Völler, Rudolf Liebig, Peter Pappert, Bernd Plappert aus Dietershausen, Bernhard Severin, Gerhard Schuster, Peter Werner aus Homberg. Diese errichteten im Jahre 1973 im Wald nahe Dassen einen Bildstock in der Nähe von drei Birken. Dort hatte 1967/68 schon einmal ein Bildstock gestanden, der jedoch zerstört worden war. Die drei Birken, von denen zwei zusammengewachsen waren, symbolisierten die drei Gruppen, die beiden zusammengehörigen Gruppen Dietershausen und Fulda und die Gruppe Homberg.

Natürlich wurden die drei Bäume auch mit der Allerheiligsten Dreifaltigkeit und der Dreimal Wunderbaren Mutter in Verbindung gebracht. Der Bildstock war in der typischen Rautenform errichtet, wie er in der Folgezeit in jedem Zeltlager gebaut wurde. Die Pflege des Bildstocks übernahm der Gruppenführer von Dietershausen, Bernd Plappert.

Die Jungen wollten wieder Gründergeneration sein, deshalb genügte ihnen die äußere Errichtung des Bildstocks nicht. Im April 1973 fuhren sie zusammen mit Jungen aus anderen Bistümern nach Cambrai; mittlerweile arbeitete von der Zentrale in Schönstatt Pater Nöthen bei den Jungmännern in Fulda mit.

## Gruppenarbeit
Im Jahr 1974 wurde erstmals ein Schönstatt-Priester aus der Diözese, Pfarrer Karl Eufinger, Standesleiter der Mannesjugend. Die Gruppenarbeit wurde intensiviert. Rudolf Liebig leitete beispielsweise die drei Gruppen Fulda-Horas, Fulda-St. Elisabeth und in Bachrain. In jene Zeit fällt die Mitarbeit von Rainer Weber, Ansgar Oswald, Harald Zentgraf, Wolfgang Paul und Christoph Vogel (Petersberg). In Petersberg existierte eine Gruppe, die sich von der Fuldaer Gruppe abgespalten hatte; es gab außerdem Gruppen in Eichenzell (Roland Dehler, Ulrich Schäfer), in Bad Hersfeld (Stefan Buß) und in Oberndorf (Klaus Desch, heute als Schönstatt-Pater in Nigeria tätig). Damals gab es insgesamt sieben bis acht Gruppen. Seit 1970 fanden regelmäßig eigene Zeltlager der Fuldaer Mannesjugend statt. Nur in einem Jahr fiel seitdem das Zeltlager aus.

## MTA-Bild

<figure>
    <img src="https://farm3.static.flickr.com/2539/4226672214_109c4e9eb6_z.jpg" />
    <figcaption>Gründungsurkunde und Ikone der SMJ-Fulda</figcaption>
</figure>

Pfarrer Eufinger brachte den Jungen ein MTA-Bild mit, das die Mannesjugend von nun an als Diözesanbild begleitete. Die Jungen eropferten Quarz-Steine für einen Rahmen. Die Krönung des Bildes fand auf dem Zeltlager im Jammertal (Bad Ems) statt. Nur durch geistliches Streben hat sich in jener Zeit etwas in der Mannesjugend bewegt.

Im Jahre 1975 fanden erstmals zwei Zeltlager statt (eines mit 70-75 Jungen, und eines mit ca. 30 Jungen). Immer wieder stellten sich die Jungen dem Anspruch, es müsse auch geistlich etwas geschehen (GTO, PE, Beichte). Zwischenzeitlich wurde sogar eine eigene Abteilung Homberg (mit Bad Hersfeld) gegründet, die dann jedoch schnell wieder einging. 1975 bis 1981 gab es ca. 120-140 Jungmänner. Rudolf Liebig leitete den KdV (Kreis des Vertrauens).

# Das neue Jahrtausend

Der Anstoß zur Erneuerung des im Jahre 1999 völlig zerstörten Bildstocks kam von einigen Mitgliedern des Josefs-Kreises (vor allem von Rene Kohl, Benjamin und David Brähler). Kaplan Vonderau, der geistliche Mitarbeiter in diesem Kreis, besorgte im Provinz-Haus der Schönstatt-Schwestern in Dietershausen im Frühjahr des Jahres 2000 ein MTA-Bild, das während der Weltjugendtage in Rom u.a. den Segen Papst Johannes Pauls II. erhielt und die Gruppe auf allen Stationen begleitete.

Auf der Kreistagung vom 25. bis 27. August 2000 wurde am Samstag, dem 26. August, der Bildstock im Wald vom Josefs-Kreis wieder errichtet. Nach der Abendmesse gingen die Jungmänner mit Kaplan Vonderau zum Bildstock um seine Segnung vorzunehmen. Am folgenden Tag entstand auch das Kreisgebet.

Seit 2004 trafen sich die älteren und ehemaligen SMJler im *Alte-Hasen-Arbeitskreis*. Daraus entstanden später monatliche Stammtische und der Senat.

Im Jahr 2005 fand anlässlich des Weltjugendtages in Köln ein Jugendfestival in Schönstatt statt, an dem sich Jugendliche aus aller Welt trafen. Dort boten die verschiedenen Regios typische Spezialitäten aus ihrer Heimat an. Die SMJ-Fulda verkaufte Zwibbelsploatz aus der Rhön. Im Verbund mit Mainz und Speyer wurde das *Kulturcafe Mitte* betrieben.

Aus dem internationalen Jugendfestival entstand die *Nacht des Heiligtums*, die jedes Jahr viele Jugendliche begeistert. Aus Fulda wird die Fahrt zu diesem Treffpunkt der deutschen Schönstattjugend zusammen mit der Mädchenjugend organisiert und finanziell von der Familienbewegung unterstützt. Die SMJ-Fulda betrieb im Rahmen der Nachtkultur das Bonifatiuszelt, für das Ellie Büdel aus Jossgrund-Pfaffenhausen zwölf Sitzsäcke genäht hat.

Zum 50-jährigen Jubiläum des Schönstattkapellchens 2007 gestalteten SMJ und MJF eine Vigilfeier mit Feuerwerk und Nachtanbetung.

Aus einer privaten Initiative entstand die SMJ-Kreuzbergwanderung, die sowohl in einer Sommer- als auch einer Wintervariante am 23. Dezember stattfindet - in manchen Jahren auch mehrfach.

In den Jahren 2009 und 2010 feierten SMJ und MJF gemeinsam Silvester im Schatten des Heiligtums in Dietershausen mit vielen Freunden und Bekannten.

Aus einer Idee von Pfarrer Schäfer vom Zeltlager 2002 entwickelte Sebastian *Bonkos* Hartmann das JESUS-Shirt. Zunächst als persönliches Unikat angefertigt, wird es seit 2009 online im SMJ-Shop verkauft. Bald kamen weitere Produkte hinzu, neben Bekleidung mit JESUS-Aufdruck auch MTA-Geldkartenhüllen, JESUS-Kollar, -Handtücher, -Brettchen und Frühstücksbretter. Der Erlös kommt der SMJ-Arbeit zugute.

JESUS-Shirt-Träger versammelten sich auch zu Fanaktionen, so die jährliche Bonifatiuswallfahrt durch die Fuldaaue zum Dom anlässlich des Bonifatiusfestes und ein JESUS-Flashmob im ZDF-Fernsehgottesdienst im Kloster Hünfeld.

Seit 2012 bietet Sebastian Hartmann einen SMS-Adventskalender an, der auch außerhalb der SMJ großes Interesse erfährt.

Der 1973 errichtete Bildstock wurde 2013 auf den Prüfstand gestellt. Um diesen schönen Fleck nicht in Vergessenheit geraten zulassen, versammelten sich im Rahmen des *72-Stunden-Projekts* Messdiener und Jugendliche aus Dietershausen um Simon Wawra und erneuerten ihn rundum. Durch eine Beschilderung aus verschiedenen Richtungen ist es zum Ziel für manche Wanderer geworden. Hinzu zum ursprünglichen Bildstock kamen auch ein Stein mit dem Bild von Josef Engling und ein vom Kreis Ullrich aufgestelltes Kreuz. 

<!-- TODO:
Fest des Glaubens

Schönstatt Jubiläum 2014

SMJ Jubiläum 2017
-->

# Geschlossene Kreise

Diese kleine Auflistung zeigt Kreise, die zwar nicht mehr aktiv, dennoch aber mit uns verbunden sind. Sie waren jahrelang Stütze für unsere Arbeit.


## Kreis Thomas

Kreis Thomas trifft sich heute nicht mehr aktiv. Seine Mitlgieder übernehmen aber noch verschiedene Aufgaben innerhalb der SMJ Fulda.

* Tobias Büdel
* Ullrich Schott
* Klaus Schmidt
* Johannes Müller
* Kreisleiter ist Thomas Stey.

## Die Pioniere

<!-- img|kreise/pioniere/Bildstock007.jpg|Die Pioniere mit dem neu errichteten Bildstock -->
Der Kreis der Pioniere setzte sich zusammen aus einer Ansammlung verschiedener Charaktere, die über das Heiligtum miteinander verbunden waren. Sie stammten aus den Jahrgängen 1988 und 1989. Der Kreis bestand seit 2002, als er mit 16 Mitgliedern unter Kreisführer René Kohl und geistlichem Leiter Pfarrer Dr. Dagobert Vonderau im Heiligtum in Dietershausen gegründet wurde.

Die Pioniere hatten es sich zum Ziel gesetzt, Wegbereiter und Brückenbauer für alle zu sein, die den Weg zu Gott beschreiten wollen. In intensiver Arbeit mit Themen sowohl aus dem schönstätterischen als auch dem alltäglichen Bereich hatten sie sich eine Grundlage geschaffen, um dieser Aufgabe gerecht zu werden.

Während den Kreistagungen wurden jedoch auch andere Arbeiten geleistet. So wurde der SMJ-Bildstock, der Jahre zuvor vom Josefskreis hergerichtet worden war mit einem Josef-Engling-Stein erweitert und erneut renoviert.

<figure>
    <img src="https://farm3.static.flickr.com/2801/4225903815_b13ee4398f_z.jpg" />
    <figcaption>Zuflucht der Sünder - Das MTA Bild der Schönstattbewegung </figcaption>
</figure>
