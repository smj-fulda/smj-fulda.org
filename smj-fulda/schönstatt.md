---
layout: page
title: Schönstatt
permalink: /schönstatt/
section: smj-fulda
---

<!-- TODO: Schönstatt Logo -->

Die Schönstatt-Bewegung ist eine internationale geistliche Erneuerungsbewegung in der katholischen Kirche. Ursprungsort wie geistlicher Mittelpunkt ist das Urheiligtum in Schönstatt, einem Ortsteil von Vallendar am Rhein (bei Koblenz).

Das Ziel des Gründers Josef Kentenich war es, der Kirche und dem Glauben der Christen ein neues, modernes Gesicht zu geben. Schönstatt kann Vision Kentenichs verstanden werden, wie die Kirche in Zukunft aussehen könnte, wenn sie nicht verstauben und weiterhin Menschen zu Jesus führen will.

Die Schönstatt-Bewegung ist heute in über 60 Ländern der Erde verbreitet, neben Deutschland sind vor allem die südamerikanischen Staaten von großer Bedeutung. Weltweit gibt es über 180 Schönstatt-Zentren mit Schönstattkapellchen, einer originalgetreuen Nachbildung des Urheiligtums in Schönstatt.

## Schönstatt
Schönstatt selbst ist ein Stadtteil der Stadt Vallendar bei Koblenz und ein katholischer Wallfahrtsort.
Im Umfeld der dortigen Pallottiner-Niederlassung entstand 1914 die Schönstatt-Bewegung, deren geistiger und symbolischer Mittelpunkt die ehemalige Michaelskapelle als so genanntes Urheiligtum wurde. Inzwischen sind in Schönstatt als spirituellem Zentrum und Mittelpunkt der internationalen Schönstattbewegung zahlreiche religiöse Bildungshäuser entstanden und Menschen aus aller Welt kommen zu religiösen Veranstaltungen.

## Der Gründer
Josef Kentenich SAC (* 16. November 1885 in Gymnich bei Köln; † 15. September 1968 in Schönstatt) war Pater in der Gesellschaft der Pallottiner, einer Ordensgemeinschaft päpstlichen Rechts, und ist Gründer der internationalen Schönstattbewegung.
Am 19. April 1914 gründete er in Schönstatt bei Vallendar zusammen mit einigen der ihm anvertrauten Studenten eine Marianische Kongregation, aus der das heutige Schönstattwerk hervorging. Der 18. Oktober 1914 gilt als Gründungstag, an dem er in der Michaelskapelle einen Vortrag vor seinen Studenten hielt.

Einen Einschnitt für die Schönstattfamilie des Bistums Fulda bedeutete der Besuch von Pater Josef Kentenich in Dietershausen. Hier unterzeichnete er am 22. Januar 1967 eine Urkunde, die mit Recht als Gründungs-Urkunde der Schönstatt-Mannesjugend im Bistum Fulda bezeichnet werden kann. Verwahrt wird sie bis heute vom jeweils amtierenden Diözesanführer der Mannesjugend.
