---
layout: page
title: Die SMJ-Fulda
section: smj-fulda
permalink: /die-smj-fulda/
cover_image: gruppenbild-kt2016.jpg
section: smj-fulda
description: >
  Die Schönstatt-Mannesjugend ist ein katholischer Jugendverband und Teil der internationalen Schönstatt-Bewegung im Bistum Fulda. Wichtige Elemente unserer Jugendarbeit sind die Übermittlung christlicher Werte und die Erziehung zu freien Persönlichkeiten mit einer Festigkeit im Glauben.
---

Die Schönstatt-Mannesjugend ist ein katholischer Jugendverband und Teil der internationalen [Schönstattbewegung](http://www.schoenstatt.de/) im [Bistum Fulda](http://www.bistum-fulda.de/). Wichtige Elemente unserer Jugendarbeit sind die Vermittlung christlicher Werte und die Erziehung zu freien Persönlichkeiten mit einer Festigkeit im Glauben.

Unsere Jugendarbeit richtet sich an männliche Kinder und Jugendliche, die Schwesterorganisation *Schönstattbewegung Mädchen/Junge Frauen* an weibliche. Diese Trennung ist in der auf dem Schönstatt-Gründer Pater Josef Kentenich beruhenden Kentenich-Pädagogik begründet, die die getrennte, geschlechtsspezifische Erziehung von Jungen und Mädchen bevorzugt.

Als Grundpfeiler der SMJ gelten die [Fünf Säulen der Mannesjugend](/säulen): Gemeinschaft, Lebensschule, Apostelsein, Liebesbündnis und Mannsein.

<!-- TODO: Organisationsform?-->

# Diözesanführung

Diözesanführer der Schönstatt-Mannesjugend im Bistum Fulda ist seit Oktober 2020 Adam Muthig. Er koordiniert und organisiert die Jugendarbeit und ist Ansprechpartner für die SMJ-Fulda. Er wird in seinen Aufgaben vom Stellvertreter Markus Breitenbach unterstützt.

<div class="kontakt-info kontakt-info--oneline">
  <div class="contact">
    <img class="contact__image" src="{{ site.baseurl }}/images/profile/adam-muthig.jpg" />
    <div class="contact__name">
      Adam Muthig
    </div>
    <div class="contact__mail">
      <a href="mailto:adam.muthig@smj-fulda.org">adam.muthig@smj-fulda.org</a>
    </div>
    <strong class="contact__office">Diözesanführer</strong>
  </div>
  <div class="contact">
    <img class="contact__image" src="{{ site.baseurl }}/images/profile/markus-breitenbach.jpg" />
    <div class="contact__name">
     Markus Breitenbach
    </div>
    <div class="contact__mail">
      <a href="mailto:markus.breitenbach@smj-fulda.org">markus.breitenbach@smj-fulda.org</a>
    </div>
    <strong class="contact__office">Stellvertretender Diözesanführer</strong>
  </div>
</div>

# Lagerleitung

Für unsere jährliche Kernveranstalung, das Zeltlager, ist aktuell Lennard Wolf verantwortlich. Zu den Aufgaben zählen, neben den vielen nötigen
Vorbereitungen, auch das morgendliche Wecken der Jungs im Lager.

<div class="contact">
  <img class="contact__image" src="{{ site.baseurl }}/images/profile/lennard-wolf.jpg" />
  <div class="contact__name">
    Lennard Wolf
  </div>
  <div class="contact__mail">
    <a href="mailto:lennard.wolf@smj-fulda.org">lennard.wolf@smj-fulda.org</a>
  </div>
  <strong class="contact__office">Lagerleiter</strong>
</div>

# Standesleitung

Unser Standesleiter ist Pfarrer Rudolf Liebig, Pfarrer in St. Antonius Künzell und Mitglied im Schönstatt-Priesterbund. Er ist für die geistliche Begleitung unserer Gemeinschaft zuständig.

<div class="contact">
  <span class="contact__image"></span>
  <div class="contact__name">
    Pfarrer Rudolf Liebig
  </div>
  <div class="contact__mail">
    <a href="mailto:rudolf.liebig@smj-fulda.org">rudolf.liebig@smj-fulda.org</a>
  </div>
  <strong class="contact__office">Standesleiter</strong>
</div>

Sein vorgänger, der langjährige Standesleiter Pfarrer Ulrich Schäfer ist [im Juli 2018 unerwartet verstorben]({{ site.baseurl }}{% link _posts/2018-07-19-trauer-um-pfarrer-schaefer.md %}).

Pfarrer Dr. Dagobert Vonderau (Pfarrer in St. Michael Neuhof) ist ebenso als geistlicher Mitarbeiter tätig. Daneben werden wir von weiteren Priestern unterstützt, die unsere Kreise und Veranstaltungen geistlich begleiten und mit uns Gottesdienste feiern.

# Finanzen

Als Kassenwart verwaltet René-André Kohl aus Fulda-Edelzell die Kasse der SMJ-Fulda. Er ist verantwortlich für alle Finanzfragen und gewährleistet somit den finanziellen Rahmen für eine erfolgreiche Jugendarbeit.

<div class="contact">
  <span class="contact__image"></span>
  <div class="contact__name">
    René Kohl
  </div>
  <div class="contact__mail">
    <a href="mailto:rene.kohl@smj-fulda.org">rene.kohl@smj-fulda.org</a>
  </div>
  <strong class="contact__office">Kassenwart</strong>
</div>

# Gruppenführer

In der SMJ-Fulda engagieren sich rund 30 Jugendliche ab 16 Jahren als ehrenamtliche Gruppenleiter. Sie bilden das Fundament der SMJ-Arbeit, planen und betreuen als Gruppenleiter die Veranstaltungen und übernehmen Verantwortung für die ihnen anvertrauten Jugendlichen.

Ein geistliches Inputteam, bestehend aus Johannes Wende, Steffen Büdel und Pfr. Liebig kümmert sich um die geistlichen Inhalte unserer Veranstaltungen Ü14.

Ein weiteres Team bestehend aus Jonas Wolf, Adam Muthig, Jonathan Flohr und Fabian Buhl kümmert sich um ein Rund-um-Paket d.h. Sie gestalten das Programm für Treffen der Gruppenleiter.

# Kooperationen

Wir gehören zur *Regio Mitte* der [SMJ in Deutschland](https://www.smj-deutschland.de/) gemeinsam mit der SMJ der Bistümer [Mainz/Limburg](https://www.smj-mainz-limburg.de/) und [Speyer](http://www.smj-speyer.de/).
Die Zusammenarbeit auf Regio-Ebene besteht aus gegenseitigem Austausch und gemeinsamen Regiofahrten und Zeltlagern.

Eng verbunden sind wir mit der *Schönstattbewegung Mädchen und Junge Frauen im Bistum Fulda* (SchönstattMJF Fulda), das weibliche Pendant zur SMJ. Die [Schönstattbewegung und das Schönstattzentrum Dietershausen](https://www.schoenstatt-fulda.de/schoenstatt/) sind unsere gemeinsame Heimat.

Auf Bistumsebene arbeiten wir auch mit der [Katholischen Jugend im Bistum Fulda](http://kjf-fulda.de) und der [OMI-Jugend](http://omi-jugend.de) zusammen. Als gemeinsame Projekte habe wir viele Jahre das [Fest des Glaubens](/aktionen#fest-des-glaubens) und [Glaubenskurs-Wochenenden](/aktionen#glaubenskurs) organisiert.
