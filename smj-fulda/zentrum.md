---
layout: page
title: Unser Zentrum
permalink: /zentrum/
section: smj-fulda
---

<!-- TODO: Bild Kapellchen-->

Unser Schönstattzentrum liegt mitten im Naturpark Hessische Rhön zwischen Wasserkuppe, Milseburg und der Barockstadt Fulda auf der Marienhöhe im Erholungsort Dietershausen.

Neben dem Zentrum der Schönstattbewegung im Bistum Fulda hatten die Schönstätter Marienschwestern bis 2015 auch ihr Provinzhaus. Dieses wurde im Januar 2016 an die Caritas Fulda verkauft und dient seitdem als Heimat für minderjährige Flüchtlinge.

Das Josef-Engling-Haus eignet sich für Tagungen, Ferienaufenthalte oder Exerzitien, die Schönstätter Marienschwestern laden immer wieder herzlich ein. Hier finden auch die meisten Treffen der SMJ statt.

<!-- TODO: Website Kapellchen-->

# Unser Kapellchen
Unser Schönstattkapellchen, das *Liebesheiligtum*, ist eine Nachbildung des Urheiligtums in Schönstatt, in dem Pater Kentenich die Schönstattbewegung gründete. Weltweit gibt es 185 dieser Kapellchen in nationalen und internationalen Zentren der Schönstattbewegung, wovon 56 in Deutschland stehen.

Als Altarbild ist in allen Kapellchen die Darstellung von Maria und Jesuskind, ein Nachdruck des Gemäldes Zuflucht der Sünder des italienischen Malers Crosio, die in Anlehnung an die Bewegung in Ingolstadt MTA-Bild (Mater Ter Admirabilis, lat. Dreimal wunderbare Mutter) genannt wird. Um das Gnadenbild ist die Inschrift *servus mariae nunquam peribit* (lat. *Ein Diener Mariens wird niemals zu Grunde gehen*) angebracht.

Unser direkt beim Zentrum gelegenes Schönstattheiligtum lädt stets zu Gebet und Stille ein.

An jedem 18. eines Monats findet die sogenannte Bündnismesse statt, zu der die gesamte Schönstattfamilie eingeladen ist. Diese Andachten werden zum Gedächtnis der Gründung Schönstatts begangen.  Weitere Termine und Gottesdienst-Zeiten finden sich auf der Website des [Zentrums](http://www.schoenstatt-fulda.de/schoenstatt/termine/).


# Josef-Engling-Haus

<!-- TODO: Bilder vom Haus, Adresse und Link zur Website-->

Das Josef-Engling-Haus mit seinem gemütlichen und einladenden Charakter steht der Schönstattbewegung sowie externen Gästen zur religiösen Bildung und zu Urlaubstagen offen. Infos und Kontakt gibt es auf der [Website des Hauses](http://www.schoenstatt-fulda.de).

<figure>
    <img src="https://farm2.staticflickr.com/1599/25849881372_bf10c268ba_z_d.jpg" />
</figure>
