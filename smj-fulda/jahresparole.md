---
title: Jahresparole
layout: page
permalink: jahresparole/
cover_image: säulen-licht.jpg
section: smj-fulda
---

Die Jahresparole wird jedes Jahr auf der Jahreskonferenz der Schönstatt-Mannesjugend (*Jako*) gefunden und soll die Arbeit in der SMJ mit einem besonderen Impuls versehen:

* 2021: Zukunft schmieden – in der Glut des Gründers
* 2020: Zukunft schmieden – in der Glut des Gründers
* 2019: Zeit für dich – begeistert Leben gestalten
* 2018: Haltung zeigen - Gott wirkt durch dich
* 2017: Schönstatt wagen – Du bist gefordert
* 2016: Vom Herz geleitet – Heute das Morgen prägen
* 2015: Es brennt in uns – mutig in die Welt
* 2014: Dein Funke – unser Feuer
* 2013: Festes Herz – himmelwärts
* 2012: Tiefe® leben – richtungsweisend
* 2011: Aufbruch – Zeichen setzen!
* 2010: Denke kühn – zeige Stärke
* 2009: Echt Leben prägend!
* 2008: Verwurzelt leben – den Menschen bewegen
* 2007: Generation Liebesbündnis –
Entflammt®Leben!
* 2006: Generation Liebesbündnis – Zukunfts®Evolution
* 2003: Gottes Ruf – unsere Herausforderung
* 2002: Die Welt hat sich verändert – Du kannst täglich mit ihr das gleiche tun!
* 1997: ER lebt – jetzt gilt's
* 1996: Gott in Dir und Mir...ERlebt
* 1995: Mann–schlag Wurzeln, und die Welt wird neu
* 1991: Zeig dein Gesicht
* 1990: Gib der Freiheit IHR Gesicht
* 1986: Im Bündnis gelebte Solidarität, Dynamik für das Schönstatt von Morgen
* 1985: Schlagt Wurzeln im Marienberg – Seid Gründer – Generation für das Schönstattwerk von Morgen
