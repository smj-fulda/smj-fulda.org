require 'flickraw'
require 'pastel'

class FlickrAlbum

  def call(id)
    html = flickr_photoset(id)
    puts
    puts
    puts html
  end

  def expand_flickr_tags(content)
    content = expand_flickr_photo(content)
    content = expand_flickr_photoset(content)
  end

  def expand_flickr_photo(content)
    content.gsub(%r{(?:<p>\s*)?\[photo id=(?:"|&quot;)?(\d+)[^\]]*\](?:\s*</p>)?}) do |_match|
      begin
        photo = flickr.photos.getInfo(photo_id: Regexp.last_match(1))
      rescue FlickRaw::FailedResponse => exc
        puts pastel.magenta(exc.to_s) + ' photo_id=' + pastel.cyan(Regexp.last_match(1))
        next
      end

      @first_image_in_post ||= photo

      url = FlickRaw.url_c(photo)
      date_taken = photo.dates.taken
      year_taken = date_taken[0..3]
      url_photopage = FlickRaw.url_photopage(photo)
      caption = flickr_photo_caption(photo)

      %(<figure data-href="#{url_photopage}">\n) +
        %(  <img src="#{url}" class="flickr-img flickr-img--medium" alt="#{caption}" />\n) +
        %{  <figcaption>#{caption} (<time datetime="#{date_taken}">#{year_taken}</time>)</figcaption>\n} +
        %(</figure>)
    end
  end

  def expand_flickr_photoset(content)
    content.gsub(%r{(?:<p>\s*)?\[(?:flickr-)?photoset[: ]id=(?:"|&quot;)?(\d+)[^\]]*\](?:\s*</p>)?}) do |_match|
      flickr_photoset(Regexp.last_match(1))
    end
  end

  def flickr_photoset(url)
    album = flickr.photosets.getInfo photoset_id: url
    photos = flickr.photosets.getPhotos photoset_id: url

    puts "loaded album ##{album.id} #{album.title} with #{album.photos} photos"

    url = FlickRaw.url_photoset(album)
    title = album.title

    html = ''
    html << %(<figure class="flickr-photoset full-width" data-href="#{url}">\n)
    photos.photo.take(14).each do |photo|
      photo = flickr.photos.getInfo(photo_id: photo.id)

      @first_image_in_post ||= photo

      img_url = FlickRaw.url_q(photo)
      large_url = FlickRaw.url_b(photo)
      photo_url = FlickRaw.url_photopage(photo) + '/in/album-' + album.id
      alt = flickr_photo_caption photo
      # alt = "Bildergalerie" if !alt || alt.empty?
      html << %(  <a href="#{photo_url}"><img src="#{img_url}" alt="#{alt}" data-src-large="#{large_url}" /></a>\n)
    end
    html << %(  <a href="#{url}" class="flickr-link" title="Bildergalerie #{title} auf Flickr">Bildergalerie</a>\n)
    html << %(</figure>)
    html
  rescue FlickRaw::FailedResponse => exc
    puts pastel.magenta(exc.to_s) + pastel.cyan(Regexp.last_match(1))
    content
  end

  def flickr_photo_caption(photo)
    caption = photo.title unless photo.title.start_with? 'DSC', 'IMG', 'CIMG'
    caption ||= photo.description if photo.respond_to? :description
    caption.strip if caption
  end

  def strip_flickr_tags(content)
    content.gsub(%r{(?:<p>\s*)?\[photo(?:set)? id=(?:"|&quot;)?(\d+)[^\]]*\](?:\s*</p>)?}, '')
  end

  def resolve_flickr_photo(idstring)
    photo_id = idstring.respond_to?(:id) ? idstring.id : idstring.to_s[/\d+/]

    idstring = nil if idstring == ''

    return idstring unless photo_id

    puts "Retrieving photo_id #{pastel.cyan(photo_id)} from Flickr."

    FlickRaw.url_b flickr.photos.getInfo(photo_id: photo_id)
  end

  private def pastel
    @pastel ||= Pastel.new
  end
end
